package com.smartLab.padra;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.net.ConnectivityManager;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;


/**
 * @author Faramarz
 *
 */

public class PadraApp extends Application {

	private static final String TAG = PadraApp.class.getSimpleName();
	
	private static Context mAppContext;

	public int SETUP_STEP = -1;

	private static PadraApp mInstance = null;

	// private ActivationController activationController;

	public PadraApp() {
		super();
	}

	public static Context getAppContext() {
		return mAppContext;
	}

	public static PadraApp getInstance() {
		return mInstance;
	}

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		Log.i(TAG, "onCreate");
		
		mInstance = this;
		mAppContext = getApplicationContext();
		
		
		//putPreferencesInt("setup_step", -1);
		if (!containsKey("setup_step")) {
			putPreferencesInt("setup_step", -1);
		}
		
		if (getPreferencesInt("setup_step") == -1) {
			putPreferencesBoolean(PreferenceRefrences.SHOW_RESPONSE_DIALOG, false);
		}
		
		SETUP_STEP = getStep();
		Log.i(TAG, "onCreate 4, setup_step : " + SETUP_STEP);
		
		//patternCheck();
		
		//testSetup();
		
	}
	
	void testSetup() {
		SharedPreferences mShared = PadraApp.preferences();
		mShared.edit().putString(PreferenceRefrences.DEVICE_NAME, "Faramarz").commit();
		mShared.edit().putString(PreferenceRefrences.DEVICE_PHONE_NUMBER, "09017964971").commit();
		mShared.edit().putInt(PreferenceRefrences.DEVICE_TYPE_KEY, 0).commit();
		mShared.edit().putString(PreferenceRefrences.DEVICE_PASSWORD, "123456").commit();
		mShared.edit().putString(PreferenceRefrences.MOBILE_NUMBER, "+989195087643").commit();
		setStep(10);
		
	}
	
	void patternCheck() {
		String str = "faramarz2093023";
		if (str.matches("[0-9]+")) {
			Log.i(TAG, "Str: " + str + " does match numeric");
		} else {
			Log.i(TAG, "Str: " + str + " does not match numeric");
		}
		
		str = "20934820342034242423";
		if (str.matches("[0-9]+")) {
			Log.i(TAG, "Str: " + str + " does match numeric");
		} else {
			Log.i(TAG, "Str: " + str + " does not match numeric");
		}
		
		str = "imei:2asdf093023";
		if (str.matches("[0-9]+")) {
			Log.i(TAG, "Str: " + str + " does match numeric");
		} else {
			Log.i(TAG, "Str: " + str + " does not match numeric");
		}
		
	}
	
	// activation utility functions
	
	public static int getStep() {
		return getInstance().getPreferencesInt("setup_step");
	}
	
	public static void setStep(int step) {
		getInstance().putPreferencesInt("setup_step", step);
	}
	
	public void setActivated(boolean flag) {
		Log.i(TAG, "PadraApplication setActivated method");		
		putPreferencesBoolean("is_activiated", flag);
	}
	
	public static boolean isActivated() {
		return getInstance().isPollActivated();
	}
	
	public boolean isPollActivated() {
		return getPreferencesBoolean("is_activiated", false);
	}
	
	public boolean containsKey(String key) {
		return preferences().contains(key);
	}
	
	public static SharedPreferences preferences() {
		return PreferenceManager.getDefaultSharedPreferences(getAppContext());			
	}

	public void putPreferencesLong(String key, long value) {
		preferences().edit().putLong(key, value).commit();
	}
	
	public void putPreferencesInt(String key, int value) {
		preferences().edit().putInt(key, value).commit();
	}
	
	public void putPreferencesBoolean(String key, Boolean value) {
		preferences().edit().putBoolean(key, value).commit();
	}
	
	public void putPreferencesString(String key, String value) {
		preferences().edit().putString(key, value).commit();
	}
	
	public String getPreferencesString(String key) {
		return getPreferencesString(key, null);
	}
	
	public String getPreferencesString(String key, String def) {
		return preferences().getString(key, def);
	}
	
	public boolean getPreferencesBoolean(String key, boolean def) {
		return preferences().getBoolean(key, def);
	}
	
	public boolean getPreferencesBoolean(String key) {
		return getPreferencesBoolean(key, false);
	}
	
	public int getPreferencesInt(String key) {
		return getPreferencesInt(key, 0);
	}
	
	public int getPreferencesInt(String key, int def) {
		return preferences().getInt(key, def);
	}
	
	public long getPreferencesLong(String key, long def) {
		return preferences().getLong(key, def);
	}

	public static boolean hasWifiOrMobileConnection() {
		ConnectivityManager conMan = (ConnectivityManager) getAppContext()
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		android.net.NetworkInfo.State wifi = conMan.getNetworkInfo(
				ConnectivityManager.TYPE_WIFI).getState();
		android.net.NetworkInfo.State mobile = conMan.getNetworkInfo(
				ConnectivityManager.TYPE_MOBILE).getState();
		return wifi == android.net.NetworkInfo.State.CONNECTED
				|| mobile == android.net.NetworkInfo.State.CONNECTED;
	}

	public static void showToast(String message) {
		showToast(message, Toast.LENGTH_SHORT);
	}

	public static void showToast(String message, int duration) {
		Toast toast = Toast.makeText(getAppContext(), message, duration);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();
	}

	public static String getVersionName(Context context) {
		try {
			PackageInfo pinfo = context.getPackageManager().getPackageInfo(
					context.getPackageName(), 0);
			return pinfo.versionName;
		} catch (android.content.pm.PackageManager.NameNotFoundException e) {
			return null;
		}
	}

	public static int getVersionCode(Context context) {
		try {
			PackageInfo pinfo = context.getPackageManager().getPackageInfo(
					context.getPackageName(), 0);
			return pinfo.versionCode;
		} catch (android.content.pm.PackageManager.NameNotFoundException e) {
			return -1;
		}
	}

}
