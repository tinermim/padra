package com.smartLab.padra;

import android.annotation.SuppressLint;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.padra.R;

@SuppressLint("NewApi")
abstract public class BaseSliderFragment extends Fragment {

	protected static final String POSITION_KEY = "position_key";
	protected static final String TAG = BaseSliderFragment.class
			.getSimpleName();
	protected int position;
	public View v;
	public int IMAGE_NUM = 0;
	protected LinearLayout iv;
	public Drawable[] drawables;

	public BaseSliderFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle args = getArguments();
		this.position = args.getInt(POSITION_KEY);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		v = getLayout(inflater, container, false);

		if (position == 0) {
			IMAGE_NUM = 36;
			iv = (LinearLayout) v.findViewById(R.id.mainLayout);
		} else if (position == 1) {
			IMAGE_NUM = 40;
			iv = (LinearLayout) v.findViewById(R.id.mainLayout);
			iv.setRotationY(-90);
		} else if (position == 2) {
			IMAGE_NUM = 40;
			iv = (LinearLayout) v.findViewById(R.id.mainLayout);
			iv.setRotationY(-90);
		} else if (position == 3) {
			IMAGE_NUM = 40;
			iv = (LinearLayout) v.findViewById(R.id.mainLayout);
			iv.setRotationY(-90);
		} else if (position == 4) {
			IMAGE_NUM = 1;
			iv = (LinearLayout) v.findViewById(R.id.mainLayout);
			iv.setRotationY(90);
		}

		return v;
	}

	protected abstract View getLayout(LayoutInflater inflater, ViewGroup container,
			boolean b);

	public void changeImage(float positionOffset,
			BaseSliderFragment[] fragmentsList) {
		int index = (int) (positionOffset * IMAGE_NUM);
		if (index < IMAGE_NUM && index >= 0) {
			iv.setRotationY(positionOffset * 90);
			if (position + 1 != fragmentsList.length) {
				fragmentsList[position + 1].iv.setRotationY(-90
						+ positionOffset * 90);
			}
		}
	}

	protected void setDrawableAnimation(final int id, final int drawable_id) {
				ImageView img = (ImageView) getActivity().findViewById(id);
				img.setBackgroundResource(drawable_id);
				AnimationDrawable frameAnimation = (AnimationDrawable) img
						.getBackground();
				frameAnimation.start();
	}

}