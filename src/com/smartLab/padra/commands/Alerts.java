package com.smartLab.padra.commands;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.padra.R;
import com.smartLab.padra.BaseSliderFragment;
import com.smartLab.padra.PreferenceRefrences;
import com.smartLab.padra.SMS.CommandItem;
import com.smartLab.padra.SMS.CommandsToPersian;
import com.smartLab.padra.SMS.TextMessageSender;
import com.smartLab.padra.commands.AnimatedExpandableListView.AnimatedExpandableListAdapter;
import com.smartLab.padra.remote.AppListAdapter;
import com.smartLab.padra.remote.FoldingPaneLayout;
import com.smartLab.padra.utils.ImageLoader;
import com.smartLab.padra.utils.ListItem;
import com.smartLab.setting.SettingActivity;

public class Alerts extends BaseSliderFragment {
	public AnimatedExpandableListView listView;
	private ExampleAdapter adapter;
	private List<GroupItem> items;
	private static float Hue;
	public FoldingPaneLayout mPaneLayout;
	public ListView mPaneList;
	private List<ListItem> drawerItems;
	private SharedPreferences mShared;

	public void updateListView() {
		new AsyncTask<Void, Void, Void>() {
			@Override
			protected Void doInBackground(Void... params) {
				if (getActivity() != null) {
					mShared = PreferenceManager
							.getDefaultSharedPreferences(getActivity()
									.getApplicationContext());

					adapter = new ExampleAdapter(getActivity());
					listView = (AnimatedExpandableListView) getActivity()
							.findViewById(R.id.listView);

					int num2show = mShared.getInt(
							PreferenceRefrences.NUMBER_OF_COMMANDS_TO_SHOW, 2);
					int what2show = mShared.getInt(
							PreferenceRefrences.ALERT_TYPE_TO_SHOW, 2);

					ArrayList<CommandItem> commands = TextMessageSender
							.getInstance(getActivity()).getCommandsLog();
					items = new ArrayList<GroupItem>();
					int counter = 0;

					if (commands.size() > 0) {
						counter = commands.size() - 1;
						if (num2show == 0) {
							GroupItem item = new GroupItem();
							Calendar c = Calendar.getInstance();
							item.title = c.get(Calendar.DAY_OF_MONTH) + "/"
									+ (c.get(Calendar.MONTH) + 1) + "/"
									+ c.get(Calendar.YEAR);

							while (counter >= 0
									&& c.get(Calendar.DAY_OF_MONTH) == commands
											.get(counter).sendTime
											.get(Calendar.DAY_OF_MONTH)
									&& c.get(Calendar.MONTH) == commands
											.get(counter).sendTime
											.get(Calendar.MONTH)
									&& c.get(Calendar.YEAR) == commands
											.get(counter).sendTime
											.get(Calendar.YEAR)) {
								ChildItem child = new ChildItem();
								child.time = commands.get(counter).sendTime
										.get(Calendar.HOUR_OF_DAY)
										+ ":"
										+ commands.get(counter).sendTime
												.get(Calendar.MINUTE);

								child.isSent = (commands.get(counter).isSent) ? true
										: false;
								child.isDelivered = (commands.get(counter).isDelivered) ? true
										: false;
								child.isResponseReceived = (commands
										.get(counter).isResponseReceived) ? true
										: false;

								child.isAlarm = commands.get(counter).isAlarm;

								child.command = (child.isAlarm) ? getActivity()
										.getString(R.string.alarm)
										+ CommandsToPersian
												.getCommandMeaningToUser(
														commands.get(counter).type,
														getActivity())
										: CommandsToPersian
												.getCommandMeaningToUser(
														commands.get(counter).type,
														getActivity());

								if (what2show == 2
										|| (what2show == 0 && child.isAlarm == false)
										|| ((what2show == 1 && child.isAlarm == true)))
									item.items.add(child);
								counter--;
							}
							if (item.items.size() > 0)
								items.add(item);
						} else if (num2show == 1)
							makeListWithNumber(commands, counter, counter - 10,
									what2show);
						else if (num2show == 2)
							makeListWithNumber(commands, counter, counter - 20,
									what2show);
						else if (num2show == 3)
							makeListWithNumber(commands, counter, counter - 50,
									what2show);
					}

					return null;
				}
				return null;
			}

			@Override
			protected void onPostExecute(Void result) {
				if (adapter != null && listView != null) {
					adapter.setData(items);
					listView.setAdapter(adapter);
					listView.setOnGroupClickListener(new OnGroupClickListener() {
						@Override
						public boolean onGroupClick(ExpandableListView parent,
								View v, int groupPosition, long id) {
							if (listView.isGroupExpanded(groupPosition)) {
								listView.collapseGroupWithAnimation(groupPosition);
							} else {
								listView.expandGroupWithAnimation(groupPosition);
							}
							return true;
						}
					});
				}
				super.onPostExecute(result);
			}
		}.execute();
	}

	private void makeListWithNumber(ArrayList<CommandItem> commands,
			int counter, int limit, int what2show) {

		while (counter >= 0 && counter >= limit) {
			GroupItem item = new GroupItem();
			Calendar c = commands.get(counter).sendTime;
			item.title = c.get(Calendar.DAY_OF_MONTH) + "/"
					+ (c.get(Calendar.MONTH) + 1) + "/" + c.get(Calendar.YEAR);

			while (c != null
					&& counter >= 0
					&& counter >= limit
					&& c.get(Calendar.DAY_OF_MONTH) == commands.get(counter).sendTime
							.get(Calendar.DAY_OF_MONTH)
					&& c.get(Calendar.MONTH) == commands.get(counter).sendTime
							.get(Calendar.MONTH)
					&& c.get(Calendar.YEAR) == commands.get(counter).sendTime
							.get(Calendar.YEAR)) {
				ChildItem child = new ChildItem();
				String minute = (commands.get(counter).sendTime
						.get(Calendar.MINUTE) >= 10) ? commands.get(counter).sendTime
						.get(Calendar.MINUTE) + ""
						: "0"
								+ commands.get(counter).sendTime
										.get(Calendar.MINUTE);
				child.time = commands.get(counter).sendTime
						.get(Calendar.HOUR_OF_DAY) + ":" + minute;

				child.isSent = (commands.get(counter).isSent) ? true : false;
				child.isDelivered = (commands.get(counter).isDelivered) ? true
						: false;
				child.isResponseReceived = (commands.get(counter).isResponseReceived) ? true
						: false;

				child.isAlarm = commands.get(counter).isAlarm;

				child.command = (child.isAlarm) ? getActivity().getString(
						R.string.alarm)
						+ CommandsToPersian.getCommandMeaningToUser(
								commands.get(counter).type, getActivity())
						: CommandsToPersian.getCommandMeaningToUser(
								commands.get(counter).type, getActivity());

				if (what2show == 2
						|| (what2show == 0 && child.isAlarm == false)
						|| ((what2show == 1 && child.isAlarm == true)))
					item.items.add(child);
				counter--;
			}
			if (item.items.size() > 0)
				items.add(item);
		}

	}

	@Override
	public void onResume() {
		mShared = PreferenceManager.getDefaultSharedPreferences(getActivity()
				.getApplicationContext());
		updateListView();
		setDrawableAnimation(R.id.animationSwipeLeftAlert,
				R.drawable.animation_left);
		setDrawableAnimation(R.id.animationSwipeRightAlert,
				R.drawable.animation_right);

		initDrawer();
		super.onResume();
	}

	public static BaseSliderFragment newInstance(int position) {
		BaseSliderFragment f = new Alerts();
		Bundle bundle = new Bundle();
		bundle.putInt(POSITION_KEY, position);
		f.setArguments(bundle);
		return f;
	}

	@Override
	protected View getLayout(LayoutInflater inflater, ViewGroup container,
			boolean b) {
		return inflater.inflate(R.layout.fragments_alerts_view, container,
				false);
	}

	private static class GroupItem {
		String title;
		List<ChildItem> items = new ArrayList<ChildItem>();
	}

	private static class ChildItem {
		String time;
		String command;
		boolean isAlarm = false;
		boolean isSent;
		boolean isDelivered;
		boolean isResponseReceived;
	}

	private static class ChildHolder {
		TextView time;
		TextView command;
		ImageView isSent;
		ImageView isDelivered;
		ImageView isResponseReceived;
	}

	private static class GroupHolder {
		TextView title;
	}

	/**
	 * Adapter for our list of {@link GroupItem}s.
	 */
	private class ExampleAdapter extends AnimatedExpandableListAdapter {
		private LayoutInflater inflater;

		private List<GroupItem> items;

		public ExampleAdapter(Context context) {
			inflater = LayoutInflater.from(context);
		}

		public void setData(List<GroupItem> items) {
			this.items = items;
		}

		@Override
		public ChildItem getChild(int groupPosition, int childPosition) {
			return items.get(groupPosition).items.get(childPosition);
		}

		@Override
		public long getChildId(int groupPosition, int childPosition) {
			return childPosition;
		}

		@Override
		public View getRealChildView(int groupPosition, int childPosition,
				boolean isLastChild, View convertView, ViewGroup parent) {
			ChildHolder holder;
			ChildItem item = getChild(groupPosition, childPosition);
			holder = new ChildHolder();
			if (!item.isAlarm)
				convertView = inflater.inflate(R.layout.list_item, parent,
						false);
			else
				convertView = inflater.inflate(R.layout.list_item_alert,
						parent, false);

			holder.time = (TextView) convertView.findViewById(R.id.commandTime);
			holder.command = (TextView) convertView
					.findViewById(R.id.commandName);

			if (!item.isAlarm) {
				holder.isSent = (ImageView) convertView
						.findViewById(R.id.isSent);
				holder.isDelivered = (ImageView) convertView
						.findViewById(R.id.isDelivered);
				holder.isResponseReceived = (ImageView) convertView
						.findViewById(R.id.isResponseReceived);
			}
			convertView.setTag(holder);

			holder.time.setText(item.time);
			holder.command.setText(item.command);
			if (!item.isAlarm) {

				try {
					int isDeliveredIcon = (item.isDelivered) ? R.drawable.check
							: R.drawable.no_check;
					int isResponseReceivedIcon = (item.isResponseReceived) ? R.drawable.check
							: R.drawable.no_check;
					int isSentIcon = (item.isSent) ? R.drawable.check
							: R.drawable.no_check;

					new ImageLoader(getActivity(), holder.isDelivered,
							isDeliveredIcon, 1).execute();
					new ImageLoader(getActivity(), holder.isResponseReceived,
							isResponseReceivedIcon, 1).execute();
					new ImageLoader(getActivity(), holder.isSent, isSentIcon, 1)
							.execute();
				} catch (Exception e) {
				}

			}
			Hue = (groupPosition * 60 / getGroupCount()) + 150;
			convertView.setBackgroundColor(Color.HSVToColor(100, new float[] {
					Hue, 0.05f, 1f }));

			return convertView;
		}

		@Override
		public int getRealChildrenCount(int groupPosition) {
			return items.get(groupPosition).items.size();
		}

		@Override
		public GroupItem getGroup(int groupPosition) {
			return items.get(groupPosition);
		}

		@Override
		public int getGroupCount() {
			return items.size();
		}

		@Override
		public long getGroupId(int groupPosition) {
			return groupPosition;
		}

		@Override
		public View getGroupView(int groupPosition, boolean isExpanded,
				View convertView, ViewGroup parent) {
			GroupHolder holder;
			GroupItem item = getGroup(groupPosition);
			if (convertView == null) {
				holder = new GroupHolder();
				convertView = inflater.inflate(R.layout.group_item, parent,
						false);
				holder.title = (TextView) convertView
						.findViewById(R.id.textTitle);
				convertView.setTag(holder);
			} else {
				holder = (GroupHolder) convertView.getTag();
			}

			holder.title.setText(item.title);
			Hue = (groupPosition * 50 / getGroupCount()) + 150;
			convertView.setBackgroundColor(Color.HSVToColor(85, new float[] {
					Hue, 0.5f, 1f }));

			return convertView;
		}

		@Override
		public boolean hasStableIds() {
			return true;
		}

		@Override
		public boolean isChildSelectable(int arg0, int arg1) {
			return true;
		}

	}

	private void initDrawer() {
		mPaneLayout = (FoldingPaneLayout) getActivity().findViewById(
				R.id.drawer_layout_alert);
		mPaneList = (ListView) getActivity().findViewById(
				R.id.left_drawer_alert);
		mPaneLayout.getFoldingLayout().setNumberOfFolds(4);
		mPaneLayout.foldingNavigationLayout.setBackgroundColor(Color.BLACK);

		// set up the drawer's list view with items and click listener

		drawerItems = new LinkedList<ListItem>();

		drawerItems.add(new ListItem(0, getActivity().getResources().getString(
				R.string.settings), getActivity().getResources().getDrawable(
				R.drawable.icon)));

		drawerItems.add(new ListItem(1, getActivity().getResources().getString(
				R.string.listSize), getActivity().getResources().getDrawable(
				R.drawable.abc_ic_go_search_api_mtrl_alpha)));

		drawerItems.add(new ListItem(2, getActivity().getResources().getString(
				R.string.messageType), getActivity().getResources()
				.getDrawable(R.drawable.abc_ic_go_search_api_mtrl_alpha)));

		AppListAdapter adapter = new AppListAdapter(getActivity()
				.getApplicationContext(), drawerItems);
		mPaneList.setAdapter(adapter);

		mPaneList.setOnItemClickListener(new DrawerItemClickListener());
	}

	/* The click listner for ListView in the navigation drawer */
	private class DrawerItemClickListener implements
			ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			selectItem(position);
		}
	}

	private void selectItem(int position) {
		Toast.makeText(getActivity(), drawerItems.get(position).text,
				Toast.LENGTH_SHORT);

		if (position == 0) {
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					Intent myIntent = new Intent(getActivity(),
							SettingActivity.class);
					getActivity().startActivity(myIntent);
				}
			}, 200);
		} else if (position == 1) {

			int key = mShared.getInt(
					PreferenceRefrences.NUMBER_OF_COMMANDS_TO_SHOW, 2);
			Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
			// set title
			alertDialogBuilder.setTitle(R.string.numberOfCommandsToShow);
			alertDialogBuilder.setCancelable(false);
			alertDialogBuilder.setSingleChoiceItems(R.array.commandToShow, key,
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							switch (which) {
							case 0:
								mShared.edit()
										.putInt(PreferenceRefrences.NUMBER_OF_COMMANDS_TO_SHOW,
												0).commit();
								break;
							case 1:
								mShared.edit()
										.putInt(PreferenceRefrences.NUMBER_OF_COMMANDS_TO_SHOW,
												1).commit();
								break;
							case 2:
								mShared.edit()
										.putInt(PreferenceRefrences.NUMBER_OF_COMMANDS_TO_SHOW,
												2).commit();
								break;
							case 3:
								mShared.edit()
										.putInt(PreferenceRefrences.NUMBER_OF_COMMANDS_TO_SHOW,
												3).commit();
								break;
							}
						}

					});

			alertDialogBuilder.setPositiveButton(getActivity().getResources()
					.getString(R.string.accept),
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int id) {
							updateListView();
						}
					});

			alertDialogBuilder.setCancelable(true);
			alertDialogBuilder.show();
		} else if (position == 2) {

			int key = mShared.getInt(PreferenceRefrences.ALERT_TYPE_TO_SHOW, 2);
			Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
			// set title
			alertDialogBuilder.setTitle(R.string.commandTypeToShow);

			alertDialogBuilder.setSingleChoiceItems(R.array.commandType, key,
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							switch (which) {
							case 0:
								mShared.edit()
										.putInt(PreferenceRefrences.ALERT_TYPE_TO_SHOW,
												0).commit();
								break;
							case 1:
								mShared.edit()
										.putInt(PreferenceRefrences.ALERT_TYPE_TO_SHOW,
												1).commit();
								break;
							case 2:
								mShared.edit()
										.putInt(PreferenceRefrences.ALERT_TYPE_TO_SHOW,
												2).commit();
								break;
							}
						}

					});

			// alertDialogBuilder.setIcon(R.drawable.question_mark);
			// // Set the action buttons
			alertDialogBuilder.setPositiveButton(getActivity().getResources()
					.getString(R.string.accept),
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int id) {
							updateListView();
						}
					});

			alertDialogBuilder.setCancelable(true);
			alertDialogBuilder.show();
		}

		mPaneLayout.closePane();
	}

}