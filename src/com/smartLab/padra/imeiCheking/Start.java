package com.smartLab.padra.imeiCheking;

import java.security.MessageDigest;
import java.util.Random;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.smartLab.padra.PadraApp;
import com.smartLab.padra.PreferenceRefrences;

public class Start {
	static int randomkey;
	public final static String url = "http://trase.ir/check_imei/";

	public static void encryption(Context context, String imei) {
		try {
			// Creating md5 IMEI
			String imeiMd5 = getMD5IMEI(imei);

			// Generating a five digit random key; you should save this for
			// further checking
			Random rnd = new Random();
			randomkey = 10000 + rnd.nextInt(90000) % 10000;
			randomkey = randomkey | 54321;
			String phone = PadraApp.getInstance().getPreferencesString(PreferenceRefrences.MOBILE_NUMBER);
			if (phone == null) {
				SharedPreferences mShared = PadraApp.preferences();
				phone = mShared.getString(PreferenceRefrences.MOBILE_NUMBER, null);
				if (phone == null) {
					phone = "+98";
				}
			}
			// Preparing data by Gson
			MyEntity example = new MyEntity();
			example.setImei(imeiMd5);
			example.setRandomkey(randomkey);
			example.setPhone(phone);
			
			Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
			String flatjson = gson.toJson(example);

			// Calling the web service
			// Calling async task to get json
			GetDaysHours task = new GetDaysHours();
			task.context = context;
			task.imei = imei;			
			task.url = url;
			task.flatJson = flatjson;
			task.execute();

			// PKCS5Padding
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static String getMD5IMEI(String imei) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(imei.getBytes());
			byte[] mdbytes = md.digest();

			// convert the byte to hex format method 2
			StringBuffer hexString = new StringBuffer();
			for (int i = 0; i < mdbytes.length; i++) {
				String hex = Integer.toHexString(0xff & mdbytes[i]);
				if (hex.length() == 1)
					hexString.append('0');
				hexString.append(hex);
			}
			return hexString.toString();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
