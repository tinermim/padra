package com.smartLab.padra.imeiCheking;

import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.IvParameterSpec;

public class Encrypter {
	private KeySpec keySpec;
	private SecretKey key;
	private IvParameterSpec iv;

	public Encrypter(byte[] keyBytes, byte[] ivBytes) {
		try {
			keySpec = new DESedeKeySpec(keyBytes);
			key = SecretKeyFactory.getInstance("DESede")
					.generateSecret(keySpec);

			iv = new IvParameterSpec(ivBytes);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public byte[] decrypt(byte[] data) {
		try {
			Cipher dcipher = Cipher.getInstance("DESede/CBC/NoPadding"); // PKCS5Padding
			dcipher.init(Cipher.DECRYPT_MODE, key, iv);
			return dcipher.doFinal(data);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public byte[] encrypt(byte[] plaintext) {
		try {
			Cipher cipher = Cipher.getInstance("DESede/CBC/PKCS5Padding"); // NoPadding
			cipher.init(Cipher.ENCRYPT_MODE, key, iv);
			return cipher.doFinal(plaintext);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}