package com.smartLab.padra.imeiCheking;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Base64;
import android.util.Log;

import com.example.padra.R;
import com.smartLab.padra.MainActivity;
import com.smartLab.padra.PadraApp;
import com.smartLab.padra.PreferenceRefrences;
import com.smartLab.padra.asetup.WelcomeActivity;

/**
 * Async task class to get json by making HTTP call
 * */
public class GetDaysHours extends AsyncTask<Void, Void, String> {
	private static final String IMEI_CONNECTION_PROBLEM = "connectionProblem";
	private static final String IMEI_ERROR = "error";
	private static final String IMEI_NOT_FOUND = "notfound";
	private static final String IMEI_FOUND = "found";
	private static final String TAG_IMEI = "imei";
	private static final String IS_IMEI_VALIDATED = "IS_IMEI_VALIDATED";

	public Context context;
	public String url;
	public String flatJson;
	public String imei;

	ProgressDialog pDialog;

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		// Showing progress dialog
		pDialog = new ProgressDialog(context);
		pDialog.setMessage(context.getString(R.string.imeiCheking));
		pDialog.setCancelable(false);
		pDialog.show();
	}

	@Override
	protected String doInBackground(Void... arg0) {
		// Creating service handler class instance
		ServiceHandler sh = new ServiceHandler();
		
		Log.i("GetDaysHours", "Flat json : " + flatJson);
		
		// Making a request to url and getting response
		createEncrypter();
		String send = encryptIt(flatJson);
		
		String jsonStr = sh.makeServiceCall(url, ServiceHandler.POST, send);
		String imei = null;
		if (jsonStr != null) {
			try {

				byte[] resall = Base64.decode(jsonStr, Base64.DEFAULT);

				byte[] decryptd = myEncrypter.decrypt(resall);
				String origalld = new String(decryptd, "utf-8");
				JSONObject jsonObj = new JSONObject(origalld);
				imei = jsonObj.getString(TAG_IMEI);
				
				
			} catch (JSONException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}

		} else {
			imei = IMEI_CONNECTION_PROBLEM;
		}

		//imei = IMEI_FOUND;
		
		return imei;
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		// Dismiss the progress dialog
		if (pDialog != null && pDialog.isShowing() )
			try{
			pDialog.dismiss();
			}catch (Exception e) {
				e.printStackTrace();
		    } 

		Builder alert = new AlertDialog.Builder(context);
		alert.setTitle(context.getResources().getString(
				R.string.imei_validation_result));

		alert.setCancelable(false);

		switch (result) {
		case IMEI_FOUND:
			alert.setPositiveButton(
					context.getResources().getString(R.string.confirm),
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int id) {
							SharedPreferences mShared = PreferenceManager
									.getDefaultSharedPreferences(context
											.getApplicationContext());
							mShared.edit().putBoolean(IS_IMEI_VALIDATED, true)
									.commit();
							
							/// FD
							PadraApp.setStep(10);
							
							mShared.edit().putBoolean(PreferenceRefrences.SHOW_RESPONSE_DIALOG, true).commit();
							
							Intent intent = new Intent(context, WelcomeActivity.class);
							intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
							context.startActivity(intent);
							/// -FD
							
							((Activity) context).finish();
						}
					});
			alert.setMessage(context.getResources().getString(
					R.string.imeiConfirmationMessage));
			break;

		case IMEI_NOT_FOUND:
			alert.setPositiveButton(
					context.getResources().getString(R.string.reconnect),
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int id) {
							Start.encryption(context, imei);
						}
					});
			alert.setMessage(context.getResources().getString(
					R.string.imei_not_found));

			break;

		case IMEI_ERROR:
			alert.setPositiveButton(
					context.getResources().getString(R.string.reconnect),
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int id) {
							Start.encryption(context, imei);
						}
					});
			alert.setMessage(context.getResources().getString(
					R.string.imei_error_try_again));
			break;

		case IMEI_CONNECTION_PROBLEM:
			alert.setPositiveButton(
					context.getResources().getString(R.string.reconnect),
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int id) {
							Start.encryption(context, imei);
						}
					});
			alert.setMessage(context.getResources().getString(
					R.string.imei_not_connected_try_again));
			break;

		default:
			break;
		}

		alert.show();

	}

	Encrypter myEncrypter;

	public void createEncrypter() {

		try {

			String passphrase = "86103702";

			String pass = Start.getMD5IMEI(passphrase);
			String key = pass.substring(0, 24);
			String iv = pass.substring(0, 8);

			myEncrypter = new Encrypter(key.getBytes("utf-8"),
					iv.getBytes("utf-8"));

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	private String encryptIt(String out) {

		try {

			if (myEncrypter == null) {
				createEncrypter();
			}

			byte[] encrypt = myEncrypter.encrypt(out.getBytes("utf-8"));

			String base64 = Base64.encodeToString(encrypt, Base64.DEFAULT);
			Log.i("Amri", "Base64 is :::" + base64 + ":::");
			return base64;

		} catch (Exception e) {
			// TODO: handle exception
		}

		return out;
	}

}