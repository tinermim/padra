package com.smartLab.padra.imeiCheking;

import com.google.gson.annotations.Expose;

public class MyEntity {

	@Expose
	private String imei = null;
	
	@Expose
	private int randomkey = 0;
	
	@Expose
	private String phone;
	
	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public int getRandomkey() {
		return randomkey;
	}
	
	public void setRandomkey(int randomkey) {
		this.randomkey = randomkey;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	
}