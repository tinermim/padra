package com.smartLab.padra.asetup;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.example.padra.R;
import com.smartLab.padra.MainActivity;
import com.smartLab.padra.PadraApp;
import com.smartLab.padra.PreferenceRefrences;

public class WelcomeActivity extends ActionBarActivity {
	
	private static final String TAG = WelcomeActivity.class.getSimpleName();
	
	private Button button;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Log.i(TAG, "onCreate");
		
//		Log.i(TAG, "onCreate : " + PadraApp.getStep());
		
		if (PadraApp.getStep() < 10) {
			PadraApp.getInstance().putPreferencesBoolean(PreferenceRefrences.SHOW_RESPONSE_DIALOG, false);
		}
		
		checkingSteps();
		
//		if (PadraApp.getStep() != -1) {
//			return;
//		}
		
		setContentView(R.layout.activity_welcome);
		
		try {
			
			setTitle("Welcome to padra");
			
			//getSupportActionBar().setDisplayHomeAsUpEnabled(false);
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		
		button = (Button) findViewById(R.id.btn_continue); 
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				goToNextActivity();
			}
		});
		
		setTitle("kkk");
		
		FirstRunHelpInformation();
		
	}
	
	private void FirstRunHelpInformation() {
		SharedPreferences mShared = PadraApp.preferences();
		if(!mShared.getBoolean(PreferenceRefrences.FIRST_RUN_HELP_INFO_KEY, false)){
			mShared.edit().putBoolean(PreferenceRefrences.FIRST_RUN_HELP_INFO_KEY, true).commit();
			new AlertDialog.Builder(WelcomeActivity.this).
			setTitle(R.string.welcome).
			setMessage(R.string.first_run_help_information).
			setCancelable(false).
			setPositiveButton(R.string.confirm, null).
			show();
		}
	}
	
	
	private void goToNextActivity() {
		//PadraApp.setStep(0);
		startDeviceInfoActivity();
	}
	
	private void checkingSteps() {
		
		switch (PadraApp.getStep()) {
		case -1:
			// We are here. good to go
			break;
		case 0:
			startDeviceInfoActivity();
			break;
		case 1:
			startFirstActivity();
			break;
		case 2:
			startSecondActivity();
			break;
		case 3:
			startThirdActivity();
			break;
		case 4:
			startFourthActivity();
			break;
		case 5:
			startFifthActivity();
			break;
		case 6:
			startSixthActivity();
			break;
		case 7:
			startSeventhActivity();
			break;
		case 8:
			startEighthActivity();
			break;
		case 9:
			startIMEIActivity();
			break;
		case 10:
			startHomeActivity();
			break;
		default:
			break;
		}
		
	}
	
	private void startHomeActivity() {
		PadraApp.setStep(10);
		
		Intent intent = new Intent(this, MainActivity.class);
		//intent.setAction("android.intent.action.MAIN");
		//intent.putExtra("fresh_start", false);
//		intent.addFlags(0x10000000);
//		intent.addFlags(0x4000000);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
		overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);

		finish();

	}
	
	private void startDeviceInfoActivity() {
		
		PadraApp.setStep(0);
		
		Intent intent = new Intent(this, DeviceInfoActivity.class);
		//intent.setAction("android.intent.action.MAIN");
		//intent.putExtra("fresh_start", false);
//		intent.addFlags(0x10000000);
//		intent.addFlags(0x4000000);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
		overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);

		finish();
	}
	
	private void startFirstActivity() {
		
		PadraApp.setStep(1);
		
		Intent intent = new Intent(this, StepFirst.class);
		//intent.setAction("android.intent.action.MAIN");
//		intent.putExtra("fresh_start", false);
//		intent.addFlags(0x10000000);
//		intent.addFlags(0x4000000);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
		overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);

		finish();
	}
	
	private void startSecondActivity() {
		
		PadraApp.setStep(2);
		
		Intent intent = new Intent(this, StepSecond.class);
		//intent.setAction("android.intent.action.MAIN");
//		intent.putExtra("fresh_start", false);
//		intent.addFlags(0x10000000);
//		intent.addFlags(0x4000000);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
		overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);

		finish();
	}
	
	private void startThirdActivity() {
		
		PadraApp.setStep(3);
		
		Intent intent = new Intent(this, StepThird.class);
		//intent.setAction("android.intent.action.MAIN");
//		intent.putExtra("fresh_start", false);
//		intent.addFlags(0x10000000);
//		intent.addFlags(0x4000000);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
		overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);

		finish();
	}

	private void startFourthActivity() {
		Log.i(TAG, "Starting fourth step activity");
		PadraApp.setStep(4);
		
		Intent intent = new Intent(this, StepFourth.class);
		//intent.setAction("android.intent.action.MAIN");
//		intent.putExtra("fresh_start", false);
//		intent.addFlags(0x10000000);
//		intent.addFlags(0x4000000);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
		overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);

		finish();
	}
	
	private void startFifthActivity() {
		
		PadraApp.setStep(5);
		
		Intent intent = new Intent(this, StepFifth.class);
		//intent.setAction("android.intent.action.MAIN");
//		intent.putExtra("fresh_start", false);
//		intent.addFlags(0x10000000);
//		intent.addFlags(0x4000000);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
		overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);

		finish();
	}	
	
	private void startSixthActivity() {
		
		PadraApp.setStep(6);
		
		Intent intent = new Intent(this, StepSixth.class);
		//intent.setAction("android.intent.action.MAIN");
//		intent.putExtra("fresh_start", false);
//		intent.addFlags(0x10000000);
//		intent.addFlags(0x4000000);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
		overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);

		finish();
	}
	
	private void startSeventhActivity() {
		
		PadraApp.setStep(7);
		
		Intent intent = new Intent(this, StepSeventh.class);
		//intent.setAction("android.intent.action.MAIN");
//		intent.putExtra("fresh_start", false);
//		intent.addFlags(0x10000000);
//		intent.addFlags(0x4000000);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
		overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);

		finish();
	}

	private void startEighthActivity() {
		
		PadraApp.setStep(8);
		
		Intent intent = new Intent(this, StepEighth.class);
		//intent.setAction("android.intent.action.MAIN");
//		intent.putExtra("fresh_start", false);
//		intent.addFlags(0x10000000);
//		intent.addFlags(0x4000000);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
		overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);

		finish();
	}
	
	private void startIMEIActivity() {
		
		PadraApp.setStep(9);
		
		Intent intent = new Intent(this, StepIMEIActivity.class);
		//intent.setAction("android.intent.action.MAIN");
//		intent.putExtra("fresh_start", false);
//		intent.addFlags(0x10000000);
//		intent.addFlags(0x4000000);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
		overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);

		finish();

	}
}
