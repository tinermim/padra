package com.smartLab.padra.asetup;

import com.example.padra.R;
import com.smartLab.padra.PadraApp;
import com.smartLab.padra.PreferenceRefrences;
import com.smartLab.padra.SMS.CommandType;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class DiagnoiseActivity extends FragmentActivity {

	private static final String TAG = DiagnoiseActivity.class.getSimpleName();
	
	private TextView topLabel;
	private TextView resetStatus;
	private Button mContinue;

	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		Log.i(TAG, "onCreate");
		super.onCreate(arg0);
		setContentView(com.example.padra.R.layout.activity_step_first);

		topLabel = (TextView) findViewById(R.id.reset);
		resetStatus = (TextView) findViewById(R.id.reset_status);
		mContinue = (Button) findViewById(R.id.reset_continue);
		
		topLabel.setText(R.string.step_sixth);
		
		mContinue.setEnabled(false);
		mContinue.setVisibility(View.GONE);
		
		logInfo();
		
	}
	
	private void logInfo() {
		
		Log.i(TAG, "Application Step : " + PadraApp.getStep());
		Log.i(TAG, "Device name : " + PadraApp.getInstance().getPreferencesString(PreferenceRefrences.DEVICE_NAME));
		Log.i(TAG, "Device phone number : " + PadraApp.getInstance().getPreferencesString(PreferenceRefrences.DEVICE_PHONE_NUMBER));
		Log.i(TAG, "Mobile Number : " + PadraApp.getInstance().getPreferencesString(PreferenceRefrences.MOBILE_NUMBER));
		
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
		// go for sending first SMS
		// send and receive authorization SMS
		resetStatus.setText("در حال ارسال پیام وضعیت دیاگ خودرو");
		
		sendBeginSMS();

	}
	
	private void sendBeginSMS() {
		
		TextMessageSenderMine sms = TextMessageSenderMine.getInstance(this);
		sms.sendSms(CommandType.diag, null);
		
	}
	
}
