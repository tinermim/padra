package com.smartLab.padra.asetup;

import com.example.padra.R;
import com.smartLab.padra.PadraApp;
import com.smartLab.padra.PreferenceRefrences;
import com.smartLab.padra.SMS.CommandType;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class StepSixth extends FragmentActivity {

	private static final String TAG = StepSixth.class.getSimpleName();
	
	
	/**
	 *  فاز سوم: تنظیمات نهایی برای ردیابی مداوم
اینجا ترتیب مهم نیست اما بهترست بازهم بهمین ترتیبی که نوشته ام اجرا شود.
        // 6. Suppress.                         ==> suppress123456
        // 7. Time Zone.                     ==> time zone123456 number
        // 8. Track Unlimited Interval.             ==> fix060s***n123456

	 */

	private SharedPreferences mShared;
	
	private TextView topLabel;
	private TextView resetStatus;
	private Button mContinue;

	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		Log.i(TAG, "onCreate");
		super.onCreate(arg0);
		setContentView(com.example.padra.R.layout.activity_step_first);

		topLabel = (TextView) findViewById(R.id.reset);
		resetStatus = (TextView) findViewById(R.id.reset_status);
		mContinue = (Button) findViewById(R.id.reset_continue);
		
		topLabel.setText(R.string.step_sixth);
		
		mContinue.setEnabled(false);
		mContinue.setVisibility(View.GONE);
		
//		mContinue.setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				goToNextActivity();
//			}
//		});
		
		logInfo();
		
		topLabel.setKeepScreenOn(true);
		resetStatus.setKeepScreenOn(true);
		
	}
	
	private void logInfo() {
		
		Log.i(TAG, "Application Step : " + PadraApp.getStep());
		Log.i(TAG, "Device name : " + PadraApp.getInstance().getPreferencesString(PreferenceRefrences.DEVICE_NAME));
		Log.i(TAG, "Device phone number : " + PadraApp.getInstance().getPreferencesString(PreferenceRefrences.DEVICE_PHONE_NUMBER));
		Log.i(TAG, "Mobile Number : " + PadraApp.getInstance().getPreferencesString(PreferenceRefrences.MOBILE_NUMBER));
		
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
		// go for sending first SMS
		// send and receive authorization SMS
		resetStatus.setText("در حال ارسال پیام");
		
		sendBeginSMS();

	}
	
	private void sendBeginSMS() {
		mShared = PadraApp.preferences();
		
		TextMessageSenderMine sms = TextMessageSenderMine.getInstance(this);
		sms.sendSms(CommandType.suppress, null);
		
	}
	
	private void goToNextActivity() {
		
		PadraApp.setStep(4);

		Intent intent = new Intent(this, StepSixth.class);
		intent.putExtra("fresh_start", false);
		intent.addFlags(0x10000000);
		intent.addFlags(0x4000000);
		startActivity(intent);
		overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);
		finish();
	}
	
}
