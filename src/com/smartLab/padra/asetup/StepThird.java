package com.smartLab.padra.asetup;

import com.example.padra.R;
import com.smartLab.padra.PadraApp;
import com.smartLab.padra.PreferenceRefrences;
import com.smartLab.padra.SMS.CommandType;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class StepThird extends FragmentActivity {

	private static final String TAG = StepThird.class.getSimpleName();
	
	
	/**
	 * فاز اول: بازنشانی تنظیمات اولیه این فاز باید حتما قبل از همه انجام شود و
	 * باید ترتیب هم در آن رعایت شود. درنتیجه پیامک دوم ارسال نمی شود تا این که
	 * پاسخ پیامک اول دریافت شده باشد. اگر از برنامه هم خارج شویم باید این ترتیب
	 * حفظ شود. // 1. Reset. ==> reset123456 // 2. Begin. ==> begin123456 // 3.
	 * Authorization. ==> admin123456 number
	 */

	private SharedPreferences mShared;
	
	private TextView topLabel;
	private TextView resetStatus;
	private Button mContinue;

	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		Log.i(TAG, "onCreate");
		super.onCreate(arg0);
		setContentView(com.example.padra.R.layout.activity_step_first);

		topLabel = (TextView) findViewById(R.id.reset);
		resetStatus = (TextView) findViewById(R.id.reset_status);
		mContinue = (Button) findViewById(R.id.reset_continue);
		
		topLabel.setText(R.string.step_third);
		
		mContinue.setVisibility(View.GONE);
//		mContinue.setEnabled(false);
//		mContinue.setOnClickListener(new OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				goToNextActivity();
//			}
//		});
		
		logInfo();
		
		topLabel.setKeepScreenOn(true);
		resetStatus.setKeepScreenOn(true);
		
	}
	
	private void logInfo() {
		
		Log.i(TAG, "Application Step : " + PadraApp.getStep());
		Log.i(TAG, "Device name : " + PadraApp.getInstance().getPreferencesString(PreferenceRefrences.DEVICE_NAME));
		Log.i(TAG, "Device phone number : " + PadraApp.getInstance().getPreferencesString(PreferenceRefrences.DEVICE_PHONE_NUMBER));
		Log.i(TAG, "Mobile Number : " + PadraApp.getInstance().getPreferencesString(PreferenceRefrences.MOBILE_NUMBER));
		
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
		// go for sending first SMS
		// send and receive authorization SMS
		resetStatus.setText("در حال ارسال پیام");
		
		sendBeginSMS();

	}
	
	private void sendBeginSMS() {
		mShared = PadraApp.preferences();
		
		String number = mShared.getString(PreferenceRefrences.MOBILE_NUMBER, "");
		if (number == "" || number.length() == 0) {
			resetStatus.setText("شماره موبایل دچار اشکال شده است");
			return;
		}
		
		TextMessageSenderMine sms = TextMessageSenderMine.getInstance(this);
		sms.sendSms(CommandType.authorization, mShared.getString(PreferenceRefrences.MOBILE_NUMBER, number));
		
	}
	
//	private void goToNextActivity() {
//		
//		PadraApp.setStep(4);
//
//		Intent intent = new Intent(this, StepThird.class);
//		intent.putExtra("fresh_start", false);
//		intent.addFlags(0x10000000);
//		intent.addFlags(0x4000000);
//		startActivity(intent);
//		overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);
//		finish();
//	}
	
}
