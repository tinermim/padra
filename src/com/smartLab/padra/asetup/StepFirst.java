package com.smartLab.padra.asetup;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.example.padra.R;
import com.smartLab.padra.PadraApp;
import com.smartLab.padra.SMS.CommandType;

public class StepFirst extends FragmentActivity {

	private static final String TAG = StepFirst.class.getSimpleName();
	
	
	/**
	 * فاز اول: بازنشانی تنظیمات اولیه این فاز باید حتما قبل از همه انجام شود و
	 * باید ترتیب هم در آن رعایت شود. درنتیجه پیامک دوم ارسال نمی شود تا این که
	 * پاسخ پیامک اول دریافت شده باشد. اگر از برنامه هم خارج شویم باید این ترتیب
	 * 1. Reset. ==> reset123456
	 * 2. Begin. ==> begin123456 
	 * 3. Authorization. ==> admin123456 number
	 */

	private TextView resetStatus;
	private Button mContinue;
	//private ProgressBar progress;
	
	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		Log.i(TAG, "onCreate");
		
		super.onCreate(arg0);
		setContentView(com.example.padra.R.layout.activity_step_first);

		resetStatus = (TextView) findViewById(R.id.reset_status);
		//progress = (ProgressBar) findViewById(R.id.reset_progress);
		mContinue = (Button) findViewById(R.id.reset_continue);
		
		mContinue.setVisibility(View.GONE);
//		mContinue.setEnabled(false);
//		mContinue.setOnClickListener(new OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				goToNextActivity();
//			}
//		});
		
		//topLabel.setKeepScreenOn(true);
		resetStatus.setKeepScreenOn(true);
		
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		Log.i(TAG, "onResume");
		super.onResume();
		
		// go for sending first SMS
		// send and receive reset SMS
		resetStatus.setText("در حال ارسال پیام");
		
		sendResetSMS();

	}
	
	private void sendResetSMS() {
		Log.i(TAG, "sendResetSMS");
		TextMessageSenderMine sms = TextMessageSenderMine.getInstance(this);
		sms.sendSms(CommandType.reset, null);
		
	}
	
	private void goToNextActivity() {
		
		PadraApp.setStep(2);

		Intent intent = new Intent(this, StepSecond.class);
		intent.putExtra("fresh_start", false);
		intent.addFlags(0x10000000);
		intent.addFlags(0x4000000);
		startActivity(intent);
		overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);
		finish();
	}
	
	void showNotification() {	
		
        //get ref to NotificationManager
        //String ns = Context.NOTIFICATION_SERVICE;
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        
        //instantiate it
        int icon = R.drawable.ic_launcher; // notification_icon;
        CharSequence tickerText = "پیامک ریست ارسال شد";
        long when = System.currentTimeMillis();

        Notification notification = new Notification(icon, tickerText, when);
        
        //Define the notification's message and PendingIntent: 
        Context context = getApplicationContext();
        CharSequence contentTitle = "پادرا";
        CharSequence contentText = "وضعیت اس ام اس";
        Intent notificationIntent = new Intent();
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

        notification.setLatestEventInfo(context, contentTitle, contentText, contentIntent);
        
        int x = PendingIntent.FLAG_CANCEL_CURRENT;
        
        //Pass the Notification to the NotificationManager:
        int HELLO_ID = 100;
        mNotificationManager.notify(HELLO_ID, notification);
        
    }
	
	
	/*
	Intent intent = new Intent(context, StartPoint.class);
    intent.putExtra(NotificationDownloadService.APP_LINK, appLink);
    intent.putExtra("path", targetFile.getAbsolutePath());
    intent.putExtra(NotificationDownloadService.NOTIFY_ID, notifyId);
    notification.contentIntent = PendingIntent.getActivity(context, 0,
            intent,
            PendingIntent.FLAG_UPDATE_CURRENT);
    */
}
