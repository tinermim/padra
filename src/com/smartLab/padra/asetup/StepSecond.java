package com.smartLab.padra.asetup;

import com.example.padra.R;
import com.smartLab.padra.PadraApp;
import com.smartLab.padra.SMS.CommandType;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class StepSecond extends FragmentActivity {

	/**
	 * فاز اول: بازنشانی تنظیمات اولیه این فاز باید حتما قبل از همه انجام شود و
	 * باید ترتیب هم در آن رعایت شود. درنتیجه پیامک دوم ارسال نمی شود تا این که
	 * پاسخ پیامک اول دریافت شده باشد. اگر از برنامه هم خارج شویم باید این ترتیب
	 * حفظ شود. // 1. Reset. ==> reset123456 // 2. Begin. ==> begin123456 // 3.
	 * Authorization. ==> admin123456 number
	 */

	private TextView topLabel;
	private TextView resetStatus;
	private Button mContinue;

	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		super.onCreate(arg0);
		setContentView(com.example.padra.R.layout.activity_step_first);

		topLabel = (TextView) findViewById(R.id.reset);
		resetStatus = (TextView) findViewById(R.id.reset_status);
		mContinue = (Button) findViewById(R.id.reset_continue);
		
		topLabel.setText(R.string.step_second);

		mContinue.setVisibility(View.GONE);
//		mContinue.setEnabled(false);
//		mContinue.setOnClickListener(new OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				goToNextActivity();
//			}
//		});

		topLabel.setKeepScreenOn(true);
		resetStatus.setKeepScreenOn(true);
		
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
		// go for sending first sms
		// send and receive reset sms
		resetStatus.setText("در حال ارسال پیام");
		
		sendBeginSMS();

	}
	
	private void sendBeginSMS() {
		
		TextMessageSenderMine sms = TextMessageSenderMine.getInstance(this);
		sms.sendSms(CommandType.initialization, null);
		
	}
	
	private void goToNextActivity() {
		
		PadraApp.setStep(3);

		Intent intent = new Intent(this, StepThird.class);
		intent.putExtra("fresh_start", false);
		intent.addFlags(0x10000000);
		intent.addFlags(0x4000000);
		startActivity(intent);
		overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);
		finish();
	}
	
}
