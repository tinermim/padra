package com.smartLab.padra.asetup;

import java.util.ArrayList;
import java.util.Calendar;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.padra.R;
import com.google.gson.Gson;
import com.smartLab.padra.PadraApp;
import com.smartLab.padra.PreferenceRefrences;
import com.smartLab.padra.SMS.CommandItem;
import com.smartLab.padra.SMS.CommandType;
import com.smartLab.padra.SMS.CommandsToPersian;
import com.smartLab.padra.options.ArchiveItem;

public class TextMessageSenderMine {
	
	private static final String TAG = TextMessageSenderMine.class.getSimpleName();
	
	private static final int NUMBER_OF_COMMANDS = 50;
	private static final int NUMBER_OF_ARCHIVE = 100;

	public class LogWrapper {
		ArrayList<CommandItem> log = new ArrayList<CommandItem>();
	}

	public class ArchiveWrapper {
		ArrayList<ArchiveItem> log = new ArrayList<ArchiveItem>();
	}

	private int numberOfCommands;
	private Context mContext;
	private SharedPreferences mShared;
	private Editor mEditor;
	private Gson gson;
	private LogWrapper smsList = null;
	private ArchiveWrapper smsArchive = null;
	private static TextMessageSenderMine __instance = null;
	private ObjectAnimator animation;
	private CommandItem currentCommand;
	private CommandType currentCommandType = null;

	
	private RelativeLayout progressBarLayout;
	private TextView progressBarStatus;
	
	private TextView status;
	private ProgressBar progress;
	private Activity activity;
	
	public static TextMessageSenderMine getInstance(Activity activity) {
		__instance = new TextMessageSenderMine(activity);
		return __instance;
	}

	public TextMessageSenderMine(Activity activity) {
		this.activity = activity;
		mContext = activity.getApplicationContext();
		//mShared = PreferenceManager.getDefaultSharedPreferences(activity);
		
		mShared = PadraApp.preferences();
		mEditor = mShared.edit();
		gson = new Gson();
		
		try {
			
			progressBarLayout = (RelativeLayout) activity.findViewById(R.id.sendProgressBar);
			progressBarStatus = (TextView) activity.findViewById(R.id.sendStatus);
			
			status = (TextView) activity.findViewById(R.id.reset_status);
			progress = (ProgressBar) activity.findViewById(R.id.reset_progress);
			
		} catch (Exception e) {
			progressBarLayout = null;
			progressBarStatus = null;
			
			status = null;
			progress = null;
		}

		animation = new ObjectAnimator();
		retrieveLog();
		retrieveArchive();
		
	}

	private void storeLog() {
		String serializedMap = gson.toJson(smsList);
		mEditor.putString(PreferenceRefrences.SMS_COMMAND_LOG, serializedMap)
				.commit();
	}

	private void storeArchive() {
		String serializedMap = gson.toJson(smsArchive);
		mEditor.putString(PreferenceRefrences.SMS_COMMAND_ARCHIVE,
				serializedMap).commit();
	}

	private void retrieveArchive() {
		String wrapperStr = mShared.getString(PreferenceRefrences.SMS_COMMAND_ARCHIVE, "");
		if (!wrapperStr.equals("")) {
			smsArchive = gson.fromJson(wrapperStr, ArchiveWrapper.class);
		} else {
			smsArchive = new ArchiveWrapper();
			addArchiveItem(new ArchiveItem());
			retrieveArchive();
		}
	}

	public void addArchiveItem(ArchiveItem archive) {
		smsArchive.log.add(archive);
		if (smsArchive.log.size() > NUMBER_OF_ARCHIVE)
			smsArchive.log.remove(0);

		storeArchive();
	}

	public ArrayList<CommandItem> getCommandsLog() {
		return smsList.log;
	}

	public ArrayList<ArchiveItem> getArchiveLog() {
		return smsArchive.log;
	}

	private void retrieveLog() {
		String wrapperStr = mShared.getString(PreferenceRefrences.SMS_COMMAND_LOG, "");
		if (!wrapperStr.equals("")) {
			smsList = gson.fromJson(wrapperStr, LogWrapper.class);
			numberOfCommands = mShared.getInt(PreferenceRefrences.NUMBER_OF_SAVED_COMMANDS,
					NUMBER_OF_COMMANDS);
		} else {
			smsList = new LogWrapper();
			addCommand(new CommandItem());
			retrieveLog();
		}
	}

	public void addCommand(CommandItem command) {
		smsList.log.add(command);
		if (smsList.log.size() > numberOfCommands)
			smsList.log.remove(0);
		storeLog();
	}

	public void sendSms(CommandType c, String additionalInfo) {
		Log.i(TAG, "in sendSms, command type : " + c);
		
		String SENT = "SMS_SENT";
		String DELIVERED = "SMS_DELIVERED";
		currentCommandType = c;

		if (additionalInfo != null && additionalInfo.length() != 0 && additionalInfo.charAt(0) == '0' && !c.equals(CommandType.speedLimit)) {
			additionalInfo = "+98" + additionalInfo.substring(1);
		}

		PendingIntent sentPI = PendingIntent.getBroadcast(mContext, 0, new Intent(SENT), 0);

		PendingIntent deliveredPI = PendingIntent.getBroadcast(mContext, 0, new Intent(DELIVERED), 0);

		// ---when the SMS has been sent---
		mContext.registerReceiver(getSendBroadCast(), new IntentFilter(SENT));

		// ---when the SMS has been delivered---
		mContext.registerReceiver(getDeliveryBroadCast(), new IntentFilter(DELIVERED));

		String phoneNumber = mShared.getString(PreferenceRefrences.DEVICE_PHONE_NUMBER, "");
		String password = mShared.getString(PreferenceRefrences.DEVICE_PASSWORD, "");

		if (!phoneNumber.equals("")) {
			SmsManager sms = SmsManager.getDefault();
			String commandText = getTextFromCommand(c, password, additionalInfo);
			if (!password.equals("")) {
				if (!commandText.equals("")) {
					currentCommand = new CommandItem();
					currentCommand.type = c;
					currentCommand.sendTime = Calendar.getInstance();
					addCommand(currentCommand);
					
//					if (PadraApp.getStep() == 10) {
//						
//						if (progressBarLayout != null) {
//							progressBarStatus.setTextColor(0xffffffff);
//							showAndHideProgressBarAnimation();
//	
//							progressBarStatus.setText(mContext.getResources().getString(R.string.sending)
//								+ CommandsToPersian.getCommandMeaningToUser(currentCommandType, mContext));
//								
//						}
//					}
					
					if (status != null) {
						progress.setVisibility(View.VISIBLE);
						status.setText(mContext.getResources().getString(R.string.sending)
//								+ CommandsToPersian.getCommandMeaningToUser(currentCommandType, mContext)
								);
					}

					ArchiveItem archive = new ArchiveItem();
					archive.content = commandText;
					archive.phoneNumber = phoneNumber;
					addArchiveItem(archive);
					
					Log.i(TAG, "Command Text : " + commandText);
					
					sms.sendTextMessage(phoneNumber, null, commandText, sentPI, deliveredPI);
					
					Log.i(TAG, "Command Text after sent : " + commandText);
					
				} else {
					Toast.makeText(
							mContext,
							mContext.getResources().getString(
									R.string.commandNotFound),
							Toast.LENGTH_SHORT).show();
				}
			} else {
				Toast.makeText(mContext,
						mContext.getResources().getString(R.string.noPassword),
						Toast.LENGTH_SHORT).show();
			}
		} else {
			Toast.makeText(mContext,
					mContext.getResources().getString(R.string.noDeviceNumber),
					Toast.LENGTH_SHORT).show();
		}
	}

	private void showAndHideProgressBarAnimation() {
		progressBarLayout.setVisibility(View.VISIBLE);
		// sms going to send
		animation = ObjectAnimator.ofFloat(progressBarLayout, "alpha", 1)
				.setDuration(300);

		class onAnimationEndListener implements AnimatorListener {
			@Override
			public void onAnimationCancel(Animator arg0) {
			}

			@Override
			public void onAnimationEnd(Animator arg0) {
			}

			@Override
			public void onAnimationRepeat(Animator arg0) {
			}

			@Override
			public void onAnimationStart(Animator arg0) {
			}
		}

		animation.addListener(new onAnimationEndListener() {
			@Override
			public void onAnimationEnd(Animator animation) {
				animation = ObjectAnimator.ofFloat(progressBarLayout, "alpha",
						0).setDuration(300);
				animation.setStartDelay(10000);
				animation.addListener(new onAnimationEndListener() {

					@Override
					public void onAnimationEnd(Animator arg0) {
						progressBarLayout.setVisibility(View.GONE);
					}
				});
				animation.start();
			}
		});
		animation.start();
	}

	private BroadcastReceiver getSendBroadCast() {
		return new BroadcastReceiver() {
			@Override
			public void onReceive(Context arg0, Intent arg1) {
				Log.i(TAG, "getSendBroadCast : onRecieve result code: " + getResultCode());
				
				switch (getResultCode()) {

				case Activity.RESULT_OK:
//					if (progressBarLayout != null) {
//						progressBarStatus.setText(mContext.getResources().getString(R.string.sendingCheck)
//								+ CommandsToPersian.getCommandMeaningToUser(currentCommandType, mContext)
//								+ mContext.getResources().getString(R.string.toDevice));
					
					if (status != null) {
						status.setText(mContext.getResources().getString(R.string.sendingCheck)
//								+ CommandsToPersian.getCommandMeaningToUser(currentCommandType, mContext)
								+ mContext.getResources().getString(R.string.toDevice));
						
						//showAndHideProgressBarAnimation();
						
					}
					
					updateFirstNotSent(currentCommandType);
					storeLog();
					break;
				case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
//					if (progressBarLayout != null) {
//						progressBarStatus.setText("Generic Failure");
//						notSendAnimation();
//					}
					if (status != null) {
						//status.setText("Generic Failure");
						status.setText("برنامه موفق به ارسال پیامک نشد.");
						progress.setVisibility(View.GONE);
					}
					break;
				case SmsManager.RESULT_ERROR_NO_SERVICE:
//					if (progressBarLayout != null) {
//						progressBarStatus.setText(mContext.getResources().getString(R.string.noService));
//						notSendAnimation();
//					}
					if (status != null) {
						status.setText(mContext.getResources().getString(R.string.noService));
						progress.setVisibility(View.GONE);
					}
					break;
				case SmsManager.RESULT_ERROR_NULL_PDU:
//					if (progressBarLayout != null) {
//						progressBarStatus.setText("NULL PDU");
//						notSendAnimation();
//					}
					if (status != null) {
						status.setText("NULL PDU");
						progress.setVisibility(View.GONE);
					}
					break;
				case SmsManager.RESULT_ERROR_RADIO_OFF:
//					if (progressBarLayout != null) {
//						progressBarStatus.setText("Radio OFF");
//						notSendAnimation();
//					}
					if (status != null) {
						status.setText("Radio OFF");
						progress.setVisibility(View.GONE);
					}
					break;
				}

			}

			private void notSendAnimation() {
				if (progressBarLayout != null) {
					progressBarStatus.setTextColor(0x50ff0000);
					showAndHideProgressBarAnimation();
				}
			}
		};
	}

	private BroadcastReceiver getDeliveryBroadCast() {
		return new BroadcastReceiver() {
			@Override
			public void onReceive(Context arg0, Intent arg1) {
				Log.i(TAG, "getDeliveryBroadCast : onRecieve result code: " + getResultCode());
				if (progressBarLayout != null) {
					showAndHideProgressBarAnimation();
				}

				switch (getResultCode()) {
				case Activity.RESULT_OK:
//					if (progressBarLayout != null) {
//						progressBarStatus.setText(mContext.getResources().getString(R.string.command)
//								+ "" + mContext.getResources().getString(R.string.isSent));
//						progressBarStatus.setTextColor(0x5000ff00);
//					}
					if (status != null) {
						// در انتظار دریافت تایید از دستگاه ...
						StringBuilder builder = new StringBuilder();
						builder.append(mContext.getResources().getString(R.string.command));
						builder.append(mContext.getResources().getString(R.string.isSent));
						builder.append(".\n");
						builder.append(" در انتظار دریافت تایید از دستگاه ...");
//						status.setText(mContext.getResources().getString(R.string.command)
//								+ "" + mContext.getResources().getString(R.string.isSent));
						status.setText(builder.toString());
						//progress.setVisibility(View.GONE);
						//mContinue.setEnabled(true);
						
					}
					
					updateFirstNotdelivered(currentCommandType);
					storeLog();

					if (currentCommandType == CommandType.trackUnlimitedInterval) {
						// We reached end
						endOfInitialSteps();
					}
					
					break;
				case Activity.RESULT_CANCELED:
//					if (progressBarLayout != null) {
//						progressBarStatus.setText(mContext.getResources().getString(R.string.notDelivered));
//						progressBarStatus.setTextColor(0x50ff0000);
//					}
					if (status != null) {
						status.setText(mContext.getResources().getString(R.string.notDelivered));
						progress.setVisibility(View.GONE);
					}
					break;
				}
			}

		};
	}

	private void endOfInitialSteps() {
		
//		try {
//			StepEighth step = (StepEighth) activity;
//			step.showDialog();
//			
//		} catch (Exception e) {
//			// TODO: handle exception
//			
//			
//		}
		Log.i(TAG, "in endOfInitialSteps");
		
		if (PadraApp.getStep() == 8) {
			PadraApp.setStep(9);
	
			Intent intent = new Intent(activity, StepIMEIActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
			activity.startActivity(intent);
			activity.overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);
			activity.finish();
		
		}
		
		
//		AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
//		builder.setTitle(mContext.getResources().getString(R.string.message))
//			.setCancelable(false)
//			.setMessage(CommandsToPersian.getCommandMeaningToUser(currentCommandType, mContext))
//			.setPositiveButton(mContext.getResources().getString(R.string.accept),
//					new DialogInterface.OnClickListener() {
//						public void onClick(DialogInterface dialog, int id) {
//							dialog.cancel();
//							
//							PadraApp.setStep(9);
//							Intent intent = new Intent(PadraApp.getAppContext(), WelcomeActivity.class);
//							intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
//							mContext.startActivity(intent);
//							//finish();
//							
//						}
//					}).create().show();

		/*
		PadraApp.setStep(9);
		Intent intent = new Intent(PadraApp.getAppContext(), WelcomeActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
		mContext.startActivity(intent);
		//mContext.finish();
		*/
		
	}
	
	private void updateFirstNotSent(CommandType c) {
		retrieveLog();
		for (int i = smsList.log.size() - 1; i >= 0; i--)
			if (smsList.log.get(i).type == c
					&& smsList.log.get(i).isSent == false) {
				smsList.log.get(i).isSent = true;
				storeLog();
				return;
			}
	}

	private void updateFirstNotdelivered(CommandType c) {
		retrieveLog();
		for (int i = smsList.log.size() - 1; i >= 0; i--)
			if (smsList.log.get(i).type == c
					&& smsList.log.get(i).isDelivered == false
					&& smsList.log.get(i).isSent == true) {
				smsList.log.get(i).isDelivered = true;
				storeLog();
				return;
			}
	}

	public void updateFirstNotGotResponse(CommandType c) {
		retrieveLog();
		for (int i = smsList.log.size() - 1; i >= 0; i--)
			if (smsList.log.get(i).type == c
					&& smsList.log.get(i).isResponseReceived == false
					&& smsList.log.get(i).isDelivered == true
					&& smsList.log.get(i).isSent == true) {
				smsList.log.get(i).isResponseReceived = true;
				storeLog();
				return;
			}
	}

	private String getTextFromCommand(CommandType c, String password, String additionalInfo) {
		
		if (additionalInfo != null && additionalInfo.length() > 0 && additionalInfo.charAt(0) == '0' && !c.equals(CommandType.speedLimit)) {
			additionalInfo = "+98" + additionalInfo.substring(1);
		}

		String commandText = mContext.getResources().getString(c.getStringID());
		if (password.equals(""))
			return "";

		if (c.equals(CommandType.initialization))
			return commandText + password;

		if (c.equals(CommandType.checkState))
			return commandText + password;
		
		if (c.equals(CommandType.speedLimit))
			return commandText + password + " " + additionalInfo;

		if (c.equals(CommandType.changePassword))
			return commandText + password + " " + additionalInfo;

		if (c.equals(CommandType.PowerSystem))
			return commandText + password;

		if (c.equals(CommandType.resumePowerSystem))
			return commandText + password;

		if (c.equals(CommandType.authorization))
			return commandText + password + " " + additionalInfo;

		if (c.equals(CommandType.noQuickStop))
			return commandText + password;

		if (c.equals(CommandType.deleteAuthorization))
			return commandText + password + " " + additionalInfo;

		if (c.equals(CommandType.doorArm))
			return commandText + password;

		if (c.equals(CommandType.doorDisarm))
			return commandText + password;

		if (c.equals(CommandType.movementAlarm))
			return commandText + password + " " + "0200";

		if (c.equals(CommandType.movementAlarmCancel))
			return commandText + password;

		if (c.equals(CommandType.tracker))
			return commandText + password;

		if (c.equals(CommandType.monitor))
			return commandText + password;

		if (c.equals(CommandType.suppress))
			return commandText + password;

		if (c.equals(CommandType.postAddress))
			return commandText + password;

		if (c.equals(CommandType.undoSuppress))
			return commandText + password;

		if (c.equals(CommandType.adminIPSetup))
			return commandText + password + " " + additionalInfo;

		if (c.equals(CommandType.gprsSetup))
			return commandText + password;

		if (c.equals(CommandType.timeZoneSetup))
			return commandText + password + " " + additionalInfo;

		if (c.equals(CommandType.suppress))
			return commandText + password;

		if (c.equals(CommandType.trackUnlimitedInterval))
			return commandText + password;

		if (c.equals(CommandType.imei))
			return commandText + password;
		
		if (c.equals(CommandType.reset))
			return commandText + password;
		
		if (c.equals(CommandType.diag))
			return commandText + password;
		
		else
			return "";
	}

}
