package com.smartLab.padra.asetup;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.padra.R;
import com.smartLab.padra.PadraApp;
import com.smartLab.padra.PreferenceRefrences;

public class DeviceInfoActivity extends FragmentActivity {

	private static final String TAG = DeviceInfoActivity.class.getSimpleName();

	private static final String SERVER_IP = "130.185.72.114 8080";
	private SharedPreferences mShared;
	private Activity __Instance;
	private String carNameEntry, padraVersion, deviceNumber, previousPass;
	private boolean isCarNameValid, isPadraVersionValid, isDeviceNumberEntered,
			isPasswordLenghtValid, isDeviceNumberLogical, isPreviousPassValid,
			hasPreviousPassModified;

	private Spinner deviceType;
	
	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		Log.i(TAG, "onCreate");
		
		super.onCreate(arg0);
		setContentView(R.layout.activity_first_time_initialize);
		
		this.mShared = PadraApp.preferences();
		
		__Instance = this;
		
		
		Button button = (Button) findViewById(R.id.first_time_initialize_ok);
		button.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				okClicked();
			}
			
		});
	
		Button button2 = (Button) findViewById(R.id.first_time_initialize_change_number);
		button2.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				changeMyMobileNumber();
			}
			
		});
		
		List<String> list = new ArrayList<String>();
		list.add(getResources().getString(R.string.first_time_initialize_padra921));
		list.add(getResources().getString(R.string.first_time_initialize_padra922));
		list.add(getResources().getString(R.string.first_time_initialize_padra923));
		list.add(getResources().getString(R.string.first_time_initialize_padra934));
		list.add(getResources().getString(R.string.first_time_initialize_padra945));
		
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, list);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		
		deviceType = (Spinner) findViewById(R.id.device_spinner);
		deviceType.setAdapter(dataAdapter);
			
		
	}
	
	private void okClicked() {
		resetForNewEntry();
		
		getInput();
		
		validateInput();
		
		if (allEntryValid()) {
			if (hasPreviousPassModified) {
				confirmPreviousPass();
			} else {
				saveInput();
			}
		} else {
			Toast.makeText(__Instance, toastText(), Toast.LENGTH_LONG).show();
		}
	}
	
	private void confirmPreviousPass() {
		final Dialog mDialog = new Dialog(__Instance);
		mDialog.setContentView(R.layout.dialog_first_time_initialize_confirm_previous_pass);
		mDialog.setCancelable(true);
		mDialog.setTitle(__Instance.getResources().getString(
				R.string.first_time_confirmPassModify_title));
		Button mConfirm = (Button) mDialog
				.findViewById(R.id.first_time_passModify_ok);
		Button mCancel = (Button) mDialog
				.findViewById(R.id.first_time_passModify_cancel);
		mConfirm.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				mShared.edit()
						.putString(
								PreferenceRefrences.DEVICE_PASSWORD,
								previousPass).commit();
				saveInput();
				mDialog.dismiss();
				//dialog.dismiss();
			}
		});

		mCancel.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				EditText text = (EditText) findViewById(R.id.first_time_previous_password);
				text.setText("");
				previousPass = "";
				hasPreviousPassModified = false;
				mDialog.dismiss();
			}
		});
		mDialog.show();
	}
	
	private void getInput() {
		
		EditText text = (EditText) findViewById(R.id.first_time_carName);
		carNameEntry = text.getText().toString();
	
		text = (EditText) findViewById(R.id.first_time_simNumber);
		deviceNumber = text.getText().toString();
	
		text = (EditText) findViewById(R.id.first_time_previous_password);
		previousPass = text.getText().toString();
		
		/*
		RadioGroup g = (RadioGroup) findViewById(R.id.whichPadra);
		RadioButton b = (RadioButton) findViewById(g.getCheckedRadioButtonId());
		if (b != null) {
			padraVersion = b.getText().toString();
		} else {
			padraVersion = "";
		}
		*/
		
		padraVersion = deviceType.getSelectedItem().toString();
		
		Log.i(TAG, "padra carNameEntry is : " + carNameEntry);
		Log.i(TAG, "padra deviceNumber is : " + deviceNumber);
		Log.i(TAG, "padra previousPass is : " + previousPass);
		Log.i(TAG, "padra version is : " + padraVersion);
		
	}

	private void validateInput() {
		Log.i(TAG, "in validateInput");
		Log.i(TAG, "padra carNameEntry is : " + carNameEntry.length() + ", " + (carNameEntry.length() == 0)) ;
		Log.i(TAG, "padra deviceNumber is : " + deviceNumber.length());
		Log.i(TAG, "padra previousPass is : " + previousPass.length());
		Log.i(TAG, "padra version is : " + padraVersion.length());
		
		// && TextUtils.isEmpty(carNameEntry)
		if (carNameEntry == null || carNameEntry.length() == 0) {
			isCarNameValid = false;
		}
		
		if (deviceNumber.length() == 0) {
			isDeviceNumberEntered = false;
		}
		
		if (deviceNumber.length() != 11) {
			isDeviceNumberLogical = false;
		}
		
		if (padraVersion.length() == 0) {
			isPadraVersionValid = false;
		}
		
		if (previousPass.length() != 0 && previousPass.length() != 6) {
			isPreviousPassValid = false;
		} else if (!previousPass.equals("123456") && previousPass.length() != 0) {
			hasPreviousPassModified = true;
		}
		
		Log.i(TAG, "end of validateInput");
//		Log.i(TAG, "padra carNameEntry is : " + carNameEntry.length());
//		Log.i(TAG, "padra deviceNumber is : " + deviceNumber.length());
//		Log.i(TAG, "padra previousPass is : " + previousPass.length());
//		Log.i(TAG, "padra version is : " + padraVersion.length());
		
		Log.i(TAG, "isCarNameValid: " + isCarNameValid);
		Log.i(TAG, "isPadraVersionValid: " + isPadraVersionValid);
		Log.i(TAG, "isDeviceNumberEntered: " + isDeviceNumberEntered);
		Log.i(TAG, "isDeviceNumberLogical: " + isDeviceNumberLogical);
		Log.i(TAG, "isPasswordLenghtValid: " + isPasswordLenghtValid);
		Log.i(TAG, "isPreviousPassValid: " + isPreviousPassValid);
		
	}
	
	private boolean allEntryValid() {
		Log.i(TAG, "in all entry valid checker");
		Log.i(TAG, "isCarNameValid: " + isCarNameValid);
		Log.i(TAG, "isPadraVersionValid: " + isPadraVersionValid);
		Log.i(TAG, "isDeviceNumberEntered: " + isDeviceNumberEntered);
		Log.i(TAG, "isDeviceNumberLogical: " + isDeviceNumberLogical);
		Log.i(TAG, "isPasswordLenghtValid: " + isPasswordLenghtValid);
		Log.i(TAG, "isPreviousPassValid: " + isPreviousPassValid);
		
		return isCarNameValid && isPadraVersionValid && isDeviceNumberEntered
				&& isDeviceNumberLogical && isPasswordLenghtValid
				&& isPreviousPassValid;
	}

	private String toastText() {
		EditText text;
		String string = "";
		if (!(isCarNameValid || isPadraVersionValid || isDeviceNumberEntered || isPasswordLenghtValid || isDeviceNumberLogical))
			string = __Instance.getString(R.string.enterInformation);
		else if (!isPadraVersionValid)
			string = __Instance.getString(R.string.chooseDeviceType);
		else if (!isCarNameValid)
			string = __Instance.getString(R.string.enterCarName);
		else if (!isDeviceNumberEntered)
			string = __Instance.getString(R.string.enterDeviceNumber);
		else if (!isDeviceNumberLogical) {
			string = __Instance.getString(R.string.invalidDeviceNumber);
			text = (EditText) findViewById(R.id.first_time_simNumber);
			text.setText("");
		} else if (!isPreviousPassValid) {
			string = __Instance.getString(R.string.invalidPasswordLenght);
			text = (EditText) findViewById(R.id.first_time_previous_password);
			text.setText("");
		}
		resetForNewEntry();
		return string;
	}

	private void resetForNewEntry() {
		isCarNameValid = true;
		isPadraVersionValid = true;
		isDeviceNumberEntered = true;
		isPasswordLenghtValid = true;
		isDeviceNumberLogical = true;
		isPreviousPassValid = true;
		hasPreviousPassModified = false;
	}

	private void saveInput() {
		
		mShared.edit().putString(PreferenceRefrences.DEVICE_NAME, carNameEntry).commit();
		
		if (mShared.getBoolean(PreferenceRefrences.IS_FIRST_RUN, true)) {
			mShared.edit().putString(PreferenceRefrences.DEVICE_PHONE_NUMBER, deviceNumber).commit();
		}
		
		if (!hasPreviousPassModified) {
			mShared.edit().putString(PreferenceRefrences.DEVICE_PASSWORD, "123456").commit();
		}
	
		if (padraVersion.equals(__Instance.getResources().getString(R.string.first_time_initialize_padra921))) {
			mShared.edit().putInt(PreferenceRefrences.DEVICE_TYPE_KEY, 0).commit();
		
		} else if (padraVersion.equals(__Instance.getResources().getString(R.string.first_time_initialize_padra922))) {
			mShared.edit().putInt(PreferenceRefrences.DEVICE_TYPE_KEY, 1).commit();
		
		} else if (padraVersion.equals(__Instance.getResources().getString(R.string.first_time_initialize_padra923))) {
			mShared.edit().putInt(PreferenceRefrences.DEVICE_TYPE_KEY, 2).commit();
		
		} else if (padraVersion.equals(__Instance.getResources().getString(R.string.first_time_initialize_padra934))) {
			mShared.edit().putInt(PreferenceRefrences.DEVICE_TYPE_KEY, 3).commit();
		} else if (padraVersion.equals(__Instance.getResources().getString(R.string.first_time_initialize_padra945))) {
			mShared.edit().putInt(PreferenceRefrences.DEVICE_TYPE_KEY, 4).commit();
		}
		
		mShared.edit().putBoolean(PreferenceRefrences.INITIALIZE_SMS_NOT_SENT, false).commit();
		
		sendFirstRunSMS(true);
		
	}

	void changeMyMobileNumber() {
		
		mShared.edit().putString(PreferenceRefrences.MOBILE_NUMBER, null).commit();
		sendFirstRunSMS(false);
		
	}
	
	void sendFirstRunSMS(final boolean gotonext) {
		String number = "";
		number = mShared.getString(PreferenceRefrences.MOBILE_NUMBER, "");
		
		if (number.length() != 0 && number.length() > 9) {
			//firstInitializationCommands(number);
			if (gotonext) {
				goToNextActivity(number);
			}
//		} else {
//			TelephonyManager tm = (TelephonyManager) __Instance.getSystemService(Context.TELEPHONY_SERVICE);
//			number = tm.getLine1Number();
//
//			if (number != null && number.length() != 0) {
//				number = "+98" + number.substring(1);
//				mShared.edit().putString(PreferenceRefrences.MOBILE_NUMBER, number).commit();
//				//firstInitializationCommands(number);
//				if (gotonext) {
//					goToNextActivity(number);
//				}
			} else {
				final Dialog dialog = new Dialog(__Instance);
				dialog.setContentView(R.layout.layout_getting_user_number);
				dialog.setCancelable(false);
				dialog.setTitle(R.string.yourOwnNumber);
				dialog.show();

				Button ok = (Button) dialog.findViewById(R.id.carSim_ok);
				ok.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						EditText text = (EditText) dialog.findViewById(R.id.setting_car_simNum);
						String string = text.getText().toString();
						if (string != null && string.length() > 1) {
							string = "+98" + string.substring(1);
							mShared.edit().putString(PreferenceRefrences.MOBILE_NUMBER, string).commit();
						}
						//text.
						dialog.dismiss();
						//firstInitializationCommands(string);
						if (gotonext) {
							goToNextActivity(string);
						}
					}
				});
			}
		//}
		
	}

	private void goToNextActivity(final String number) {
		Log.i(TAG, "GO to next : mobile number : " + number);
		
		// ask for final check with a dialog
		StringBuilder builder = new StringBuilder();
		builder.append("شماره تلفن دستگاه: ");
		builder.append(mShared.getString(PreferenceRefrences.DEVICE_PHONE_NUMBER, null));
		builder.append("\n");
		builder.append("شماره موبایل خود: ");
		builder.append(mShared.getString(PreferenceRefrences.MOBILE_NUMBER, null));
		
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
		alertDialogBuilder.setTitle("بررسی نهایی");
		alertDialogBuilder.setMessage("آیا از اطلاعات زیر مطمين هستید؟\n" + builder.toString());
		alertDialogBuilder.setPositiveButton("بله",  new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				stepAside(number);
			}
		});
		alertDialogBuilder.setNegativeButton("خیر", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				// TODO Auto-generated method stub
				arg0.dismiss();
			}
		});
		
		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
		
	}
	
	private Handler next = new Handler(new Callback() {
		
		@Override
		public boolean handleMessage(Message msg) {
			// TODO Auto-generated method stub
			return false;
		}
	});
	
	private void stepAside(String number) {
		
		mShared.edit().putString(PreferenceRefrences.MOBILE_NUMBER, number).commit();
		//PadraApp.getInstance().putPreferencesString(PreferenceRefrences.MOBILE_NUMBER, number);
		
		Toast.makeText(__Instance, "اطلاعات شما ذخیره شد.", Toast.LENGTH_SHORT).show();
		
		PadraApp.setStep(1);

		next.postDelayed(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				Intent intent = new Intent(DeviceInfoActivity.this, StepFirst.class);
				intent.putExtra("fresh_start", false);
				intent.addFlags(0x10000000);
				intent.addFlags(0x4000000);
				startActivity(intent);
				overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);

				finish();

			}
		}, 500);
		
		/*
		mShared.edit().putString(PreferenceRefrences.MOBILE_NUMBER, number).commit();
		//PadraApp.getInstance().putPreferencesString(PreferenceRefrences.MOBILE_NUMBER, number);
		
		Toast.makeText(__Instance, "اطلاعات شما ذخیره شد.", Toast.LENGTH_SHORT).show();
		
		PadraApp.setStep(1);
		
		Intent intent = new Intent(this, StepFirst.class);
		intent.putExtra("fresh_start", false);
		intent.addFlags(0x10000000);
		intent.addFlags(0x4000000);
		startActivity(intent);
		overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);

		finish();
		*/
		
	}
	
	void closeKeyBoard(Context context) {
		
		InputMethodManager inputManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE); 
		inputManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
		
	}
	
}
