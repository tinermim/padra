package com.smartLab.padra;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;

import com.example.padra.R;
import com.smartLab.padra.asetup.WelcomeActivity;

public class PreferenceRefrences {

	public final static String FIRST_TIME_IN_MAP = "FIRST_TIME_IN_MAP";
	
	public final static String ALARM_TYPE_KEY = "ALARM_TYPE_KEY";
	public final static String TIME_INTERVAL_TO_SERVER = "TIME_INTERVAL_TO_SERVER";
	
	public final static String SHOW_RESPONSE_DIALOG = "SHOW_RESPONSE_DIALOG";

	public final static String IS_APP_PASSWORD_NEEDED_KEY = "IS_APP_PASSWORD_NEEDED";
	public final static String IS_IMEI_VALIDATED_KEY = "IS_IMEI_VALIDATED";
	public final static String APP_PASSWORD_KEY = "APP_PASSWORD_KEY";
	public final static String IS_LOOKING_FOR_CAR_LOCATION = "IS_LOOKING_FOR_CAR_LOCATION";
	public final static String LAST_TIME_LOCATION_UPDATE_KEY = "LAST_TIME_LOCATION_UPDATE";
	public final static String FIRST_RUN_HELP_INFO_KEY = "FIRST_RUN_HELP_INFO_KEY";
	public final static String LAST_LAT_KEY = "LAST_SAVED_LAT";
	public final static String LAST_LONG_KEY = "LAST_SAVED_LONG";
	public final static String MOBILE_NUMBER = "MOBILE_NUMBER";
	public final static String SPEED_LIMIT_KEY = "SPEED_LIMIT_KEY";
	public final static String DEVICE_PHONE_NUMBER = "DEVICE_PHONE_NUMBER";
	public final static String DEVICE_PASSWORD = "DEVICE_PASSWORD";
	public final static String IS_FIRST_RUN = "IS_FIRST_RUN";
	public final static String APP_VISIBILITY = "APP_VISIBLE";
	public final static String DEVICE_TYPE_KEY = "DEVICE_TYPE_KEY";
	public final static String NUMBER_OF_COMMANDS_TO_SHOW = "NUMBER_OF_COMMANDS_TO_SHOW";
	public final static String ALERT_TYPE_TO_SHOW = "WHAT_TO_SHOW";
	public final static String MAP_TYPE = "MAP_TYPE";
	public final static String CAR_VOICE_START_TIME = "CAR_VOICE_START_TIME";
	public final static String SMS_COMMAND_ARCHIVE = "SMS_COMMAND_ARCHIVE";
	public final static String SMS_COMMAND_LOG = "SMS_COMMAND_LOG";
	public final static String NUMBER_OF_SAVED_COMMANDS = "NUMBER_OF_SAVED_COMMANDS";
	public final static String DOORS_STATUS = "DOORS_STATUS";
	public final static String DEVICE_NAME = "DEVICE_NAME";
	public final static String DEVICE_VALID_USERS = "DEVICE_VALID_USERS";
	public final static String DEVICE_PASSWORD_REQ = "DEVICE_PASSWORD_REQ";
	public final static String ADMIN_DELETING_REQ = "ADMIN_DELETING_REQ";
	public final static String DEVICE_VALID_USERS_REQ = "DEVICE_VALID_USERS_REQ";
	public final static String BATTERY_STATUS = "BATTERY_STATUS";
	public final static String GSM_STATUS = "GSM_STATUS";
	public final static String OIL_STATUS = "OIL_STATUS";
	public final static String POWER_SYSTEM_STATUS = "POWER_SYSTEM_STATUS"; // state:
																			// ON
	public final static String CHECK_LAST_UPDATE = "CHECK_LAST_UPDATE";
	public final static String GPRS_STATUS = "GPRS_STATUS";
	public final static String GPS_STATUS = "GPS_STATUS";
	public final static String ACC_STATUS = "ACC_STATUS";
	public final static String INITIALIZE_SMS_NOT_SENT = "INITIALIZE_SMS_NOT_SENT";

	public static void resetApplicationPreferencesInfo(final Activity a,
			final SharedPreferences mShared) {

		new AsyncTask<Void, Void, String>() {

			ProgressDialog pDialog;

			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				// Showing progress dialog
				pDialog = new ProgressDialog(a);
				pDialog.setMessage(a.getString(R.string.remove_information));
				pDialog.setCancelable(false);
				pDialog.show();
			}

			@Override
			protected String doInBackground(Void... params) {
				mShared.edit()
						.putString(PreferenceRefrences.DEVICE_PASSWORD,
								"123456").commit();

				mShared.edit()
						.putBoolean(PreferenceRefrences.IS_IMEI_VALIDATED_KEY,
								false).commit();

				mShared.edit()
						.putString(PreferenceRefrences.DEVICE_PHONE_NUMBER, "")
						.commit();

				mShared.edit()
						.putString(PreferenceRefrences.SMS_COMMAND_ARCHIVE, "")
						.commit();

				mShared.edit()
						.putString(PreferenceRefrences.SMS_COMMAND_LOG, "")
						.commit();

				mShared.edit()
						.putString(PreferenceRefrences.BATTERY_STATUS, "?")
						.commit();

				mShared.edit().putString(PreferenceRefrences.GSM_STATUS, "?")
						.commit();

				mShared.edit().putString(PreferenceRefrences.GPRS_STATUS, "?")
						.commit();

				mShared.edit().putString(PreferenceRefrences.GPS_STATUS, "?")
						.commit();

				mShared.edit().putString(PreferenceRefrences.ACC_STATUS, "?")
						.commit();

				mShared.edit().putString(PreferenceRefrences.OIL_STATUS, "?")
						.commit();

				mShared.edit()
						.putString(PreferenceRefrences.POWER_SYSTEM_STATUS, "?")
						.commit();

				mShared.edit().putString(PreferenceRefrences.DOORS_STATUS, "?")
						.commit();

				mShared.edit()
						.putString(PreferenceRefrences.DEVICE_PASSWORD_REQ, "")
						.commit();

				mShared.edit()
						.putString(PreferenceRefrences.DEVICE_VALID_USERS, "")
						.commit();

				mShared.edit()
						.putString(PreferenceRefrences.DEVICE_VALID_USERS_REQ,
								"").commit();

				mShared.edit()
						.putBoolean(PreferenceRefrences.IS_FIRST_RUN, true)
						.commit();
				mShared.edit()

				.putBoolean(PreferenceRefrences.INITIALIZE_SMS_NOT_SENT, true)
						.commit();

				return null;
			}

			protected void onPostExecute(String result) {
				pDialog.dismiss();
				a.finish();
				
				/// FD
				/// going to first step
				PadraApp.setStep(0);
				Intent intent = new Intent(PadraApp.getAppContext(), WelcomeActivity.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
				PadraApp.getAppContext().startActivity(intent);
				/// -FD
				
			};
		}.execute();
	}

}
