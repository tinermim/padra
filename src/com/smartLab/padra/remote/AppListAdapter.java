package com.smartLab.padra.remote;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.padra.R;
import com.smartLab.padra.utils.ListItem;

public class AppListAdapter extends BaseAdapter {
	private LayoutInflater mInflater;
	private List<ListItem> mItems;

	public AppListAdapter(Context context, List<ListItem> items) {
		mInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.mItems = items;
	}

	@Override
	public int getCount() {
		return mItems.size();
	}

	@Override
	public Object getItem(int position) {
		return mItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		return mItems.get(position).id;
	}

	/* private view holder class */
	private class ViewHolder {
		ImageView imageView;
		TextView appName;
	}

	@SuppressWarnings("deprecation")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		ListItem item = (ListItem) getItem(position);

		convertView = mInflater.inflate(R.layout.drawer_item, parent, false);
		holder = new ViewHolder();
		holder.appName = (TextView) convertView.findViewById(R.id.title);
		holder.imageView = (ImageView) convertView.findViewById(R.id.icon);

		holder.appName.setText(item.text);
		int sdk = android.os.Build.VERSION.SDK_INT;
		if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN)
			holder.imageView.setBackgroundDrawable(item.icon);
		else
			holder.imageView.setBackground(item.icon);

		return convertView;
	}
}
