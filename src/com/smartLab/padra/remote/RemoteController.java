package com.smartLab.padra.remote;

import java.util.LinkedList;
import java.util.List;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.padra.R;
import com.smartLab.padra.BaseSliderFragment;
import com.smartLab.padra.PreferenceRefrences;
import com.smartLab.padra.SMS.CommandType;
import com.smartLab.padra.SMS.TextMessageSender;
import com.smartLab.padra.support.SupportActivity;
import com.smartLab.padra.utils.ImageLoader;
import com.smartLab.padra.utils.ListItem;
import com.smartLab.setting.SettingActivity;

public class RemoteController extends BaseSliderFragment {
	private static final int COLOR_DETECT_THRESHOLD = 40;
	private long black = 0;
	private long green = 0;
	private long red = 0;
	private long violet = 0;
	private long yellow = 0;
	private long brown = 0;
	private int downX = 0;
	private int downY = 0;
	public int width;
	public int height;
	public static Bitmap hotspots;
	public static FoldingPaneLayout mPaneLayout;
	public ListView mPaneList;
	private static FrameLayout frame;
	private static SharedPreferences mShared;
	private static List<ListItem> appsData;
	private int deviceType;
	private ImageView image_black;
	private ImageView image_green;
	private ImageView image_yellow;
	private ImageView image_violet;
	private ImageView image_brown;
	private ImageView image_red;
	private ImageView remoteImageArea;

	// TODO
	@Override
	public void onResume() {
		
		mShared = PreferenceManager.getDefaultSharedPreferences(getActivity()
				.getApplicationContext());

		deviceType = mShared.getInt(PreferenceRefrences.DEVICE_TYPE_KEY, 3);
		Log.i(TAG, "in onResume, deviceType; " + deviceType);
		
		/// handling 
		ImageView alarmOff = (ImageView) getActivity().findViewById(R.id.alarm_status);
		if (mShared.getInt(PreferenceRefrences.ALARM_TYPE_KEY, 0) == 1) {
			alarmOff.setVisibility(View.VISIBLE);
		} else {
			alarmOff.setVisibility(View.GONE);
		}
		
		frame = (FrameLayout) getActivity().findViewById(R.id.my_frame_remote);
		
		final ImageView blackButton = ((ImageView) (getActivity()
				.findViewById(R.id.black)));
		final ImageView greenButton = ((ImageView) (getActivity()
				.findViewById(R.id.green)));
		final ImageView yellowButton = ((ImageView) (getActivity()
				.findViewById(R.id.yellow)));
		final ImageView violetButton = ((ImageView) (getActivity()
				.findViewById(R.id.violet)));
		final ImageView brownButton = ((ImageView) (getActivity()
				.findViewById(R.id.brown)));
		final ImageView redButton = ((ImageView) (getActivity()
				.findViewById(R.id.red)));

		setDrawableAnimation(R.id.animationSwipeLeft, R.drawable.animation_left);

		final OnTouchListener changeColorListener = new OnTouchListener() {
			@Override
			public boolean onTouch(final View v, MotionEvent ev) {
				final int action = ev.getAction();

				final int evX = (int) ev.getX();
				final int evY = (int) ev.getY();
				final int touchColor = getHotspotColor(evX, evY);
				if (touchColor == -1)
					return true;

				Runnable handle = new Runnable() {
					@Override
					public void run() {
						switch (action) {

						case MotionEvent.ACTION_UP:

							if (evX - downX > 140 || evY - downY > 140)
								mPaneLayout.openPane();

							TextMessageSender sender = TextMessageSender
									.getInstance(getActivity());

							if ((int) Math.abs(touchColor - 0xff000000) < COLOR_DETECT_THRESHOLD
									&& black == 1) {
								if (isCommandValid(CommandType.movementAlarmCancel))
									sender.sendSms(
											CommandType.movementAlarmCancel,
											null);
								else
									notValidOperationToast();
							} else if ((int) Math.abs(touchColor - 0xff00ff00) < COLOR_DETECT_THRESHOLD
									&& green == 1) {
								if (isCommandValid(CommandType.carTrunk))
									Toast.makeText(
											getActivity(),
											getActivity().getResources()
													.getString(
															R.string.carTrunk),
											Toast.LENGTH_SHORT).show();
								else
									notValidOperationToast();
							} else if ((int) Math.abs(touchColor - 0xffff0000) < COLOR_DETECT_THRESHOLD
									&& red == 1) {
								if (isCommandValid(CommandType.movementAlarm))
									sender.sendSms(CommandType.movementAlarm,
											null);
								else
									notValidOperationToast();
							} else if ((int) Math.abs(touchColor - 0xffcc00cc) < COLOR_DETECT_THRESHOLD
									&& violet == 1) {
								if (isCommandValid(CommandType.doorDisarm))
									sender.sendSms(CommandType.doorDisarm, null);
								else
									notValidOperationToast();
							} else if ((int) Math.abs(touchColor - 0xffffff33) < COLOR_DETECT_THRESHOLD
									&& yellow == 1) {
								if (isCommandValid(CommandType.doorArm))
									sender.sendSms(CommandType.doorArm, null);
								else
									notValidOperationToast();

							} else if ((int) Math.abs(touchColor - 0xff663300) < COLOR_DETECT_THRESHOLD
									&& brown == 1)
								if (isCommandValid(CommandType.start))
									sender.sendSms(CommandType.start, null);
								else
									notValidOperationToast();

							setButtonsOnClicked();

							break;
						case MotionEvent.ACTION_DOWN:
							downX = evX;
							downY = evY;

							if ((int) Math.abs(touchColor - 0xff000000) < COLOR_DETECT_THRESHOLD) {
								blackButton.setVisibility(View.VISIBLE);
								black = 1;
							} else if ((int) Math.abs(touchColor - 0xff00ff00) < COLOR_DETECT_THRESHOLD) {
								greenButton.setVisibility(View.VISIBLE);
								green = 1;
							} else if ((int) Math.abs(touchColor - 0xffff0000) < COLOR_DETECT_THRESHOLD) {
								redButton.setVisibility(View.VISIBLE);
								red = 1;
							} else if ((int) Math.abs(touchColor - 0xffcc00cc) < COLOR_DETECT_THRESHOLD) {
								violetButton.setVisibility(View.VISIBLE);
								violet = 1;
							} else if ((int) Math.abs(touchColor - 0xffffff33) < COLOR_DETECT_THRESHOLD) {
								yellowButton.setVisibility(View.VISIBLE);
								yellow = 1;
							} else if ((int) Math.abs(touchColor - 0xff663300) < COLOR_DETECT_THRESHOLD) {
								brownButton.setVisibility(View.VISIBLE);
								brown = 1;
							}

							break;

						case MotionEvent.ACTION_MOVE:
							if (black == 1
									&& (int) Math.abs(touchColor - 0xff000000) > COLOR_DETECT_THRESHOLD
									|| green == 1
									&& (int) Math.abs(touchColor - 0xff00ff00) > COLOR_DETECT_THRESHOLD
									|| black == 1
									&& (int) Math.abs(touchColor - 0xff000000) > COLOR_DETECT_THRESHOLD
									|| yellow == 1
									&& (int) Math.abs(touchColor - 0xffffff33) > COLOR_DETECT_THRESHOLD
									|| violet == 1
									&& (int) Math.abs(touchColor - 0xffcc00cc) > COLOR_DETECT_THRESHOLD
									|| brown == 1
									&& (int) Math.abs(touchColor - 0xff663300) > COLOR_DETECT_THRESHOLD
									|| red == 1
									&& (int) Math.abs(touchColor - 0xffff0000) > COLOR_DETECT_THRESHOLD)
								setButtonsOnClicked();

							break;
						default:
							setButtonsOnClicked();
							break;

						} // end switch

					}

					private boolean isCommandValid(CommandType type) {
						if (deviceType != 3)
							if (type.equals(CommandType.carTrunk)
									|| type.equals(CommandType.start))
								return false;

						if (deviceType == 0 || deviceType == 2)
							if (type.equals(CommandType.doorArm)
									|| type.equals(CommandType.doorDisarm))
								return false;

						return true;
					}

					private void notValidOperationToast() {
						Toast.makeText(
								getActivity(),
								getActivity().getResources().getString(
										R.string.notValidOperation),
								Toast.LENGTH_SHORT).show();
					}
				};

				handle.run();

				return true;

			}

			public int getHotspotColor(int x, int y) {
				if (hotspots == null || hotspots.getHeight() < y || y < 0
						|| hotspots.getWidth() < x || x < 0) {
					setButtonsOnClicked();
					return -1;
				}
				try {
					return hotspots.getPixel(x, y);
				} catch (Exception e) {
					return -1;
				}
				
			}

			private void setButtonsOnClicked() {
				black = 0;
				green = 0;
				violet = 0;
				red = 0;
				yellow = 0;
				brown = 0;

				blackButton.setVisibility(View.INVISIBLE);
				yellowButton.setVisibility(View.INVISIBLE);
				greenButton.setVisibility(View.INVISIBLE);
				redButton.setVisibility(View.INVISIBLE);
				brownButton.setVisibility(View.INVISIBLE);
				violetButton.setVisibility(View.INVISIBLE);
			}
		};

		frame.post(new Runnable() {
			@Override
			public void run() {
				// TODO
				if (hotspots == null)
					setHotspot();

				if (remoteImageArea == null)
					setRemoteBackground();

				if (image_black == null)
					configureButton(image_black, R.id.black,
							R.drawable.remote_black_clicked);
				if (image_green == null)
					configureButton(image_green, R.id.green,
							R.drawable.remote_green_clicked);
				if (image_yellow == null)
					configureButton(image_yellow, R.id.yellow,
							R.drawable.remote_yellow_clicked);
				if (image_violet == null)
					configureButton(image_violet, R.id.violet,
							R.drawable.remote_violet_clicked);
				if (image_brown == null)
					configureButton(image_brown, R.id.brown,
							R.drawable.remote_brown_clicked);
				if (image_red == null)
					configureButton(image_red, R.id.red,
							R.drawable.remote_red_clicked);

				frame.setOnTouchListener(changeColorListener);
				frame.setDrawingCacheEnabled(true);
			}

			private void configureButton(ImageView image, int imageViewId,
					int backgournId) {
				new ImageLoader(getActivity(), ((ImageView) (getActivity().findViewById(imageViewId))), backgournId, 1).execute();
			}

			private void setHotspot() {
				width = frame.getWidth();
				height = frame.getHeight();

				new AsyncTask<Void, Void, Void>() {
					int sampleSize = 2;

					@Override
					protected Void doInBackground(Void... params) {
						loadBitmapSafety(sampleSize);
						return null;
					}

					private void loadBitmapSafety(int sSize) {
						BitmapFactory.Options ops = new BitmapFactory.Options();
						ops.inSampleSize = sSize;
						try {
							Bitmap temp = BitmapFactory.decodeResource(
									getResources(),
									R.drawable.remote_color_invisible);
							hotspots = Bitmap.createScaledBitmap(temp, width,
									height, false);
						} catch (OutOfMemoryError e) {
							if (sampleSize != 6)
								loadBitmapSafety(sSize + 1);

						} catch (Exception e) {
						}
					}

				}.execute();
			}

			private void setRemoteBackground() {
				System.out.println("in setRemoteBackground");
				Log.i(TAG, "in setRemoteBackground, deviceType:" + deviceType);
				//Toast.makeText(getActivity(), "setRemoteBackground, deviceType:" + deviceType, Toast.LENGTH_SHORT).show();
				
				int padra_remote_type = R.drawable.remote_921_923;

				if (deviceType == 1 || deviceType == 4)
					padra_remote_type = R.drawable.remote_922;
				else if (deviceType == 3)
					padra_remote_type = R.drawable.remote_934;
					
				new ImageLoader(getActivity(), ((ImageView) (getActivity()
						.findViewById(R.id.image_areas))), padra_remote_type, 1).execute();
				
			}
		});

		initDrawer();
		super.onResume();
	}

	private void initDrawer() {
		mPaneLayout = (FoldingPaneLayout) getActivity().findViewById(
				R.id.drawer_layout);
		mPaneList = (ListView) getActivity().findViewById(R.id.left_drawer);
		mPaneLayout.getFoldingLayout().setNumberOfFolds(4);
		mPaneLayout.foldingNavigationLayout.setBackgroundColor(Color.BLACK);
		// set up the drawer's list view with items and click listener
		appsData = new LinkedList<ListItem>();
		appsData.add(new ListItem(0, getActivity().getResources().getString(
				R.string.settings), getActivity().getResources().getDrawable(
				R.drawable.icon)));
		appsData.add(new ListItem(1, getActivity().getResources().getString(
				R.string.support), getActivity().getResources().getDrawable(
				R.drawable.contact_us)));
		AppListAdapter adapter = new AppListAdapter(getActivity()
				.getApplicationContext(), appsData);
		mPaneList.setAdapter(adapter);
		mPaneList.setOnItemClickListener(new DrawerItemClickListener());
	}

	public static BaseSliderFragment newInstance(int position) {
		BaseSliderFragment f = new RemoteController();
		Bundle bundle = new Bundle();
		bundle.putInt(POSITION_KEY, position);
		f.setArguments(bundle);
		return f;
	}

	@Override
	protected View getLayout(LayoutInflater inflater, ViewGroup container,
			boolean b) {
		return inflater.inflate(R.layout.fragments_remote_controller_view,
				container, false);
	}

	/* The click listner for ListView in the navigation drawer */
	private class DrawerItemClickListener implements
			ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			selectItem(position);
		}
	}

	private void selectItem(int position) {
		Toast.makeText(getActivity().getApplicationContext(),
				appsData.get(position).text, Toast.LENGTH_SHORT);
		if (position == 0) {
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					Intent myIntent = new Intent(getActivity()
							.getApplicationContext(), SettingActivity.class);
					getActivity().startActivity(myIntent);
				}
			}, 200);
		} else if (position == 1) {
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					Intent myIntent = new Intent(getActivity()
							.getApplicationContext(), SupportActivity.class);
					getActivity().startActivity(myIntent);
				}
			}, 200);
		}
		mPaneLayout.closePane();
	}

}