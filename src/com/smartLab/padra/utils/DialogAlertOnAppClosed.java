package com.smartLab.padra.utils;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.Button;

import com.example.padra.R;
import com.smartLab.padra.MainActivity;
import com.smartLab.padra.SMS.CommandType;

public class DialogAlertOnAppClosed extends DialogAlert {
	private Button yes;
	private Button no;

	public DialogAlertOnAppClosed(Activity a, String bodyMessage,CommandType c) {
		super(a, bodyMessage,c);
	}

	@Override
	protected void setButtons() {
		yes = (Button) findViewById(R.id.btn_yes);
		no = (Button) findViewById(R.id.btn_no);
		yes.setOnClickListener(this);
		no.setOnClickListener(this);
	}

	@Override
	protected void handleButtonsClick(View v) {
		Intent intent;
		switch (v.getId()) {
		case R.id.btn_yes:
			intent = new Intent(c, MainActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);		
			c.startActivity(intent);
			c.finish();
			break;
		case R.id.btn_no:
			c.finish();
			dismiss();
			return;

		}
	}

	@Override
	protected void setLayoutID() {
		layoutID = R.layout.dialog_alert_alarm_app_closed;
	}
}
