package com.smartLab.padra.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.PowerManager;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.TextView;

import com.example.padra.R;
import com.smartLab.padra.PadraApp;
import com.smartLab.padra.PreferenceRefrences;
import com.smartLab.padra.SMS.CommandType;
import com.smartLab.padra.SMS.TextMessageSender;

/*
 * Here is the implementation of the dialog which is shown any where that we should ask the customer to buy
 * the related component(package). any pro feature which is not purchased will show this dialog, through which we handle the purchase 
 * process and do IAP
 */
public abstract class DialogAlert extends Dialog implements
		android.view.View.OnClickListener {
	protected Activity c;
	protected Dialog d;
	protected MediaPlayer media;
	protected String message;
	protected int layoutID;
	protected Window wind;
	protected CommandType alarmType;

	public DialogAlert(Activity a, String bodyMessage, CommandType alarmType) {
		super(a);
		this.c = a;
		message = bodyMessage;
		this.alarmType = alarmType;
	}

	private void prepareMediaAlarm() {
		media = MediaPlayer.create(c.getApplicationContext(), R.raw.alarm);
		AudioManager audioManager = (AudioManager) c
				.getSystemService(Context.AUDIO_SERVICE);
		int maxVolumeMusic = audioManager
				.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
		audioManager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
		audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, maxVolumeMusic,
				AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);
		media.setLooping(true);
		media.setAudioStreamType(AudioManager.STREAM_MUSIC);
		media.setVolume(1.0f, 1.0f);
		media.setWakeMode(c.getApplicationContext(),
				PowerManager.PARTIAL_WAKE_LOCK);
		media.start();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setLayoutID();
		setContentView(layoutID);
		setButtons();
		setCancelable(false);
		((TextView) findViewById(R.id.alertMessage)).setText(message);
		turnScreenOn();
		
		if (PadraApp.getInstance().getPreferencesInt(PreferenceRefrences.ALARM_TYPE_KEY, 0) == 0) {
			prepareMediaAlarm();
		}
		
	}

	private void turnScreenOn() {
		WindowManager.LayoutParams params = c.getWindow().getAttributes();
		params.flags = WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
		params.screenBrightness = 0.9f;
		c.getWindow().setAttributes(params);

		wind = this.getWindow();
		wind.addFlags(LayoutParams.FLAG_DISMISS_KEYGUARD);
		wind.addFlags(LayoutParams.FLAG_SHOW_WHEN_LOCKED);
		wind.addFlags(LayoutParams.FLAG_TURN_SCREEN_ON);
	}

	abstract protected void setLayoutID();

	abstract protected void setButtons();

	@Override
	public void onClick(View v) {
		terminatePreparation();
		handleButtonsClick(v);
		alarmHandlePostActions();
	}

	private void alarmHandlePostActions() {
		if (alarmType == CommandType.speedLimitAlarmActivated) {
//			TextMessageSender.getInstance(c).sendSms(CommandType.doorDisarm,
//					null);
//			TextMessageSender.getInstance(c).sendSms(CommandType.speedLimit,
//					"120");
			
			TextMessageSender.getInstance(c).sendSms(CommandType.nospeed,
					null);
			
		} else if (alarmType == CommandType.doorOpenAlarmActivated
				|| alarmType == CommandType.shockSensorAlarmActivated
				|| alarmType == CommandType.accidentAlarmActivated
				|| alarmType == CommandType.accOnAlarmActivated
				) {
			
//			TextMessageSender.getInstance(c).sendSms(CommandType.doorArm, null);
			TextMessageSender.getInstance(c).sendSms(CommandType.doorDisarm, null);
			
		} else if (alarmType == CommandType.helpMeAlarmActivated) {
			TextMessageSender.getInstance(c).sendSms(CommandType.helpme, null);
		}
		
	}

	abstract protected void handleButtonsClick(View v);

	protected void terminatePreparation() {
		try {
			if (media != null) {
				media.stop();
				media.release();
				media = null;
			}
			dismiss();
		} catch (Exception e) {
			// TODO: handle exception
		}

	}
}
