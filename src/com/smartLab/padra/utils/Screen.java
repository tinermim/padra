package com.smartLab.padra.utils;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.view.WindowManager;

public abstract class Screen {
	
	public static int dpToPixels(int dps,Context context){
		
		float scale = context.getResources().getDisplayMetrics().density;
		return (int) (dps * scale + 0.5f);
	}
	
	public static void dimScreen(final float brightness,int delay_msec,final Activity activity) {
		
		new Handler().postDelayed(new Runnable() {
			
			@Override
			public void run() {
				WindowManager.LayoutParams WMLP = activity.getWindow().getAttributes();
				WMLP.screenBrightness = brightness;
				activity.getWindow().setAttributes(WMLP);
			}
		},delay_msec);
	}
}
