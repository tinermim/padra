package com.smartLab.padra.utils;

import java.lang.ref.WeakReference;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

public class ImageLoader extends AsyncTask<Void, Void, Bitmap> {
	private int imageViewId;
	private final WeakReference<ImageView> holder;
	private Context context;
	private int sampleSize;

	public ImageLoader(Context context, ImageView holder, int ImageViewId,
			int sampleSize) {
		this.holder = new WeakReference<ImageView>(holder);
		this.imageViewId = ImageViewId;
		this.context = context;
		this.sampleSize = sampleSize;
	}

	@Override
	protected Bitmap doInBackground(Void... params) {
		return loadBitmapSafety(sampleSize);
	}

	private Bitmap loadBitmapSafety(int sSize) {
		BitmapFactory.Options ops = new BitmapFactory.Options();
		ops.inSampleSize = sSize;
		try {
			return BitmapFactory.decodeResource(context.getResources(),
					imageViewId, ops);
		} catch (OutOfMemoryError e) {
			if (sampleSize == 6)
				return null;
			return loadBitmapSafety(sSize + 1);

		} catch (Exception e) {
			return null;
		}
	}

	@Override
	protected void onPostExecute(Bitmap result) {
		if (holder != null && result != null) {
			final ImageView imageView = holder.get();
			if (imageView != null) {
				imageView.setImageBitmap(result);
			}
		}

		super.onPostExecute(result);
	}
}