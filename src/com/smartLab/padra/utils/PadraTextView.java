package com.smartLab.padra.utils;

import java.util.Locale;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;



public class PadraTextView extends TextView {

	public PadraTextView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		if (Locale.getDefault().getLanguage().equals("fa"))
			this.setTypeface(Fonts.getYekan(context));
		else
			this.setTypeface(Fonts.getRobotLight(context));

	}

	public PadraTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		if (Locale.getDefault().getLanguage().equals("fa"))
			this.setTypeface(Fonts.getYekan(context));
		else
			this.setTypeface(Fonts.getRobotLight(context));
	}

	public PadraTextView(Context context) {
		super(context);
		if (Locale.getDefault().getLanguage().equals("fa"))
			this.setTypeface(Fonts.getYekan(context));
		else
			this.setTypeface(Fonts.getRobotLight(context));
	}

}
