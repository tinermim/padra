package com.smartLab.padra.utils;

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.View;

public abstract class BaseFragment extends Fragment {
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setupActionBar();
	}
	
	public static void setupActionBar(Activity activity, final View actionBarLayout) {
		android.app.ActionBar actionbar = ((FragmentActivity)activity).getActionBar();
		actionbar.setDisplayShowHomeEnabled(false);
		actionbar.setDisplayShowTitleEnabled(false);
		actionbar.setDisplayHomeAsUpEnabled(false);
		actionbar.setDisplayShowCustomEnabled(true);

		Drawable lastActionBarBg = null;
		if (actionbar.getCustomView() != null)
			lastActionBarBg = actionbar.getCustomView().getBackground();
		int colorFrom = (lastActionBarBg instanceof ColorDrawable) ? ((ColorDrawable) lastActionBarBg)
				.getColor() : Color.BLACK;

		Drawable actionBarBg = actionBarLayout.getBackground();
		int colorTo = (actionBarBg instanceof ColorDrawable) ? ((ColorDrawable) actionBarBg)
				.getColor() : Color.BLACK;
		actionbar.setCustomView(actionBarLayout);
				
		ValueAnimator colorAnimation = ValueAnimator.ofObject(
				new ArgbEvaluator(), colorFrom, colorTo);
		colorAnimation.addUpdateListener(new AnimatorUpdateListener() {

			@Override
			public void onAnimationUpdate(ValueAnimator animator) {
				actionBarLayout.setBackgroundColor((Integer) animator
						.getAnimatedValue());
			}

		});
		colorAnimation.start();
	}

	private void setupActionBar() {
		setupActionBar(getActivity(), getActionBarView());
	}

	protected abstract View getActionBarView();

}
