
package com.smartLab.padra.utils;

import android.app.Activity;
import android.view.View;
import android.widget.Button;

import com.example.padra.R;
import com.smartLab.padra.SMS.CommandType;


public class DialogAlertOnAppRun extends DialogAlert {
	public DialogAlertOnAppRun(Activity a, String bodyMessage,CommandType c) {
		super(a, bodyMessage,c);
	}

	private Button ok;
	@Override
	protected void setButtons() {
		ok = (Button) findViewById(R.id.btn_no);
		ok.setOnClickListener(this);
	}

	@Override
	protected void handleButtonsClick(View v) {
		c.finish();
	}
	protected void setLayoutID() {
		layoutID = R.layout.dialog_alert_alarm_app_run;	
	}
	
	
}
