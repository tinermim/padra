package com.smartLab.padra.utils;

import android.content.Context;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.RelativeLayout;

public class TextSendingStatusView extends RelativeLayout {
//	private static final int SWIPE_MIN_DISTANCE = 10;
//	private static final int SWIPE_THRESHOLD_VELOCITY = 20;

	public TextSendingStatusView(Context context, AttributeSet attrs,
			int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	public TextSendingStatusView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public TextSendingStatusView(Context context) {
		super(context);
		init();

	}

	private int _yDelta;

	private void init() {

		setOnLongClickListener(new OnLongClickListener() {
			
			@Override
			public boolean onLongClick(View arg0) {
//TODO writng longclick to enable confirm button
				return false;
			}
		});

		setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View view, MotionEvent event) {
				final int Y = (int) event.getRawY();
				switch (event.getAction() & MotionEvent.ACTION_MASK) {
				case MotionEvent.ACTION_DOWN:
					RelativeLayout.LayoutParams lParams = (RelativeLayout.LayoutParams) view
							.getLayoutParams();
					_yDelta = Y - lParams.topMargin;
					break;
				case MotionEvent.ACTION_UP:
					break;
				case MotionEvent.ACTION_POINTER_DOWN:
					break;
				case MotionEvent.ACTION_POINTER_UP:
					break;
				case MotionEvent.ACTION_MOVE:
					WindowManager wm = (WindowManager) getContext()
							.getSystemService(Context.WINDOW_SERVICE);
					Display display = wm.getDefaultDisplay();
					Point size = new Point();
					display.getSize(size);
					int height = size.y;

					RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view
							.getLayoutParams();

					if (Y - _yDelta >= 0 && Y - _yDelta < height - 200)
						layoutParams.topMargin = (Y - _yDelta);

					view.setLayoutParams(layoutParams);
					break;
				}
				// mRrootLayout.invalidate();
				return true;
			}

		});

		// final GestureDetector gdt = new GestureDetector(new
		// GestureListener());
		// setOnTouchListener(new OnTouchListener() {
		// @Override
		// public boolean onTouch(final View view, final MotionEvent event) {
		// gdt.onTouchEvent(event);
		// return true;
		// }
		// });
	}

//	private class GestureListener extends SimpleOnGestureListener {
//		@Override
//		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
//				float velocityY) {
//
//			scrollBy(0, -50);
//
//			return true;
//		}
//	}

}
