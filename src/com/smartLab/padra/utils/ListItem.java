package com.smartLab.padra.utils;

import android.graphics.drawable.Drawable;

public class ListItem{
	
	public ListItem(int id , String text , Drawable icon) {
		this.text = text;
		this.id = id;
		this.icon = icon;
	}
	public String text;
	public Drawable icon;
	public int id;
}