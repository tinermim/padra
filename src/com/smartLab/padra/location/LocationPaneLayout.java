package com.smartLab.padra.location;
import android.content.Context;
import android.support.v4.view.MotionEventCompat;
import android.util.AttributeSet;
import android.view.MotionEvent;

import com.smartLab.padra.remote.FoldingPaneLayout;

class LocationPaneLayout extends FoldingPaneLayout {

	public LocationPaneLayout(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	public LocationPaneLayout(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public LocationPaneLayout(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);

	}

	@Override
	public boolean onInterceptTouchEvent(MotionEvent arg0) {
		int action = MotionEventCompat.getActionMasked(arg0);
		// if (action == MotionEvent.ACTION_UP) {
		// // Release the scroll.
		// return true; // Do not intercept touch event, let the child handle it
		// }

		if (action == MotionEvent.ACTION_MOVE)
			return false;
		//
		return false;

	}
}