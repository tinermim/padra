package com.smartLab.padra.location;

import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.padra.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.smartLab.padra.BaseSliderFragment;
import com.smartLab.padra.PadraApp;
import com.smartLab.padra.PreferenceRefrences;
import com.smartLab.padra.SMS.CommandType;
import com.smartLab.padra.SMS.TextMessageSender;
import com.smartLab.padra.remote.AppListAdapter;
import com.smartLab.padra.remote.FoldingPaneLayout;
import com.smartLab.padra.utils.ListItem;
import com.smartLab.setting.SettingActivity;
import com.tyczj.mapnavigator.Navigator;

public class MapControllerFragment extends BaseSliderFragment implements
		OnMapReadyCallback {

	public FoldingPaneLayout mPaneLayout;
	public ListView mPaneList;
	private List<ListItem> appsData;
	private LatLng mobileCurrentLocation = null;

	private SharedPreferences mShared = null;
	private Editor mEditor = null;
	private boolean isLocationSet = false;
	private boolean isLookingForPath = false;

	public GoogleMap map;
	public MapFragment mapFragment;
	public Navigator nav;
	
	
	/*
	06-15 12:09:55.930: E/Google Maps Android API(28446): In the Google Developer Console (https://console.developers.google.com)
	06-15 12:09:55.930: E/Google Maps Android API(28446): Ensure that the "Google Maps Android API v2" is enabled.
	06-15 12:09:55.930: E/Google Maps Android API(28446): Ensure that the following Android Key exists:
	06-15 12:09:55.930: E/Google Maps Android API(28446): 	API Key: AIzaSyCewbEZQT5DIKfyr1kw3at8zhCGzhC_mcI
	06-15 12:09:55.930: E/Google Maps Android API(28446): 	Android Application (<cert_fingerprint>;<package_name>): D9:16:46:BD:F8:38:CA:F4:69:A9:C3:00:A4:85:72:F4:FF:7E:E3:DD;com.example.padra

	06-15 11:19:53.550: E/Google Maps Android API(26952): Authorization failure.  Please see https://developers.google.com/maps/documentation/android/start for how to correctly set up the map.
	06-15 11:19:53.580: E/Google Maps Android API(26952): In the Google Developer Console (https://console.developers.google.com)
	06-15 11:19:53.580: E/Google Maps Android API(26952): Ensure that the "Google Maps Android API v2" is enabled.
	06-15 11:19:53.580: E/Google Maps Android API(26952): Ensure that the following Android Key exists:
	06-15 11:19:53.580: E/Google Maps Android API(26952): 	API Key: AIzaSyCewbEZQT5DIKfyr1kw3at8zhCGzhC_mcI
	06-15 11:19:53.580: E/Google Maps Android API(26952): 	Android Application (<cert_fingerprint>;<package_name>): D9:16:46:BD:F8:38:CA:F4:69:A9:C3:00:A4:85:72:F4:FF:7E:E3:DD;com.example.padra

	*/
	@Override
	public void onResume() {
		Log.i("MapControllerFragment", "in onResume");
		
		setDrawableAnimation(R.id.animationSwipeLeftLocation,
				R.drawable.animation_left);
		setDrawableAnimation(R.id.animationSwipeRightLocation,
				R.drawable.animation_right);
		mShared = PreferenceManager.getDefaultSharedPreferences(getActivity()
				.getApplicationContext());
		mEditor = mShared.edit();
		initDrawer();
		mapFragment = ((MapFragment) getActivity().getFragmentManager()
				.findFragmentById(R.id.map));
		mapFragment.getMapAsync(this);
		//mapFragment.getMap().
		super.onResume();
	}

	private void setLocation(LatLng loc) {
		mobileCurrentLocation = loc;
	}

	@Override
	public void onMapReady(GoogleMap googleMap) {
		// TODO Auto-generated method stub
		Log.i("MapControllerFragment", "in onMapReady");

		if (GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity()
				.getApplicationContext()) == ConnectionResult.SUCCESS) {
			map = googleMap;
			map.getUiSettings().setZoomControlsEnabled(true);
			map.getUiSettings().setMapToolbarEnabled(true);
			map.setMyLocationEnabled(true);
			map.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
				@Override
				public void onMyLocationChange(Location location) {
					LatLng loc = new LatLng(location.getLatitude(), location
							.getLongitude());
					setLocation(loc);
					isLocationSet = true;

					if (isLookingForPath)
						pathToDevice();
				}
			});

			map.setMapType(mShared.getInt(PreferenceRefrences.MAP_TYPE, 1));
			map.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
				@Override
				public boolean onMyLocationButtonClick() {
					map.setMyLocationEnabled(true);
					Location location = map.getMyLocation();

					if (location != null) {
						Toast.makeText(
								getActivity(),
								getActivity().getResources().getString(
										R.string.showCellPhoneLocation),
								Toast.LENGTH_SHORT).show();

						map.addMarker(new MarkerOptions().position(
								new LatLng(location.getLatitude(), location
										.getLongitude())).title("You are here"));

						CameraPosition cameraPosition = CameraPosition
								.builder().target(mobileCurrentLocation)
								.zoom(13).build();
						map.animateCamera(CameraUpdateFactory
								.newCameraPosition(cameraPosition), 2000, null);
						return true;
					} else
						System.out.println("location Null");
					return true;
				}
			});
			
			//Log.i("MapControllerFragment", "in onMapReady 2");
			// Go to Iran's location on the map
			double lat = 35.74726459862971;//35.680705313847436;
			double lng = 51.3775634765625; //51.40777587890625;
			LatLng coordinate = new LatLng(lat, lng);
			CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(coordinate, 5);
			map.animateCamera(yourLocation);
			
			
			// if it's the first user has been here,
			// please request for the user location
			if (mShared.getBoolean(PreferenceRefrences.FIRST_TIME_IN_MAP, true)) {
				mShared.edit().putBoolean(PreferenceRefrences.FIRST_TIME_IN_MAP, false).commit();
				PadraApp.showToast("Requesting for car location");
				sendTrackerRequest();
			}
			
		}

	}

	public static BaseSliderFragment newInstance(int position) {
		BaseSliderFragment f = new MapControllerFragment();
		Bundle bundle = new Bundle();
		bundle.putInt(POSITION_KEY, position);
		f.setArguments(bundle);
		return f;
	}

	@Override
	protected View getLayout(LayoutInflater inflater, ViewGroup container, boolean b) {
		return inflater.inflate(R.layout.fragments_location_view, container, false);
	}

	private void initDrawer() {
		mPaneLayout = (FoldingPaneLayout) getActivity().findViewById(
				R.id.drawer_layout_location);
		mPaneList = (ListView) getActivity().findViewById(
				R.id.left_drawer_location);
		mPaneLayout.getFoldingLayout().setNumberOfFolds(4);
		mPaneLayout.foldingNavigationLayout.setBackgroundColor(Color.BLACK);
		appsData = new LinkedList<ListItem>();
		appsData.add(new ListItem(0, getActivity().getResources().getString(
				R.string.settings), getActivity().getResources().getDrawable(
				R.drawable.icon)));
		appsData.add(new ListItem(1, getActivity().getResources().getString(
				R.string.mapType), getActivity().getResources().getDrawable(
				R.drawable.abc_ic_menu_share_mtrl_alpha)));
		appsData.add(new ListItem(2, getActivity().getResources().getString(
				R.string.pathToCar), getActivity().getResources().getDrawable(
				R.drawable.abc_ic_menu_share_mtrl_alpha)));
		appsData.add(new ListItem(3, getActivity().getResources().getString(
				R.string.carLocationFind), getActivity().getResources()
				.getDrawable(R.drawable.abc_ic_menu_share_mtrl_alpha)));
		appsData.add(new ListItem(4, getActivity().getResources().getString(
				R.string.mapClear), getActivity().getResources().getDrawable(
				R.drawable.abc_ic_menu_share_mtrl_alpha)));

		AppListAdapter adapter = new AppListAdapter(getActivity()
				.getApplicationContext(), appsData);
		mPaneList.setAdapter(adapter);

		mPaneList.setOnItemClickListener(new DrawerItemClickListener());
	}

	private class DrawerItemClickListener implements
			ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			selectItem(position);
		}
	}

	private void pathToDevice() {
		if (isLocationSet) {
			final Float latitude = mShared.getFloat(
					PreferenceRefrences.LAST_LAT_KEY, 0.0f);
			final Float longtitude = mShared.getFloat(
					PreferenceRefrences.LAST_LONG_KEY, 0.0f);
			long last_update_time = mShared.getLong(
					PreferenceRefrences.LAST_TIME_LOCATION_UPDATE_KEY, 0);
			long now = Calendar.getInstance().getTimeInMillis() / 1000;
			if (now - last_update_time > 60 * 10) {
				Builder alert = new AlertDialog.Builder(getActivity());
				alert.setTitle(
						getActivity().getResources().getString(R.string.info))
						.setMessage(
								getActivity().getResources().getString(
										R.string.moreThanTenMinsReq));
				alert.setCancelable(true);
				alert.setPositiveButton(
						getActivity().getResources().getString(R.string.yes),
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int id) {
								drawPathToCar(new LatLng(latitude, longtitude));
							}
						});

				alert.setNegativeButton(
						getActivity().getResources().getString(
								R.string.relocate),
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int id) {
								lookForCarLocation();
							}
						});
				alert.show();
			} else {
				drawPathToCar(new LatLng(latitude, longtitude));
			}

		} else {
			Toast.makeText(
					getActivity(),
					getActivity().getResources().getString(
							R.string.pathWillBeShown), Toast.LENGTH_LONG)
					.show();
			isLookingForPath = true;
		}
	}

	private void drawPathToCar(LatLng location) {
		Toast.makeText(
				getActivity(),
				getActivity().getResources().getString(
						R.string.pathWillBeShownIfPossible), Toast.LENGTH_SHORT)
				.show();
		nav = new Navigator(map, new LatLng(map.getMyLocation().getLatitude(),
				map.getMyLocation().getLongitude()), location);
		nav.setPathLineWidth(7);
		nav.findDirections(false);
		isLookingForPath = false;
	}

	private void selectItem(int position) {
		if (position == 0)
			goToSettings();
		else if (position == 1)
			chooseMapType();
		else if (position == 2)
			pathToDevice();
		else if (position == 3)
			lookForCarLocation();
		if (position == 4) {
			if (map != null)
				map.clear();
		}
		mPaneLayout.closePane();
	}

	private void goToSettings() {
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				Intent myIntent = new Intent(getActivity(),
						SettingActivity.class);
				getActivity().startActivity(myIntent);
			}
		}, 200);
	}

	private void chooseMapType() {
		int key = mShared.getInt(PreferenceRefrences.MAP_TYPE, 1);
		Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
		// set title
		alertDialogBuilder.setTitle(getActivity().getResources().getString(
				R.string.mapTypeSelect));

		alertDialogBuilder.setSingleChoiceItems(R.array.mapTypes, key,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						if (map != null)
							map.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
						
						switch (which) {
						case 0:
							if (map != null)
								mEditor.putInt(PreferenceRefrences.MAP_TYPE, 0)
										.commit();
							break;
						case 1:
							if (map != null)
								map.setMapType(GoogleMap.MAP_TYPE_HYBRID);
							mEditor.putInt(PreferenceRefrences.MAP_TYPE, 1)
									.commit();
							break;
						case 2:
							if (map != null)
								map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
							mEditor.putInt(PreferenceRefrences.MAP_TYPE, 2)
									.commit();
							break;
						}
					}
				});

		alertDialogBuilder.setPositiveButton(getActivity().getResources()
				.getString(R.string.confirm), null);

		alertDialogBuilder.setCancelable(true);
		alertDialogBuilder.show();
	}

	public void showCarLocationOnMap() {
		long last_update_time = mShared.getLong(
				PreferenceRefrences.LAST_TIME_LOCATION_UPDATE_KEY, 0);
		final Float longtitude = mShared.getFloat(
				PreferenceRefrences.LAST_LONG_KEY, 0.0f);
		final Float latitude = mShared.getFloat(
				PreferenceRefrences.LAST_LAT_KEY, 0.0f);

		if (latitude != 0.0f && longtitude != 0.0f
				&& System.currentTimeMillis() / 1000 - last_update_time < 180) {
			map.addMarker(
					new MarkerOptions()
							.position(new LatLng(latitude, longtitude))
							.title("car location").snippet("car location"))
					.setIcon(
							BitmapDescriptorFactory
									.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));

			CameraPosition cameraPosition = CameraPosition.builder()
					.target(new LatLng(latitude, longtitude)).zoom(13).build();
			// Animate the change in camera view over 2 seconds
			map.animateCamera(
					CameraUpdateFactory.newCameraPosition(cameraPosition),
					2000, null);

			mShared.edit()
					.putBoolean(
							PreferenceRefrences.IS_LOOKING_FOR_CAR_LOCATION,
							false).commit();
		}

	}

	private void lookForCarLocation() {
		mShared.edit()
				.putBoolean(PreferenceRefrences.IS_LOOKING_FOR_CAR_LOCATION,
						true).commit();
		long last_update_time = mShared.getLong(
				PreferenceRefrences.LAST_TIME_LOCATION_UPDATE_KEY, 0);
		final Float longtitude = mShared.getFloat(
				PreferenceRefrences.LAST_LONG_KEY, 0.0f);
		final Float latitude = mShared.getFloat(
				PreferenceRefrences.LAST_LAT_KEY, 0.0f);

		if (latitude == 0.0f && longtitude == 0.0f)
			sendTrackerRequest();
		else if (System.currentTimeMillis() / 1000 - last_update_time > 60)
			sendTrackerRequest();
		else {
			Builder alert = new AlertDialog.Builder(getActivity());
			alert.setTitle(
					getActivity().getResources().getString(R.string.info))
					.setMessage(
							getActivity().getResources().getString(
									R.string.lessThanThreeMinute));
			alert.setCancelable(true);
			alert.setPositiveButton(
					getActivity().getResources().getString(R.string.yes),
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int id) {
							sendTrackerRequest();
						}
					});

			alert.setNegativeButton(
					getActivity().getResources().getString(R.string.no),
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int id) {
							showCarLocationOnMap();
						}
					});
			alert.show();
		}

	}

	private void sendTrackerRequest() {
		Toast.makeText(
				getActivity(),
				getActivity().getResources().getString(
						R.string.locationinReqSent), Toast.LENGTH_SHORT).show();

		TextMessageSender sender = TextMessageSender.getInstance(getActivity());
		sender.sendSms(CommandType.tracker, null);
	}

}