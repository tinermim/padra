package com.smartLab.padra.SMS;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.telephony.SmsMessage;

import com.smartLab.padra.PreferenceRefrences;

public class TextMessageReceiver extends BroadcastReceiver {

	public void onReceive(Context context, Intent intent) {
		Bundle bundle = intent.getExtras();
		Intent i = new Intent(context, TextReceiveActivity.class);
		i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		i.putExtras(bundle);
		if (isSmsFromDevice(context, i)){
			context.startActivity(i);
		}
	}

	private boolean isSmsFromDevice(Context context, Intent intent) {
		SharedPreferences mShared = PreferenceManager
				.getDefaultSharedPreferences(context);
		Bundle bundle = intent.getExtras();
        Object messages[] = (Object[]) bundle.get("pdus");
        SmsMessage smsMessage[] = new SmsMessage[messages.length];      

        for (int n = 0; n < messages.length; n++) 
            smsMessage[n] = SmsMessage.createFromPdu((byte[]) messages[n]); 
        
        String originatingAddress = smsMessage[0].getOriginatingAddress();
        originatingAddress = (originatingAddress!=null && originatingAddress.length()>3)?(originatingAddress.substring(3)):("") ;

		String deviceNumber = mShared.getString(PreferenceRefrences.DEVICE_PHONE_NUMBER, "");
		if(deviceNumber.contains("+98"))
			deviceNumber = "0"+deviceNumber.substring(3);
		
		if (!deviceNumber.equals(""))
			deviceNumber = deviceNumber.substring(1);

		if (originatingAddress.equals(deviceNumber))
			return true;
		return false;
	}
}
