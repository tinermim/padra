package com.smartLab.padra.SMS;

import java.util.Calendar;

public class CommandItem {
	public CommandType type;
	public boolean isSent = false;
	public boolean isDelivered = false;
	public boolean isResponseReceived = false;
	public boolean isAlarm = false;
	public Calendar sendTime;

	@Override
	public String toString() {
		return super.toString();
	}
}