package com.smartLab.padra.SMS;

import android.content.Context;

import com.example.padra.R;

/**
 * to add a new command/alarm, we shoud follow these steps: 1. add the command
 * String in strings.xml ( for alarm skip this step ) 2. add the command/alarm
 * reply in strings.xml (text you get from device) 3. add the translation in
 * strings.xml (what user expect to see in UI e.g. move(command) => هشدار دهنده
 * حرکت خودرو)
 * 
 * 4. add the command/alarm name in the specific enum( CommandType.java ) 5. add
 * the command send structure in getTextFromCommand => TextMessageHandler.java
 * 6. add the command/alarm type in getCommandTypeFromMessage =>
 * TextReceiveActivity.java 7. add the command/alarm Persian equivalent in
 * getCommandMeaningToUser => CommandsToPersian
 * 
 * */

public class CommandsToPersian {

	public static String getCommandMeaningToUser(CommandType c, Context context) {

		if (c == null)
			return context.getResources().getString(R.string.unclear);

		if (c.equals(CommandType.initialization))
			return context.getResources().getString(
					R.string.initializationInPersian);

		if (c.equals(CommandType.passwordFail))
			return context.getResources().getString(
					R.string.passwordFailInPersian);

		if (c.equals(CommandType.authorization))
			return context.getResources().getString(
					R.string.authorizationInPersian);

		if (c.equals(CommandType.speedLimit))
			return context.getResources().getString(
					R.string.speedLimitInPersian);

		if (c.equals(CommandType.checkState))
			return context.getResources().getString(
					R.string.checkStateInPersian);

		if (c.equals(CommandType.changePassword))
			return context.getResources().getString(
					R.string.changePasswordInPersian);

		if (c.equals(CommandType.deleteAuthorization))
			return context.getResources().getString(
					R.string.deleteAuthorizationInPersian);

		if (c.equals(CommandType.doorArm))
			return context.getResources().getString(R.string.doorArmInPersian);

		if (c.equals(CommandType.doorDisarm))
			return context.getResources().getString(
					R.string.doorDisarmInPersian);

		if (c.equals(CommandType.initialization))
			return context.getResources().getString(
					R.string.initializationInPersian);

		if (c.equals(CommandType.PowerSystem))
			return context.getResources().getString(
					R.string.powerSystemInPersian);

		if (c.equals(CommandType.noQuickStop))
			return context.getResources().getString(
					R.string.noQuickStopInPersian);

		if (c.equals(CommandType.resumePowerSystem))
			return context.getResources().getString(
					R.string.resumePowerSystemInPersian);

		if (c.equals(CommandType.movementAlarm))
			return context.getResources().getString(
					R.string.movementAlarmInPersian);

		if (c.equals(CommandType.movementAlarmCancel))
			return context.getResources().getString(
					R.string.movementAlarmCancelInPersian);

		if (c.equals(CommandType.movementAlarmActivated))
			return context.getResources().getString(
					R.string.movementAlarmActivatedInPersian);

		if (c.equals(CommandType.helpMeAlarmActivated))
			return context.getResources().getString(
					R.string.helpMeAlarmActivatedInPersian);

		if (c.equals(CommandType.doorOpenAlarmActivated))
			return context.getResources().getString(
					R.string.doorOpenAlarmActivatedInPersian);

		if (c.equals(CommandType.shockSensorAlarmActivated))
			return context.getResources().getString(
					R.string.shockSensorAlarmActivatedInPersian);

		if (c.equals(CommandType.accidentAlarmActivated))
			return context.getResources().getString(
					R.string.accidentAlarmActivatedInPersian);

		if (c.equals(CommandType.speedLimitAlarmActivated))
			return context.getResources().getString(
					R.string.speedLimitAlarmInPersian);

		if (c.equals(CommandType.accOnAlarmActivated))
			return context.getResources().getString(
					R.string.accOffAlarmActivatedInPersian);

		if (c.equals(CommandType.accOffAlarmActivated))
			return context.getResources().getString(
					R.string.accOffAlarmActivatedInPersian);

		if (c.equals(CommandType.lowBatteryAlarmActivated))
			return context.getResources().getString(
					R.string.lowBatteryAlarmActivatedInPersian);

		if (c.equals(CommandType.powerAlarmActivated))
			return context.getResources().getString(
					R.string.powerAlarmActivatedInPersian);

		if (c.equals(CommandType.noGpsAlarmActivated))
			return context.getResources().getString(
					R.string.noGpsAlarmActivatedInPersian);

		if (c.equals(CommandType.outOfFenceAlarmActivated))
			return context.getResources().getString(
					R.string.outOfFenceAlarmActivatedInPersian);

		if (c.equals(CommandType.lowFuelAlarmActivated))
			return context.getResources().getString(
					R.string.lowFuelAlarmActivatedInPersian);

		if (c.equals(CommandType.passwordFail) || c.equals(CommandType.wrongPasswordCommand))
			return context.getResources().getString(
					R.string.passwordFailInPersian);

		if (c.equals(CommandType.noGpsMoveFail))
			return context.getResources().getString(
					R.string.noGpsMoveFailInPersian);

		if (c.equals(CommandType.tracker))
			return context.getResources().getString(
					R.string.trackerModeOnInPersian);

		if (c.equals(CommandType.monitor))
			return context.getResources().getString(
					R.string.monitorModeOnInPersian);

		if (c.equals(CommandType.latLong))
			return context.getResources().getString(R.string.latLongInPersian);

		if (c.equals(CommandType.adminIPSetup))
			return context.getResources().getString(
					R.string.adminIpSetupInPersian);

		if (c.equals(CommandType.gprsSetup))
			return context.getResources()
					.getString(R.string.gprsSetupInPersian);
		;

		if (c.equals(CommandType.timeZoneSetup))
			return context.getResources().getString(
					R.string.timeZoneSetupInPersian);

		if (c.equals(CommandType.suppress))
			return context.getResources().getString(R.string.suppressInPersian);

		if (c.equals(CommandType.trackUnlimitedInterval) || c.equals(CommandType.trackUnlimitedIntervalCustom))
			return context.getResources().getString(
					R.string.trackUnlimitedIntervalInPersian);

		if (c.equals(CommandType.postAddress))
			return context.getResources().getString(
					R.string.postAddressInPersian);

		if (c.equals(CommandType.imei))
			return context.getResources().getString(R.string.imeiInPersian);
		
		if (c.equals(CommandType.reset))
			return context.getResources().getString(R.string.resetInPersian);
		
		if (c.equals(CommandType.diag))
			return context.getResources().getString(R.string.diagInPersian);
		
		if (c.equals(CommandType.helpme))
			return context.getResources().getString(R.string.helpMePersian);
		
		
		return "";

	}

}
