package com.smartLab.padra.SMS;

import java.util.Calendar;
import java.util.Locale;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.example.padra.R;
import com.smartLab.padra.MainActivity;
import com.smartLab.padra.PadraApp;
import com.smartLab.padra.PreferenceRefrences;
import com.smartLab.padra.asetup.DeviceInfoActivity;
import com.smartLab.padra.asetup.WelcomeActivity;
import com.smartLab.padra.imeiCheking.Start;
import com.smartLab.padra.options.ArchiveItem;
import com.smartLab.padra.utils.DialogAlert;
import com.smartLab.padra.utils.DialogAlertOnAppClosed;
import com.smartLab.padra.utils.DialogAlertOnAppRun;

public class TextReceiveActivity extends Activity {
	
	private static final String TAG = TextReceiveActivity.class.getSimpleName();
	
	private boolean isAlarm = false;
	private SharedPreferences mShared;
	private Context mContext;
	private Boolean isVisible;
	private String phoneNumber = "";
	private String body = "";
	private TextMessageSender handler;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContext = getApplicationContext();
		mShared = PreferenceManager.getDefaultSharedPreferences(mContext);
		System.out.println("textReceiveActivity: OnCreate");
	}

	@Override
	protected void onResume() {
		System.out.println("textReceiveActivity: OnResume");
		isVisible = mShared.getBoolean(PreferenceRefrences.APP_VISIBILITY, false);
		extractSmsInformation();
		archiveSms();
		
		Log.i(TAG, "sms recieve body : " + body);
		Log.i(TAG, "is alarm : " + isAlarm);
		
		CommandType receiveCommandType = getCommandTypeFromMessage(body);
		if (isAlarm) {
			handleAlarmReceived(receiveCommandType);
		} else {
			handleCommandReceived(receiveCommandType);
		}
		super.onResume();
	}

	private void handleCommandReceived(CommandType receiveCommandType) {
		Log.i(TAG, "handleCommandReceived : " + receiveCommandType.toString());
		
		///
		saveImporantInfo(receiveCommandType, body);
		
		handler.updateFirstNotGotResponse(receiveCommandType);
		commandReceiveInform(isVisible, receiveCommandType);
		
	}

	private void handleAlarmReceived(CommandType receiveCommandType) {
		
		Log.i(TAG, "message is an alarm");
		
		CommandItem alarmItem = new CommandItem();
		alarmItem.isAlarm = true;
		alarmItem.type = receiveCommandType;
		alarmItem.sendTime = Calendar.getInstance();
		handler.addCommand(alarmItem);
		alarmReceiveInform(receiveCommandType, isVisible);

		if (receiveCommandType == CommandType.movementAlarmActivated)
			handleMovementAlarmActivatedPostActions();
	}

	private void handleMovementAlarmActivatedPostActions() {
		String[] parts = body.split("\\s+");
		if (parts != null && parts.length > 2) {
			try {
				Float latitude = Float.parseFloat(parts[1].trim());
				Float longtitude = Float.parseFloat(parts[2].trim());
				mShared.edit()
						.putFloat(PreferenceRefrences.LAST_LAT_KEY, latitude)
						.commit();
				mShared.edit()
						.putFloat(PreferenceRefrences.LAST_LONG_KEY, longtitude)
						.commit();
				mShared.edit()
						.putLong(PreferenceRefrences.LAST_TIME_LOCATION_UPDATE_KEY,
								Calendar.getInstance().getTimeInMillis() / 1000)
						.commit();
			} catch (Exception e) {
			}
		}
	}

	private void archiveSms() {
		handler = TextMessageSender.getInstance(this);
		ArchiveItem archive = new ArchiveItem();
		archive.phoneNumber = phoneNumber;
		archive.content = body;
		handler.addArchiveItem(archive);
	}

	private void extractSmsInformation() {
		Bundle bundle = getIntent().getExtras();
		Object[] messages = (Object[]) bundle.get("pdus");
		SmsMessage[] sms = new SmsMessage[messages.length];

		phoneNumber = "";
		body = "";
		for (int i = 0; i < messages.length; i++) {
			sms[i] = SmsMessage.createFromPdu((byte[]) messages[i]);
			phoneNumber += sms[i].getOriginatingAddress();
			body += sms[i].getMessageBody().toString();
		}

		// FD
		//phoneNumber ==
		
		body = body.toLowerCase(Locale.getDefault());

		if (body.contains("!")) {
			String[] temp = body.split("!");
			body = "";
			for (int i = 0; i < temp.length; i++)
				body += temp[i];
		}
	}

	private void commandReceiveInform(Boolean isVisible, CommandType receiveCommandType) {
		
		if (receiveCommandType != CommandType.imei) {

			Log.i(TAG, "show response dialog : " + mShared.getBoolean(PreferenceRefrences.SHOW_RESPONSE_DIALOG, false));
			
			//// handle information when command is dialog
			
			
			if (mShared.getBoolean(PreferenceRefrences.SHOW_RESPONSE_DIALOG, false) 
					&& receiveCommandType != CommandType.tracker 
					&& receiveCommandType != CommandType.monitor)
			{
				if (isVisible) {
					commandReceiveDialogWhenAppIsRunning(receiveCommandType);
				} else {
					commandReceiveDialogWhenAppIsClosed(receiveCommandType);
				}
				return;
			}
			
			Log.i(TAG, "here; after show dialog reponse");
			
			
			// if we have finished all steps, then it dosen't need to start from welcome activity
			// for commands including: check, arm, disarm, ....
			// 
			if (PadraApp.getStep() < 10) {
				Intent intent = new Intent(PadraApp.getAppContext(), WelcomeActivity.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
				finish();
			} else {
				Log.i(TAG, "step is 10");
				Log.i(TAG, "receiveCommandType is :" + receiveCommandType.toString());
				
//				if (receiveCommandType == CommandType.tracker) {
//					return;
//				}
				
				isVisible = mShared.getBoolean(PreferenceRefrences.APP_VISIBILITY, false);
//				Log.i(TAG, "value isVisible " + isVisible);
				
				if (!isVisible) {
					Log.i(TAG, "It is not visible. starting main activity");
					Intent intent = new Intent(PadraApp.getAppContext(), MainActivity.class);
					intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(intent);
					finish();
				}
			}
			
		}
	}

	private void alarmReceiveInform(CommandType type, Boolean isVisible) {
		
		Log.i(TAG, "alarm received in form");
		
		// sending message to cancel the alarm
//		TextMessageSender smsHandler = TextMessageSender
//				.getInstance(this);
//		smsHandler.sendSms(CommandType.checkState, null);
		
//		if (type == CommandType.helpMeAlarmActivated) {
//			// we have help me alarm here
//		}
		
		DialogAlert dialog;
//		if (mShared.getInt(PreferenceRefrences.ALARM_TYPE_KEY, 0) == 1) {
//			// alarm is inactive
//			finish();
//			return;
//		}
		
		if (isVisible)
			dialog = new DialogAlertOnAppRun(this, CommandsToPersian.getCommandMeaningToUser(type, this), type);
		else
			dialog = new DialogAlertOnAppClosed(this, CommandsToPersian.getCommandMeaningToUser(type, this), type);

		dialog.show();
		
	}

	private void saveImporantInfo(CommandType receiveCommandType, String body) {
		System.out.println("save important information, command type: " + receiveCommandType.toString());
		
		if (mShared.getBoolean(PreferenceRefrences.IS_FIRST_RUN, true) && receiveCommandType == CommandType.authorization)
			handleFirstRunCommands();
		else if (receiveCommandType == CommandType.noQuickStop) {
			TextMessageSender sms = TextMessageSender.getInstance(this);
			sms.sendSms(CommandType.PowerSystem, null);
		} else if (receiveCommandType == CommandType.doorArm)
			mShared.edit().putString(PreferenceRefrences.DOORS_STATUS, "off").commit();
		else if (receiveCommandType == CommandType.doorDisarm)
			mShared.edit().putString(PreferenceRefrences.DOORS_STATUS, "on").commit();
		else if (receiveCommandType == CommandType.PowerSystem)
			mShared.edit().putString(PreferenceRefrences.POWER_SYSTEM_STATUS, "off").commit();
		else if (receiveCommandType == CommandType.resumePowerSystem)
			mShared.edit().putString(PreferenceRefrences.POWER_SYSTEM_STATUS, "on").commit();
		else if (receiveCommandType == CommandType.checkState)
			parseCheckInformation(body);
		else if (receiveCommandType == CommandType.initialization)
			factoryReset();
		else if (receiveCommandType == CommandType.changePassword)
			changePassword();
		else if (receiveCommandType == CommandType.authorization)
			addAuthorizedUser();
		else if (receiveCommandType == CommandType.monitor)
			monitorCommandPostExecute();
		else if (receiveCommandType == CommandType.deleteAuthorization)
			removeAdmin();
		else if (receiveCommandType == CommandType.postAddress)
			postAddressShowDialog(body);
		else if (receiveCommandType == CommandType.tracker)
			trackerPostExecute();
		else if (receiveCommandType == CommandType.latLong)
			updateLatLong(body);
		else if (receiveCommandType == CommandType.imei)
			imeiValidationCheck(body, receiveCommandType);
		else if (receiveCommandType == CommandType.reset)
			resetValidationCheck(body);
		else if (receiveCommandType == CommandType.adminIPSetup)
			adminIpValidationCheck(body);
		else if (receiveCommandType == CommandType.gprsSetup)
			gprsValidationCheck(body);
		else if (receiveCommandType == CommandType.suppress)
			suppressValidationCheck(body);
		else if (receiveCommandType == CommandType.timeZoneSetup)
			timezoneValidationCheck(body);
		else if (receiveCommandType == CommandType.trackUnlimitedInterval)
			trackUnlimitedValidationCheck(body);
		else if (receiveCommandType == CommandType.wrongPasswordCommand || receiveCommandType == CommandType.passwordFail)
			wrongPassword(body);
		
	}
	

	private void wrongPassword(String body) {
		if (PadraApp.getStep() < 10) {
			PreferenceRefrences.resetApplicationPreferencesInfo(this, mShared);
			PadraApp.setStep(0);
			
			PadraApp.showToast("Wrong password!", Toast.LENGTH_LONG);
			
			/*
			if (isVisible) {
				commandReceiveDialogWhenAppIsRunning(CommandType.wrongPasswordCommand);
			
//			} else {
//				commandReceiveDialogWhenAppIsClosed(CommandType.wrongPasswordCommand);
			} else {
				finish();
			}
			*/
			finish();
			
			Log.i(TAG, "here i am wong password");
			
		}
		
	}
	
	private void handleFirstRunCommands() {
		String string = mShared.getString(PreferenceRefrences.MOBILE_NUMBER, "");
		mShared.edit().putString(PreferenceRefrences.DEVICE_VALID_USERS, (string + ",")).commit();
		mShared.edit().putBoolean(PreferenceRefrences.IS_FIRST_RUN, false).commit();
		addAuthorizedUser();
	}
	
	
	/** First step in application setup */
	private void resetValidationCheck(String body) {
		if (PadraApp.getStep() == 10) {
			finish();
			return;
		}
		PadraApp.setStep(2);
	}
	
	/** Second step in application setup */
	private void factoryReset() {
		if (PadraApp.getStep() == 10) {
			finish();
			return;
		}
		//mShared.edit().putString(PreferenceRefrences.DEVICE_NAME, "").commit();
		mShared.edit().putString(PreferenceRefrences.DEVICE_VALID_USERS, mShared.getString(PreferenceRefrences.MOBILE_NUMBER, "")).commit();
		
		PadraApp.setStep(3);
	}

	/** Third step in application setup */
	private void addAuthorizedUser() {
//		if (PadraApp.getStep() == 10) {
//			finish();
//			return;
//		}
		Log.i(TAG, "adding authenticated user");
		String string = mShared.getString(PreferenceRefrences.DEVICE_VALID_USERS, "");
		if (string.length() > 55)
			Toast.makeText(this, R.string.limitedUsers, Toast.LENGTH_SHORT).show();
		else if (string.length() > 44)
			string += (mShared.getString(PreferenceRefrences.DEVICE_VALID_USERS_REQ, ""));
		else
			string += (mShared.getString(PreferenceRefrences.DEVICE_VALID_USERS_REQ, "") + ",");
		
		mShared.edit().putString(PreferenceRefrences.DEVICE_VALID_USERS, string).commit();
		mShared.edit().putString(PreferenceRefrences.DEVICE_VALID_USERS_REQ, "").commit();
		
		if (PadraApp.getStep() == 10) {
			//finish();
			return;
		}
		////
		PadraApp.setStep(4);
		
	}
	
	/** Fourth step in application setup */
	private void adminIpValidationCheck(String body) {
		if (PadraApp.getStep() == 10) {
			finish();
			return;
		}
		PadraApp.setStep(5);
	}
	
	/** Fifth step in application setup */
	private void gprsValidationCheck(String body) {
		if (PadraApp.getStep() == 10) {
			finish();
			return;
		}
		PadraApp.setStep(6);
	}
	
	/** Sixth step in application setup */
	private void suppressValidationCheck(String body) {
		if (PadraApp.getStep() == 10) {
			finish();
			return;
		}
		PadraApp.setStep(7);
	}
	
	/** Seventh step in application setup */
	private void timezoneValidationCheck(String body) {
		if (PadraApp.getStep() == 10) {
			finish();
			return;
		}
		PadraApp.setStep(8);
	}
	
	/** Eighth step in application setup */
	private void trackUnlimitedValidationCheck(String body) {
		if (PadraApp.getStep() == 10) {
			finish();
			return;
		}
		PadraApp.setStep(9);
	}
	
	/** Ninth step in application setup */
	private void imeiValidationCheck(String imei, CommandType receiveCommandType) {
		System.out.println("going to call the service");
		
		if (!PadraApp.hasWifiOrMobileConnection()) {
			noInternetDialog(receiveCommandType);
			return;
		}
		
		if (imei.contains("imei:") || mShared.getInt(PreferenceRefrences.DEVICE_TYPE_KEY, 0) == 4) {
			
			String first = imei;
			imei = first.substring(5); // Resposne Format is ==> IMEI:45232327
			
			//imei.indexOf(":")
			
		}
		Log.i(TAG, "Final imei ::" + imei);
		
		Start.encryption(this, /* "359710042181215" */ imei);
		//Start.encryption(this, "865328021056345");
		
		//PadraApp.setStep(10);
	}
	
	private void noInternetDialog(CommandType receiveCommandType) {
		
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(getResources().getString(R.string.no_internet))
			.setCancelable(false)
			.setMessage(getResources().getString(R.string.no_internet_msg))
			.setCancelable(false);
		builder.setPositiveButton(getResources().getString(R.string.accept),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
//						Intent intent = new Intent(PadraApp.getAppContext(), WelcomeActivity.class);
//						intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
//						startActivity(intent);
//						finish();
					}
			})/*.create().show()*/;
		builder.setNegativeButton(getResources().getString(R.string.go_netowrk_settings),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
//						Intent intent = new Intent(Intent.ACTION_MAIN);
//						intent.setClassName("com.android.phone", "com.android.phone.Setting");
//						startActivity(intent);
						startActivity(new Intent(Settings.ACTION_SETTINGS));
//						finish();
					}
				});
		AlertDialog alert = builder.create();
		alert.show();
	}
	
	private void updateLatLong(String body) {
		System.out.println("showing parts " + body + " "
				+ body.split("\\s+").length);
		String[] bodyParts = body.trim().split("\\s+");
		String latitude = null, longtitude = null;
		for (String part : bodyParts) {
			System.out.println("parts:");
			System.out.println(part);
			if (part.contains("lat") && part.length() > 4)
				latitude = part.substring(4);
			else if (part.contains("long") && part.length() > 5) {
				longtitude = part.substring(5);
				longtitude = longtitude.split(",")[0];
			} else if (part.contains("lon") && part.length() > 4)
				longtitude = part.substring(4);
		}

		if (latitude != null && longtitude != null) {
			try {
				mShared.edit()
						.putFloat(PreferenceRefrences.LAST_LAT_KEY,
								Float.parseFloat(latitude)).commit();
				mShared.edit()
						.putFloat(PreferenceRefrences.LAST_LONG_KEY,
								Float.parseFloat(longtitude)).commit();
				mShared.edit()
						.putLong(PreferenceRefrences.LAST_TIME_LOCATION_UPDATE_KEY,
								Calendar.getInstance().getTimeInMillis() / 1000)
						.commit();
			} catch (Exception e) {

			}
		}
	}

	private void trackerPostExecute() {
		try {
			Intent callIntent = new Intent(Intent.ACTION_CALL);
			callIntent.setData(Uri.parse("tel:" + mShared.getString(PreferenceRefrences.DEVICE_PHONE_NUMBER, "")));
			callIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(callIntent);
			finish(); // Call once you redirect to another activity
		} catch (Exception e) {
			Toast.makeText(this, R.string.noCallService, Toast.LENGTH_SHORT).show();
		}
	}

	private void postAddressShowDialog(String body) {
		final Dialog dialog = new Dialog(this);
		dialog.setContentView(R.layout.post_address_dialog);
		dialog.setCancelable(true);
		dialog.setTitle(R.string.carPostAddrressTitle);
		TextView view = (TextView) dialog.findViewById(R.id.car_current_location);
		view.setText(body);
		dialog.show();
	}

	private void changePassword() {
		mShared.edit().putString(PreferenceRefrences.DEVICE_PASSWORD,
						mShared.getString(PreferenceRefrences.DEVICE_PASSWORD_REQ, "")).commit();
		mShared.edit().putString(PreferenceRefrences.DEVICE_PASSWORD_REQ, "").commit();
	}

	private void removeAdmin() {
		String del = mShared.getString(PreferenceRefrences.ADMIN_DELETING_REQ, "");
		String delete = "";
		if (del.length() != 0)
			delete = "+98" + del.substring(1);
		
		String[] users = mShared.getString(PreferenceRefrences.DEVICE_VALID_USERS, "").split(",");
		String string = "";
		for (int i = 0; i < users.length; i++)
			if (!users[i].equals(del) && !users[i].equals(delete)
					&& (users[i].length() == 11 || users[i].length() == 13)) {
				if (i != (users.length - 1))
					string += (users[i] + ",");
				else
					string += users[i];
			}
		mShared.edit()
				.putString(PreferenceRefrences.DEVICE_VALID_USERS, string)
				.commit();
	}

	private void monitorCommandPostExecute() {
		Long time = System.currentTimeMillis();
		time = time
				- mShared.getLong(PreferenceRefrences.CAR_VOICE_START_TIME, 0);
		time /= 1000;
		if (time < 120) {
			trackerPostExecute();
		}
	}
	
	private void parseCheckInformation(String body) {
		/** FD **/
		Log.i(TAG, "device type : " + mShared.getInt(PreferenceRefrences.DEVICE_TYPE_KEY, 3));
		
		mShared.edit().putLong(PreferenceRefrences.CHECK_LAST_UPDATE, System.currentTimeMillis()).commit();
		
		if (mShared.getInt(PreferenceRefrences.DEVICE_TYPE_KEY, 3) == 4) {
			handlePadra945CheckIn(body);
			return;
		} else if (mShared.getInt(PreferenceRefrences.DEVICE_TYPE_KEY, 3) == 2) {
			handlePadra923CheckIn(body);
			return;
		}
		
		String[] results = body.split("\n");
		setState(results[0], "POWER_SYSTEM_STATUS");
		setState(results[2], "GPRS_STATUS");
		setState(results[3], "GPS_STATUS");
		setState(results[4], "ACC_STATUS");
		setState(results[5], "DOORS_STATUS");

		if (results[1] != null)
			mShared.edit()
					.putString(PreferenceRefrences.BATTERY_STATUS,
							results[1].substring(8)).commit();
		if (results[6] != null)
			mShared.edit()
					.putString(PreferenceRefrences.GSM_STATUS,
							results[6].substring(4)).commit();
		if (results[7] != null)
			mShared.edit()
					.putString(PreferenceRefrences.OIL_STATUS,
							results[7].substring(4)).commit();
	}
	
	/// FD
	private void handlePadra923CheckIn(String body) {
		Log.i(TAG, "in handlePadra923CheckIn");
		try {
			
		
		body = body.toLowerCase(Locale.getDefault());
		
		String[] results = body.split("\n");
		setState(results[0], "POWER_SYSTEM_STATUS");
		if (results[1] != null) {
			mShared.edit().putString(PreferenceRefrences.BATTERY_STATUS, results[1].substring(8)).commit();
		}
		setState(results[2], "GPRS_STATUS");
		setState(results[3], "GPS_STATUS");
//		setState(results[4], "ACC_STATUS");
//		setState(results[5], "DOORS_STATUS");

		if (results[4] != null) {
			mShared.edit().putString(PreferenceRefrences.GSM_STATUS, results[4].substring(4)).commit();
		}
		
//		if (results[7] != null)
//			mShared.edit().putString(PreferenceRefrences.OIL_STATUS, results[7].substring(4)).commit();
		
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
	}
			
	private void handlePadra945CheckIn(String body) {
		try {
			
		
		body = body.toLowerCase(Locale.getDefault());
		
		String[] results = body.split("\n");
		
		String[] top = results[0].split(" ");
		setState(top[0], "POWER_SYSTEM_STATUS");
		if (top[1] != null) {
			mShared.edit().putString(PreferenceRefrences.BATTERY_STATUS, top[1].substring(8)).commit();
		}
		if (results[1].contains("ok")) {
			mShared.edit().putString("GPS_STATUS", "on").commit();
		} else {
			mShared.edit().putString("GPS_STATUS", "off").commit();
		}
		setState(results[2], "GPRS_STATUS");
		if (results[3] != null) {
			mShared.edit().putString(PreferenceRefrences.GSM_STATUS, results[3].substring(4)).commit();
		}
		if (results[4] != null) {
			mShared.edit().putString(PreferenceRefrences.OIL_STATUS, results[4].substring(4)).commit();
		}
		
//		setState(results[4], "ACC_STATUS");
//		setState(results[5], "DOORS_STATUS");

		
		
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	private void setState(String s, String key) {
		if (s != null)
			if (s.contains("off"))
				mShared.edit().putString(key, "off").commit();
			else if (s.contains("on"))
				mShared.edit().putString(key, "on").commit();
	}

	private void commandReceiveDialogWhenAppIsClosed(CommandType type) {
		Log.i(TAG, "commandReceiveDialogWhenAppIsClosed : " + type);

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle(getResources().getString(R.string.message))
			.setMessage(CommandsToPersian.getCommandMeaningToUser(type, this))
			.setCancelable(false)
			.setPositiveButton(getResources().getString(R.string.accept),
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							dialog.cancel();
							if (PadraApp.getStep() != 10) {
								Intent intent = new Intent(PadraApp.getAppContext(), WelcomeActivity.class);
								intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
								startActivity(intent);
							}
							finish();
							return;
						}
					});

		builder.setNegativeButton(getResources().getString(R.string.goToApp),
			new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					dialog.cancel();
//						Intent intent = new Intent(TextReceiveActivity.this, WelcomeActivity.class);
//						intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//						startActivity(intent);
					Intent intent = new Intent(PadraApp.getAppContext(), WelcomeActivity.class);
//						intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//						intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//						intent.addFlags(0x10000000);
//						intent.addFlags(0x4000000);
					intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(intent);
					finish();
					
					// do some better works for tracker
//						if (PadraApp.getStep() == 10) {
//							Intent intent2 = new Intent(PadraApp.getAppContext(), MainActivity.class);
//							startActivity(intent2);
//							finish();
//						}
				}
			});
		
		if (type == CommandType.diag || type == CommandType.UNKNOWN || type == CommandType.postAddress || type == CommandType.latLong) {
			builder.setMessage(body);
		}
		
		AlertDialog alert = builder.create();
		alert.show();
	}

	private void commandReceiveDialogWhenAppIsRunning(CommandType type) {
		Log.i(TAG, "commandReceiveDialogWhenAppIsRunning : " + type);
		
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(getResources().getString(R.string.message))
			.setCancelable(false)
			.setMessage(CommandsToPersian.getCommandMeaningToUser(type, this))
			.setPositiveButton(R.string.accept,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
						if (PadraApp.getStep() != 10) {
						}
						/*
						Intent intent = new Intent(PadraApp.getAppContext(), WelcomeActivity.class);
						intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
						startActivity(intent);
						*/
						finish();
					}
				}) /*.create().show()*/ ;
		
		if (type == CommandType.diag || type == CommandType.UNKNOWN || type == CommandType.postAddress || type == CommandType.latLong) {
			builder.setMessage(body);
		}
		
		AlertDialog alert = builder.create();
		alert.show();
		
		Log.i(TAG, "commandReceiveDialogWhenAppIsRunning 3");
		
	}

	private CommandType getCommandTypeFromMessage(String body) {

		/// Detecting command type from received SMS body
		// Command Type: Check_State
		if (body.toLowerCase(Locale.getDefault()).contains("power") && body.toLowerCase(Locale.getDefault()).contains("battery")
				&& body.toLowerCase(Locale.getDefault()).contains("gprs"))
			return CommandType.checkState;
		
		// Command Type: begin
		if (body.contains(getResources().getString(R.string.initializationReply)))
			return CommandType.initialization;
		
		// Command Type: monitor
		if (body.contains(getResources().getString(R.string.monitorReply)))
			return CommandType.monitor;
		
		// Command Type: begin
		if (body.contains(getResources().getString(R.string.speedLimitReply)))
			return CommandType.speedLimit;
		
		// Command Type: tracker
		if (body.contains(getResources().getString(R.string.trackerReply)) || 
				(body.contains("tracker") && body.contains("ok")))
			return CommandType.tracker;

		// Command Type: address
		if (body.contains(getResources().getString(R.string.postAddress)))
			return CommandType.postAddress;

		// Command Type: suppress
		if (body.contains(getResources().getString(R.string.suppressReply)))
			return CommandType.suppress;

		// Command Type: nosuppress
		if (body.contains(getResources().getString(R.string.undoSuppress)))
			return CommandType.undoSuppress;

		// Command Type: password (Change_Password)
		if (body.contains(getResources().getString(R.string.changePasswordReply)))
			return CommandType.changePassword;

		// Command Type: arm
		if (body.contains(getResources().getString(R.string.doorArmReplySucceed))
				|| (body.contains("tracker") && body.contains("is") && body.contains("activated"))) {
			return CommandType.doorArm;
		}
		
		// Command Type: disarm
		if (body.contains(getResources().getString(R.string.doorDisarmReply))
				|| (body.contains("tracker") && body.contains("is") && body.contains("deactivated"))) {
			return CommandType.doorDisarm;
		}

		// Command Type: No_GPS_Move_Fail
		if (body.contains(getResources().getString(R.string.noGpsMoveFail)))
			return CommandType.noGpsMoveFail;

		// Command Type: noadmin
		if (body.contains(getResources().getString(R.string.deleteAuthorizationReply)))
			return CommandType.deleteAuthorization;
		
		// Command Type: admin
		if (body.contains(getResources().getString(R.string.authorizationReply)))
			return CommandType.authorization;
		
		// Command Type: nomove
		if (body.contains(getResources().getString(R.string.movementAlarmCancelReply)))
			return CommandType.movementAlarmCancel;

		// Command Type: move
		if (body.contains(getResources().getString(R.string.movementAlarmReply)))
			return CommandType.movementAlarm;
		
		if (body.contains(getResources().getString(R.string.powerSystemReply)))
			return CommandType.PowerSystem;

		if (body.contains(getResources().getString(R.string.resumePowerSystemReply)))
			return CommandType.resumePowerSystem;

		if (body.contains(getResources().getString(R.string.passwordFail)))
			return CommandType.passwordFail;

		if (body.contains("fail") && body.contains("password"))
			return CommandType.wrongPasswordCommand;
		
		if (body.contains(getResources().getString(R.string.timeZoneSetupReply)))
			return CommandType.timeZoneSetup;
		

		
//		if (body.contains(getResources().getString(R.string.adminIpSetupReply)) ||
//				body.contains(getResources().getString(R.string.adminIpSetupReply945)))
//			return CommandType.adminIPSetup;
		
		if (body.contains("adminip") && body.contains("ok") )
			return CommandType.adminIPSetup;
		
		if (body.contains(getResources().getString(R.string.gprsSetupReply)))
			return CommandType.gprsSetup;

		if (body.contains(getResources().getString(R.string.noQuickStopReply)))
			return CommandType.noQuickStop;
		
		////////////
		if (body.contains(getResources().getString(R.string.reset)))
			return CommandType.reset;
		
		if ((body.length() == 15 && body.matches("[0-9]+")) || 
				body.toLowerCase(Locale.getDefault()).contains("imei:"))  // for padra model 945
		{
			return CommandType.imei;
		}
		
//		if (body.matches("[0-9]+") && body.length() > 2) {
//		}
		
		if (body.contains("odo:") && body.contains("dtime:") && body.contains("pload:")) {
			return CommandType.diag;
		}
		
//		865328021056345
//		t:24/06/15 12:39:22
//		odo:215682
//		dtime:83
//		speed:000
//		pload:0.0%
//		temp:+
//		atp:0.0%
//		rpm:00000
//		bat:13.2
//		dtc:   

		// p945 Bafande device info
		// 117788
		// 09101978805
		
		
		if (body.toString().contains(getResources().getString(R.string.helpMeAlarmOK))
				|| (body.contains("help") && body.contains("me") && body.contains("ok"))) {
			return CommandType.helpme;
		}
		
		// //////////////////////////ALERTS///////////////////////

		isAlarm = true;

		if (body.toString().contains(
				getResources().getString(R.string.movementAlarmActivated)))
			return CommandType.movementAlarmActivated;

		if (body.toString().contains(getResources().getString(R.string.helpMeAlarmActivated)) 
				&& !body.toString().contains("ok")) {
			return CommandType.helpMeAlarmActivated;
		}
		
		if (body.toString().contains(
				getResources().getString(R.string.doorOpenAlarmActivated)))
			return CommandType.doorOpenAlarmActivated;

		if (body.toString().contains(
				getResources().getString(R.string.shockSensorAlarmActivated)))
			return CommandType.shockSensorAlarmActivated;

		if (body.toString().contains(
				getResources().getString(R.string.accidentAlarmActivated)))
			return CommandType.accidentAlarmActivated;

		try {
			if (body.toString().substring(0,7).contains(
					getResources().getString(R.string.speedLimitAlarmActivated))
					)
				return CommandType.speedLimitAlarmActivated;
		} catch (Exception e) {
			if (body.toString().substring(0,5).contains(
					getResources().getString(R.string.speedLimitAlarmActivated))
					)
				return CommandType.speedLimitAlarmActivated;
		}


		if (body.toString().contains(
				getResources().getString(R.string.accOnAlarmActivated)))
			return CommandType.accOnAlarmActivated;

		if (body.toString().contains(
				getResources().getString(R.string.accOffAlarmActivated)))
			return CommandType.accOffAlarmActivated;

		if (body.toString().contains(
				getResources().getString(R.string.lowBatteryAlarmActivated)))
			return CommandType.lowBatteryAlarmActivated;

		if (body.toString().contains(
				getResources().getString(R.string.powerAlarmActivated)))
			return CommandType.powerAlarmActivated;

		if (body.toString().contains(
				getResources().getString(R.string.noGpsAlarmActivated)))
			return CommandType.noGpsAlarmActivated;

		if (body.toString().contains(
				getResources().getString(R.string.outOfFenceAlarmActivated)))
			return CommandType.outOfFenceAlarmActivated;
		
		if (body.toString().contains("arm") && body.toString().contains("fail")) {
			
		}
				
		if (body.toString().contains(
				getResources().getString(R.string.lowFuelAlarmActivated)))
			return CommandType.lowFuelAlarmActivated;
		
		else {
			isAlarm = false;
			if (body.contains("lat") && body.toString().contains("lon")) {
				System.out.println("LatLong command");
				return CommandType.latLong;
			}
			return CommandType.UNKNOWN;
		}
	}
	
	public static boolean isNumeric(String str) {
	    for (char c : str.toCharArray()) {
	        if (!Character.isDigit(c)) return false;
	    }
	    return true;
	}
}