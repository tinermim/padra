package com.smartLab.padra.SMS;

import com.example.padra.R;

public enum CommandType {

	initialization(R.string.initialization), 
	authorization(R.string.authorization), 
	deleteAuthorization(R.string.deleteAuthorization), 
	movementAlarm(R.string.movementAlarm), 
	movementAlarmCancel(R.string.movementAlarmCancel), 
	changePassword(R.string.changePassword), 
	PowerSystem(R.string.powerSystem), 
	resumePowerSystem(R.string.resumePowerSystem), 
	doorArm(R.string.doorArm), 
	doorDisarm(R.string.doorDisarm), 
	checkState(R.string.checkState), 
	passwordFail(R.string.passwordFail), 
	tracker(R.string.tracker), 
	monitor(R.string.monitor), 
	suppress(R.string.suppress), 
	imei(R.string.imei), 
	postAddress(R.string.postAddress), 
	speedLimit(R.string.speedLimit), 
	postAddressFail(R.string.postAddressSetupFailReply), 
	undoSuppress(R.string.undoSuppress), 
	carTrunk(R.string.carTrunk), 
	noQuickStop(R.string.noQuickStop), 
	start(R.string.start), 
	adminIPSetup(R.string.adminIpSetup), 
	gprsSetup(R.string.gprsSetup), 
	timeZoneSetup(R.string.timeZoneSetup), 
	trackUnlimitedInterval(R.string.trackUnlimitedInterval), 
	latLong(R.string.latLong),
	movementAlarmActivated(R.string.movementAlarmActivated), 
	helpMeAlarmActivated(R.string.helpMeAlarmActivated), 
	doorOpenAlarmActivated(R.string.doorOpenAlarmActivated), 
	shockSensorAlarmActivated(R.string.shockSensorAlarmActivated), 
	accidentAlarmActivated(R.string.accidentAlarmActivated), 
	speedLimitAlarmActivated(R.string.speedLimitAlarmActivated), 
	accOnAlarmActivated(R.string.accOnAlarmActivated), 
	accOffAlarmActivated(R.string.accOffAlarmActivated), 
	lowBatteryAlarmActivated(R.string.lowBatteryAlarmActivated), 
	powerAlarmActivated(R.string.powerAlarmActivated), 
	noGpsAlarmActivated(R.string.noGpsAlarmActivated), 
	outOfFenceAlarmActivated(R.string.outOfFenceAlarmActivated), 
	lowFuelAlarmActivated(R.string.lowFuelAlarmActivated), 
	noGpsMoveFail(R.string.noGpsMoveFail),
	reset(R.string.reset),
	diag(R.string.diag),
	nospeed(R.string.nospeed),
	helpme(R.string.helpme),
	trackUnlimitedIntervalCustom(R.string.trackUnlimitedIntervalCustom),
	wrongPasswordCommand(R.string.wrongPasswordCommand),
	UNKNOWN(R.string.unknown);
	
	private final int string_id;

	private CommandType(int type) {
		this.string_id = type;
	}

	public int getStringID() {
		return string_id;
	}
}
