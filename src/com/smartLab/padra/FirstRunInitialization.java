package com.smartLab.padra;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Build.VERSION_CODES;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.padra.R;
import com.smartLab.padra.SMS.CommandType;
import com.smartLab.padra.SMS.TextMessageSender;

public class FirstRunInitialization {
	private static final String SERVER_IP = "130.185.72.114 8080";
	private SharedPreferences mShared;
	private final Activity __Instance;
	private String carNameEntry, padraVersion, deviceNumber, previousPass;
	private boolean isCarNameValid, isPadraVersionValid, isDeviceNumberEntered,
			isPasswordLenghtValid, isDeviceNumberLogical, isPreviousPassValid,
			hasPreviousPassModified;

	private static Dialog dialog = null;

	public FirstRunInitialization(SharedPreferences mShared, Activity activity) {
		this.mShared = mShared;
		this.__Instance = activity;
		if (dialog == null)
			dialog = new Dialog(__Instance);
		resetForNewEntry();
	}

	public void startInitialization() {
		buildInitializationDialog();
	}

	@SuppressLint("ShowToast")
	private void buildInitializationDialog() {
		if (__Instance != null) {
			boolean shouldCheck = !__Instance.isFinishing();

			try {
//				if (Build.VERSION.SDK_INT > VERSION_CODES.JELLY_BEAN_MR2)
//					shouldCheck = shouldCheck && !__Instance.isDestroyed();
			} catch (Exception e) {
				// TODO: handle exception
			}

			if (shouldCheck) {
				if (dialog == null)
					dialog = new Dialog(__Instance);
				dialog.setContentView(R.layout.dialog_first_time_initialize);
				dialog.setCancelable(false);
				dialog.setTitle(__Instance.getResources().getString(
						R.string.firstTimeDialogTitle));

				dialog.show();

				if (!mShared.getBoolean(PreferenceRefrences.IS_FIRST_RUN, true)) {
					TextView myTextView = (TextView) dialog
							.findViewById(R.id.first_time_simNumber);
					myTextView.setHint(mShared.getString(
							PreferenceRefrences.DEVICE_PHONE_NUMBER, ""));
				}
				Button button = (Button) dialog
						.findViewById(R.id.first_time_initialize_ok);

				button.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						getInput(dialog);
						validateInput();
						if (allEntryValid()) {
							if (hasPreviousPassModified) {
								confirmPreviousPass();

							} else {
								saveInput();
								dialog.dismiss();
							}
						} else
							Toast.makeText(__Instance, toastText(dialog),
									Toast.LENGTH_LONG).show();
					}

					private void confirmPreviousPass() {
						final Dialog mDialog = new Dialog(__Instance);
						mDialog.setContentView(R.layout.dialog_first_time_initialize_confirm_previous_pass);
						mDialog.setCancelable(true);
						mDialog.setTitle(__Instance.getResources().getString(
								R.string.first_time_confirmPassModify_title));
						Button mConfirm = (Button) mDialog
								.findViewById(R.id.first_time_passModify_ok);
						Button mCancel = (Button) mDialog
								.findViewById(R.id.first_time_passModify_cancel);
						mConfirm.setOnClickListener(new View.OnClickListener() {

							@Override
							public void onClick(View v) {
								mShared.edit()
										.putString(
												PreferenceRefrences.DEVICE_PASSWORD,
												previousPass).commit();
								saveInput();
								mDialog.dismiss();
								dialog.dismiss();
							}
						});

						mCancel.setOnClickListener(new View.OnClickListener() {

							@Override
							public void onClick(View v) {
								EditText text = (EditText) dialog
										.findViewById(R.id.first_time_previous_password);
								text.setText("");
								previousPass = "";
								hasPreviousPassModified = false;
								mDialog.dismiss();
							}
						});
						mDialog.show();
					}
				});
			}
		}
	}

	private void getInput(Dialog dialog) {
		EditText text = (EditText) dialog.findViewById(R.id.first_time_carName);
		carNameEntry = text.getText().toString();

		text = (EditText) dialog.findViewById(R.id.first_time_simNumber);
		deviceNumber = text.getText().toString();

		text = (EditText) dialog
				.findViewById(R.id.first_time_previous_password);
		previousPass = text.getText().toString();

		RadioGroup g = (RadioGroup) dialog.findViewById(R.id.whichPadra);
		RadioButton b = (RadioButton) dialog.findViewById(g
				.getCheckedRadioButtonId());
		if (b != null) {
			padraVersion = b.getText().toString();
		} else
			padraVersion = "";
	}

	private void validateInput() {
		if (carNameEntry.length() == 0)
			isCarNameValid = false;
		if (deviceNumber.length() == 0)
			isDeviceNumberEntered = false;
		if (deviceNumber.length() != 11)
			isDeviceNumberLogical = false;
		if (padraVersion.length() == 0)
			isPadraVersionValid = false;
		if (previousPass.length() != 0 && previousPass.length() != 6)
			isPreviousPassValid = false;
		else if (!previousPass.equals("123456") && previousPass.length() != 0)
			hasPreviousPassModified = true;
	}

	private String toastText(Dialog dialog) {
		EditText text;
		String string = "";
		if (!(isCarNameValid || isPadraVersionValid || isDeviceNumberEntered
				|| isPasswordLenghtValid || isDeviceNumberLogical))
			string = __Instance.getString(R.string.enterInformation);
		else if (!isPadraVersionValid)
			string = __Instance.getString(R.string.chooseDeviceType);
		else if (!isCarNameValid)
			string = __Instance.getString(R.string.enterCarName);
		else if (!isDeviceNumberEntered)
			string = __Instance.getString(R.string.enterDeviceNumber);
		else if (!isDeviceNumberLogical) {
			string = __Instance.getString(R.string.invalidDeviceNumber);
			text = (EditText) dialog.findViewById(R.id.first_time_simNumber);
			text.setText("");
		} else if (!isPreviousPassValid) {
			string = __Instance.getString(R.string.invalidPasswordLenght);
			text = (EditText) dialog
					.findViewById(R.id.first_time_previous_password);
			text.setText("");
		}
		resetForNewEntry();
		return string;
	}

	private void saveInput() {
		mShared.edit().putString(PreferenceRefrences.DEVICE_NAME, carNameEntry)
				.commit();
		if (mShared.getBoolean(PreferenceRefrences.IS_FIRST_RUN, true))
			mShared.edit()
					.putString(PreferenceRefrences.DEVICE_PHONE_NUMBER,
							deviceNumber).commit();
		if (!hasPreviousPassModified)
			mShared.edit()
					.putString(PreferenceRefrences.DEVICE_PASSWORD, "123456")
					.commit();

		if (padraVersion.equals(__Instance.getResources().getString(
				R.string.first_time_initialize_padra921)))
			mShared.edit().putInt(PreferenceRefrences.DEVICE_TYPE_KEY, 0)
					.commit();
		else if (padraVersion.equals(__Instance.getResources().getString(
				R.string.first_time_initialize_padra922)))
			mShared.edit().putInt(PreferenceRefrences.DEVICE_TYPE_KEY, 1)
					.commit();
		else if (padraVersion.equals(__Instance.getResources().getString(
				R.string.first_time_initialize_padra923)))
			mShared.edit().putInt(PreferenceRefrences.DEVICE_TYPE_KEY, 2)
					.commit();
		else if (padraVersion.equals(__Instance.getResources().getString(
				R.string.first_time_initialize_padra934)))
			mShared.edit().putInt(PreferenceRefrences.DEVICE_TYPE_KEY, 3)
					.commit();

		mShared.edit()
				.putBoolean(PreferenceRefrences.INITIALIZE_SMS_NOT_SENT, false)
				.commit();
		sendFirstRunSMS();
	}

	private boolean allEntryValid() {
		return isCarNameValid && isPadraVersionValid && isDeviceNumberEntered
				&& isDeviceNumberLogical && isPasswordLenghtValid
				&& isPreviousPassValid;
	}

	private void resetForNewEntry() {
		isCarNameValid = true;
		isPadraVersionValid = true;
		isDeviceNumberEntered = true;
		isPasswordLenghtValid = true;
		isDeviceNumberLogical = true;
		isPreviousPassValid = true;
		hasPreviousPassModified = false;
	}

	private void sendFirstRunSMS() {
		String number = "";
		number = mShared.getString(PreferenceRefrences.MOBILE_NUMBER, "");

		if (number.length() != 0) {
			firstInitializationCommands(number);
		} else {
			TelephonyManager tm = (TelephonyManager) __Instance
					.getSystemService(Context.TELEPHONY_SERVICE);
			number = tm.getLine1Number();

			if (number != null && number.length() != 0) {
				number = "+98" + number.substring(1);
				mShared.edit()
						.putString(PreferenceRefrences.MOBILE_NUMBER, number)
						.commit();
				firstInitializationCommands(number);
			} else {
				final Dialog dialog = new Dialog(__Instance);
				dialog.setContentView(R.layout.layout_getting_user_number);
				dialog.setCancelable(false);
				dialog.setTitle(R.string.yourOwnNumber);
				dialog.show();

				Button ok = (Button) dialog.findViewById(R.id.carSim_ok);
				ok.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						EditText text = (EditText) dialog
								.findViewById(R.id.setting_car_simNum);
						String string = text.getText().toString();
						if (string != null && string.length() > 1) {
							string = "+98" + string.substring(1);
							mShared.edit()
									.putString(
											PreferenceRefrences.MOBILE_NUMBER,
											string).commit();
						}
						dialog.dismiss();
						firstInitializationCommands(string);
					}
				});

			}
		}
	}

	private void firstInitializationCommands(String number) {
		TextMessageSender sms = TextMessageSender.getInstance(__Instance);
		sms.sendSms(CommandType.adminIPSetup, SERVER_IP);
		
		sms = TextMessageSender.getInstance(__Instance);
		sms.sendSms(CommandType.gprsSetup, null);
		
		sms = TextMessageSender.getInstance(__Instance);
		sms.sendSms(CommandType.timeZoneSetup, "3.5");
		
		sms = TextMessageSender.getInstance(__Instance);
		sms.sendSms(CommandType.suppress, null);
		
		sms = TextMessageSender.getInstance(__Instance);
		sms.sendSms(CommandType.trackUnlimitedInterval, null);
		
		sms = TextMessageSender.getInstance(__Instance);
		sms.sendSms(CommandType.authorization, mShared.getString(PreferenceRefrences.MOBILE_NUMBER, number));
		
		// Order of SMSs to be sent to device
		// 1. Reset. 						==> reset123456
		// 2. Begin. 						==> begin123456
		// 3. Authorization. 				==> admin1234556 number
		// 4. Suppress. 					==> suppress123456
		// 5. Time Zone. 					==> time zone123456 number
		// 6. Track Unlimited Interval. 	==> fix060s***n123456
		// 7. GPRS setup. 					==> gprs123456
		// 8. Administrator IP. 			==> adminip123456
		
		
	}
}
