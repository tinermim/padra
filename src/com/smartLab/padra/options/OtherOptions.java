package com.smartLab.padra.options;

import java.util.Arrays;

import org.askerov.dynamicgrid.DynamicGridView;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.padra.R;
import com.smartLab.padra.BaseSliderFragment;
import com.smartLab.padra.PreferenceRefrences;
import com.smartLab.padra.SMS.CommandType;
import com.smartLab.padra.SMS.TextMessageSender;
import com.smartLab.padra.asetup.TextMessageSenderMine;
import com.smartLab.padra.remote.FoldingPaneLayout;
import com.smartLab.padra.support.SupportActivity;
import com.smartLab.setting.SettingActivity;

public class OtherOptions extends BaseSliderFragment {
	final GridItem[] items = new GridItem[6];
	private SharedPreferences mShared;
	private Editor mEditor;
	private int oldPosition = 0;
	private int newPosition = 0;
	public FoldingPaneLayout mPaneLayout;
	public ListView mPaneList;
	
	@Override
	public void onResume() {
		final DynamicGridView gridView;
		gridView = (DynamicGridView) getActivity().findViewById(
				R.id.dynamic_grid);

		items[mShared.getInt(0 + "", 0)] = new GridItem(getActivity()
				.getResources().getString(R.string.settings),
				R.drawable.settings, 0);
		items[mShared.getInt(1 + "", 1)] = new GridItem(getActivity()
				.getString(R.string.postalAddress), R.drawable.envelope, 1);
		items[mShared.getInt(2 + "", 2)] = new GridItem(getActivity()
				.getString(R.string.support), R.drawable.contact_us, 2);
		items[mShared.getInt(3 + "", 3)] = new GridItem(getActivity()
				.getString(R.string.carVoice), R.drawable.car_sound, 3);
		items[mShared.getInt(4 + "", 4)] = new GridItem(getActivity()
				.getString(R.string.Archive), R.drawable.archive, 4);
		
		items[mShared.getInt(5 + "", 5)] = new GridItem(getActivity()
				.getString(R.string.diagnoise), R.drawable.diagnoise_icon, 5);
		
//		for (int i = 0; i < items.length; i++)
//			Log.i("first",
//					items[i].id + " " + i + " "
//							+ mShared.getInt(items[i].id + "", i));

		final Activity a = this.getActivity();
		gridView.setAdapter(new ItemDynamicAdapter(a, Arrays.asList(items),
				getResources().getInteger(R.integer.options_column_count)));
		// add callback to stop edit mode if needed
		gridView.setOnDropListener(new DynamicGridView.OnDropListener() {
			@Override
			public void onActionDrop() {
				gridView.stopEditMode();
				gridView.setAdapter(new ItemDynamicAdapter(a, Arrays
						.asList(items), getResources().getInteger(
						R.integer.options_column_count)));
			}
		});

		gridView.setOnDragListener(new DynamicGridView.OnDragListener() {
			@Override
			public void onDragStarted(int position) {
				// Log.d(TAG, "drag started at position " + position);
			}

			@Override
			public void onDragPositionsChanged(int oldP, int newP) {

				oldPosition = oldP;
				newPosition = newP;
				int counter = 0;


				if (newPosition > oldPosition) {
					counter = newPosition;
					mEditor.putInt(items[oldPosition].id + "", newPosition)
							.commit();

					for (; counter != oldPosition; counter -= 1) {
						mEditor.putInt(
								items[counter].id + "",
								mShared.getInt(items[counter].id + "", counter) - 1)
								.commit();
					}

				} else {
					counter = newPosition;
					mEditor.putInt(items[oldPosition].id + "", newPosition)
							.commit();

					for (; counter != oldPosition; counter += 1)
						mEditor.putInt(
								items[counter].id + "",
								mShared.getInt(items[counter].id + "", counter) + 1)
								.commit();

				}

				items[mShared.getInt(0 + "", 0)] = new GridItem(getActivity()
						.getResources().getString(R.string.settings),
						R.drawable.settings, 0);
				items[mShared.getInt(1 + "", 1)] = new GridItem(getActivity()
						.getString(R.string.postalAddress),
						R.drawable.envelope, 1);
				items[mShared.getInt(2 + "", 2)] = new GridItem(getActivity()
						.getString(R.string.support), R.drawable.contact_us, 2);
				items[mShared.getInt(3 + "", 3)] = new GridItem(getActivity()
						.getString(R.string.carVoice), R.drawable.car_sound, 3);
				items[mShared.getInt(4 + "", 4)] = new GridItem(getActivity()
						.getString(R.string.Archive), R.drawable.archive, 4);
				items[mShared.getInt(5 + "", 5)] = new GridItem(getActivity()
						.getString(R.string.diagnoise), R.drawable.diagnoise_icon, 5);
				

			}
		});
		gridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view,
					int position, long id) {
				gridView.startEditMode(position);
				return true;
			}
		});

		gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				if (items[position].id == 0) {
					Intent myIntent = new Intent(getActivity(),
							SettingActivity.class);
					getActivity().startActivity(myIntent);

				} else if (items[position].id == 1) {
					TextMessageSender sms = TextMessageSender
							.getInstance(getActivity());
					sms.sendSms(CommandType.postAddress, "");

				} else if (items[position].id == 2) {
					Intent myIntent = new Intent(getActivity(),
							SupportActivity.class);
					getActivity().startActivity(myIntent);
				} else if (items[position].id == 3) {
					TextMessageSender sms = TextMessageSender
							.getInstance(getActivity());
					sms.sendSms(CommandType.monitor, "");
					mShared.edit()
							.putLong(PreferenceRefrences.CAR_VOICE_START_TIME,
									System.currentTimeMillis()).commit();

				} else if (items[position].id == 4) {

					Intent intent = new Intent(getActivity(), Archive.class);
					startActivity(intent);
					
				} else if (items[position].id == 5) {
					
					if (mShared.getInt(PreferenceRefrences.DEVICE_TYPE_KEY, 3) == 4) {
//						Intent intent = new Intent(getActivity(), DiagnoiseActivity.class);
//						startActivity(intent);
						TextMessageSender sms = TextMessageSender
								.getInstance(getActivity());
						sms.sendSms(CommandType.diag, null);
					} else {
						Toast.makeText(getActivity(), getResources().getString(R.string.notValidOperation), Toast.LENGTH_SHORT).show();
						
					}
					
				}
				

			}
		});

		setDrawableAnimation(R.id.animationSwipeLeftOtherOptions,
				R.drawable.animation_left);
		setDrawableAnimation(R.id.animationSwipeRightOtherOptions,
				R.drawable.animation_right);

		super.onResume();
	}

	public static BaseSliderFragment newInstance(int position) {
		BaseSliderFragment f = new OtherOptions();
		Bundle bundle = new Bundle();
		bundle.putInt(POSITION_KEY, position);
		f.setArguments(bundle);
		return f;
	}

	@Override
	protected View getLayout(LayoutInflater inflater, ViewGroup container,
			boolean b) {
		View v = inflater.inflate(R.layout.fragments_other_options_view,
				container, false);
		mShared = PreferenceManager.getDefaultSharedPreferences(getActivity());
		mEditor = mShared.edit();
		return v;
	}

}