package com.smartLab.padra.options;


public class GridItem {
	public String title;
	public int imageViewId;
	public int id;

	public GridItem(String title, int imageViewId, int id) {
		this.title = title;
		this.imageViewId = imageViewId;
		this.id = id;
	}
}
