package com.smartLab.padra.options;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.example.padra.R;
import com.smartLab.padra.SMS.TextMessageSender;
import com.smartLab.padra.commands.AnimatedExpandableListView;
import com.smartLab.padra.commands.AnimatedExpandableListView.AnimatedExpandableListAdapter;
import com.smartLab.padra.remote.FoldingPaneLayout;

public class Archive extends ActionBarActivity {
	public AnimatedExpandableListView listView;
	private ExampleAdapter adapter;
	private List<GroupItem> items;
	private static float Hue;
	public FoldingPaneLayout mPaneLayout;
	public ListView mPaneList;
	private Toolbar toolbar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_archive);
		toolbar = (Toolbar) findViewById(R.id.SettingsToolbar);
		((TextView) toolbar.findViewById(R.id.SettingsTitle))
				.setText(getTitle());
		setSupportActionBar(toolbar);

	}

	public void updateListView() {
		ArrayList<ArchiveItem> commands = TextMessageSender.getInstance(this)
				.getArchiveLog();
		items = new ArrayList<GroupItem>();
		int counter = 0;

		if (commands.size() > 0) {
			counter = commands.size() - 1;

			while (counter >= 0) {
				GroupItem item = new GroupItem();
				Calendar c = commands.get(counter).time;
				item.title = c.get(Calendar.DAY_OF_MONTH) + "/"
						+ (c.get(Calendar.MONTH) + 1) + "/" + c.get(Calendar.YEAR);
				while (counter >= 0
						&& c.get(Calendar.DAY_OF_MONTH) == commands
								.get(counter).time.get(Calendar.DAY_OF_MONTH)
						&& c.get(Calendar.MONTH) == commands.get(counter).time
								.get(Calendar.MONTH)
						&& c.get(Calendar.YEAR) == commands.get(counter).time
								.get(Calendar.YEAR)) {
					ChildItem child = new ChildItem();
					child.time = commands.get(counter).time
							.get(Calendar.HOUR_OF_DAY)
							+ ":"
							+ commands.get(counter).time.get(Calendar.MINUTE);

					child.phoneNumber = commands.get(counter).phoneNumber;

					child.content = commands.get(counter).content;

					item.items.add(child);
					counter--;
				}

				if (item.items.size() > 0)
					items.add(item);
			}
		}

		adapter.setData(items);

		listView.setAdapter(adapter);
	}

	@Override
	public void onResume() {

		adapter = new ExampleAdapter(this);
		listView = (AnimatedExpandableListView) this
				.findViewById(R.id.listView);

		updateListView();

		// In order to show animations, we need to use a custom click
		// handler
		// for our ExpandableListView.
		listView.setOnGroupClickListener(new OnGroupClickListener() {
			@Override
			public boolean onGroupClick(ExpandableListView parent, View v,
					int groupPosition, long id) {
				// We call collapseGroupWithAnimation(int) and
				// expandGroupWithAnimation(int) to animate group
				// expansion/collapse.
				if (listView.isGroupExpanded(groupPosition)) {
					listView.collapseGroupWithAnimation(groupPosition);
				} else
					listView.expandGroupWithAnimation(groupPosition);

				return true;
			}
		});

		super.onResume();
	}

	private static class GroupItem {
		String title;
		List<ChildItem> items = new ArrayList<ChildItem>();
	}

	private static class ChildItem {
		String time;
		String phoneNumber;
		String content;
	}

	private static class ChildHolder {
		TextView time;
		TextView phoneNumber;
		TextView content;
	}

	private static class GroupHolder {
		TextView title;
	}

	/**
	 * Adapter for our list of {@link GroupItem}s.
	 */
	private class ExampleAdapter extends AnimatedExpandableListAdapter {
		private LayoutInflater inflater;

		private List<GroupItem> items;

		public ExampleAdapter(Context context) {
			inflater = LayoutInflater.from(context);
		}

		public void setData(List<GroupItem> items) {
			this.items = items;
		}

		@Override
		public ChildItem getChild(int groupPosition, int childPosition) {
			return items.get(groupPosition).items.get(childPosition);
		}

		@Override
		public long getChildId(int groupPosition, int childPosition) {
			return childPosition;
		}

		@Override
		public View getRealChildView(int groupPosition, int childPosition,
				boolean isLastChild, View convertView, ViewGroup parent) {
			ChildHolder holder;
			ChildItem item = getChild(groupPosition, childPosition);
			holder = new ChildHolder();

			convertView = inflater.inflate(R.layout.list_item_status, parent,
					false);

			holder.time = (TextView) convertView.findViewById(R.id.commandTime);
			holder.phoneNumber = (TextView) convertView
					.findViewById(R.id.phoneNumber);
			holder.content = (TextView) convertView
					.findViewById(R.id.smsContent);

			convertView.setTag(holder);

			holder.time.setText(item.time);
			holder.phoneNumber.setText(item.phoneNumber);
			holder.content.setText(item.content);

			Hue = (groupPosition * 60 / getGroupCount()) + 150;
			convertView.setBackgroundColor(Color.HSVToColor(100, new float[] {
					Hue, 0.05f, 1f }));

			return convertView;
		}

		@Override
		public int getRealChildrenCount(int groupPosition) {
			return items.get(groupPosition).items.size();
		}

		@Override
		public GroupItem getGroup(int groupPosition) {
			return items.get(groupPosition);
		}

		@Override
		public int getGroupCount() {
			return items.size();
		}

		@Override
		public long getGroupId(int groupPosition) {
			return groupPosition;
		}

		@Override
		public View getGroupView(int groupPosition, boolean isExpanded,
				View convertView, ViewGroup parent) {
			GroupHolder holder;
			GroupItem item = getGroup(groupPosition);
			if (convertView == null) {
				holder = new GroupHolder();
				convertView = inflater.inflate(R.layout.group_item, parent,
						false);
				holder.title = (TextView) convertView
						.findViewById(R.id.textTitle);
				convertView.setTag(holder);
			} else {
				holder = (GroupHolder) convertView.getTag();
			}

			holder.title.setText(item.title);

			Hue = (groupPosition * 50 / getGroupCount()) + 150;
			// TODO
			convertView.setBackgroundColor(Color.HSVToColor(85, new float[] {
					Hue, 0.5f, 1f }));

			return convertView;
		}

		@Override
		public boolean hasStableIds() {
			return true;
		}

		@Override
		public boolean isChildSelectable(int arg0, int arg1) {
			return true;
		}

	}

}