package com.smartLab.padra;

//import io.fabric.sdk.android.Fabric;
import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.astuetz.PagerSlidingTabStrip;
import com.astuetz.PagerSlidingTabStrip.IconTabProvider;
//import com.crashlytics.android.Crashlytics;
import com.example.padra.R;
import com.haibison.android.lockpattern.LockPatternActivity;
import com.smartLab.padra.SMS.CommandType;
import com.smartLab.padra.SMS.TextMessageSender;
import com.smartLab.padra.commands.Alerts;
import com.smartLab.padra.location.MapControllerFragment;
import com.smartLab.padra.options.OtherOptions;
import com.smartLab.padra.remote.FoldingPaneLayout;
import com.smartLab.padra.remote.RemoteController;
import com.smartLab.padra.status.Status;
import com.smartLab.padra.utils.Screen;

public class MainActivity extends ActionBarActivity implements
		OnPageChangeListener {
	private static final int REQ_ENTER_PATTERN = 2;
	private static final String DEVICE_DEFAULT_PASSWORD = "123456";
	private static final int NUM_PAGES = 5;
	public int currentPosition = -1;

	private int[] slideIcons = { R.drawable.home_not_select,
			R.drawable.map_not_select, R.drawable.alert_not_select,
			R.drawable.state_not_select, R.drawable.other_options_not_select };

	private int[] slideSelectedIcons = { R.drawable.home_select,
			R.drawable.map_select, R.drawable.alert_select,
			R.drawable.state_select, R.drawable.other_options_select };

	private ViewPager mPager;
	public BaseSliderFragment[] fragmentsList;
	private PagerAdapter mPagerAdapter;
	private Toolbar toolbar;
	private static boolean isUp = false;
	private static SharedPreferences mShared;
	private final MainActivity __instance = this;
	private static ProgressDialog pDialog;
	private static Builder alert = null;

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		// TODO
		int retryCount = 0;
		if (data != null)
			retryCount = data.getIntExtra(
					LockPatternActivity.EXTRA_RETRY_COUNT, 0);

		switch (requestCode) {
		case REQ_ENTER_PATTERN: {
			/*
			 * NOTE that there are 4 possible result codes!!!
			 */
			switch (resultCode) {
			case RESULT_OK:
				Toast.makeText(getApplicationContext(), getResources()
						.getString(R.string.confirm_password),
						Toast.LENGTH_SHORT);
				break;
			case RESULT_CANCELED:
				if (data == null) {
					PreferenceManager
							.getDefaultSharedPreferences(
									getApplicationContext())
							.edit()
							.putBoolean(
									PreferenceRefrences.IS_APP_PASSWORD_NEEDED_KEY,
									false).commit();
					Toast.makeText(
							getApplicationContext(),
							getResources().getString(
									R.string.pattern_password_not_supported),
							Toast.LENGTH_LONG).show();
				}
				finish();
				break;
			case LockPatternActivity.RESULT_FAILED:
				// The user failed to enter the pattern
				Toast.makeText(getApplicationContext(), getResources()
						.getString(R.string.wrongPassword), Toast.LENGTH_SHORT);
				if (retryCount > 4)
					finish();
				break;
			case LockPatternActivity.RESULT_FORGOT_PATTERN:
				// The user forgot the pattern and invoked your recovery
				// Activity.
				break;
			default:
				PreferenceManager
						.getDefaultSharedPreferences(getApplicationContext())
						.edit()
						.putBoolean(PreferenceRefrences.IS_APP_PASSWORD_NEEDED_KEY,
								false).commit();
				Toast.makeText(
						getApplicationContext(),
						getResources().getString(
								R.string.pattern_password_not_supported),
						Toast.LENGTH_LONG).show();
				break;
			}

			break;
		}// REQ_ENTER_PATTERN
		}
	}

	private void checkLockPattern() {
		if (mShared.getBoolean(PreferenceRefrences.IS_APP_PASSWORD_NEEDED_KEY,
				false)) {
			String password = mShared.getString(
					PreferenceRefrences.APP_PASSWORD_KEY, "");
			if (!password.equals("")) {
				char[] pattern = password.toCharArray();
				Intent intent = new Intent(
						LockPatternActivity.ACTION_COMPARE_PATTERN, null,
						getApplicationContext(), LockPatternActivity.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				intent.putExtra(LockPatternActivity.EXTRA_PATTERN, pattern);
				startActivityForResult(intent, REQ_ENTER_PATTERN);
			}
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
//		Fabric.with(this, new Crashlytics());
		setContentView(R.layout.activity_main);
		setToolbar();

		mShared = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		
		// TODO
		if (isFirstRun() && !mShared.getBoolean(PreferenceRefrences.INITIALIZE_SMS_NOT_SENT, true))
			mShared.edit().putBoolean(PreferenceRefrences.INITIALIZE_SMS_NOT_SENT, true).commit();
		
		checkLockPattern();
		
//		06-15 10:36:16.430: I/Google Maps Android API(25058): Google Play services client version: 7327000
//		06-15 10:36:16.530: I/Google Maps Android API(25058): Google Play services package version: 7329034

		
		
	}

	@Override
	protected void onResume() {		
		mShared.edit().putBoolean(PreferenceRefrences.APP_VISIBILITY, true).commit();
		
		if (isFirstRun() && mShared.getBoolean(PreferenceRefrences.INITIALIZE_SMS_NOT_SENT, true)) {
		
			mShared.edit().putString(PreferenceRefrences.DEVICE_PASSWORD, DEVICE_DEFAULT_PASSWORD).commit();
			mShared.edit().putBoolean(PreferenceRefrences.IS_IMEI_VALIDATED_KEY, false).commit();
			
//			FirstRunInitialization firstTimeInitialization = new FirstRunInitialization(mShared, MainActivity.this);
//			firstTimeInitialization.startInitialization();
		
		} else {

			// TODO removing test comments
			if (mShared.getBoolean(PreferenceRefrences.IS_IMEI_VALIDATED_KEY, false)) {
				updateSlidePages();
			} else if (!isFirstRun()) {
				checkImeiValidation();
			}

		}

		//FirstRunHelpInformation(); 
		super.onResume();
	}

	private void FirstRunHelpInformation() {
		if(!mShared.getBoolean(PreferenceRefrences.FIRST_RUN_HELP_INFO_KEY, false)){
			mShared.edit().putBoolean(PreferenceRefrences.FIRST_RUN_HELP_INFO_KEY, true).commit();
			new AlertDialog.Builder(MainActivity.this).
			setTitle(R.string.firstTimeDialogTitle).
			setMessage(R.string.first_run_help_information).
			setCancelable(false).
			setPositiveButton(R.string.confirm, null).
			show();
		}
	}

	private boolean isFirstRun() {
		if (mShared.getBoolean(PreferenceRefrences.IS_FIRST_RUN, true)) {
			mShared.edit()
					.putString(PreferenceRefrences.DEVICE_PASSWORD,
							DEVICE_DEFAULT_PASSWORD).commit();
			return true;
		}
		return false;
	}

	private int getDrawer(int currentPosition) {
		if (currentPosition == 0)
			return R.id.drawer_layout;
		else if (currentPosition == 1)
			return R.id.drawer_layout_location;
		else if (currentPosition == 2)
			return R.id.drawer_layout_alert;
		else
			return R.id.drawer_layout_status;
	}

	private void setToolbar() {
		toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		toolbar.findViewById(R.id.toolbarDrawerNavigation).setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						FoldingPaneLayout mPaneLayout = (FoldingPaneLayout) __instance
								.findViewById(getDrawer(currentPosition));

						if (currentPosition < 4)
							if (mPaneLayout.isOpen())
								mPaneLayout.closePane();
							else
								mPaneLayout.openPane();

					}
				});

		fragmentsList = new BaseSliderFragment[NUM_PAGES];
		fragmentsList[0] = RemoteController.newInstance(0);
		fragmentsList[1] = MapControllerFragment.newInstance(1);
		fragmentsList[2] = Alerts.newInstance(2);
		fragmentsList[3] = com.smartLab.padra.status.Status.newInstance(3);
		fragmentsList[4] = OtherOptions.newInstance(4);
		mPager = (ViewPager) findViewById(R.id.pager);
		mPager.setOffscreenPageLimit(6);
		mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
		mPager.setAdapter(mPagerAdapter);

		PagerSlidingTabStrip tabs = (PagerSlidingTabStrip) toolbar
				.findViewById(R.id.tabs);
		setTabsPadding(tabs);
		tabs.setViewPager(mPager);
		tabs.setOnPageChangeListener(__instance);
		overrideFonts(__instance, findViewById(R.id.PadraMainLayout));
	}

	private void overrideFonts(final Context context, final View v) {
		try {
			if (v instanceof ViewGroup) {
				ViewGroup vg = (ViewGroup) v;
				for (int i = 0; i < vg.getChildCount(); i++) {
					View child = vg.getChildAt(i);
					overrideFonts(context, child);
				}
			} else if (v instanceof TextView) {
				((TextView) v).setTypeface(Typeface.createFromAsset(
						context.getAssets(), "Yekan.ttf"));
			}
		} catch (Exception e) {
		}
	}

	private void setTabsPadding(final PagerSlidingTabStrip tabs) {
		int screenSize = getResources().getConfiguration().screenLayout
				& Configuration.SCREENLAYOUT_SIZE_MASK;
		switch (screenSize) {
		case Configuration.SCREENLAYOUT_SIZE_LARGE:
			tabs.setTabPaddingLeftRight(Screen.dpToPixels(8, this));
			break;
		case Configuration.SCREENLAYOUT_SIZE_NORMAL:
			tabs.setTabPaddingLeftRight(Screen.dpToPixels(2, this));
			break;
		case Configuration.SCREENLAYOUT_SIZE_SMALL:
			tabs.setTabPaddingLeftRight(Screen.dpToPixels(0, this));
			break;
		default:
			tabs.setTabPaddingLeftRight(Screen.dpToPixels(-2, this));
		}
	}

	@Override
	public void onPageScrolled(final int position, final float positionOffset,
			int positionOffsetPixels) {
		currentPosition = position;
		// change slide image according to slide positionOffset
		(fragmentsList[position]).changeImage(positionOffset, fragmentsList);

		try {
			RemoteController.mPaneLayout.closePane();
		} catch (Exception e) {
		}

	}

	@Override
	public void onPageSelected(int arg0) {
		System.out.println("page selected: " + arg0);
		if (arg0 == 2)
			((Alerts) fragmentsList[arg0]).updateListView();
	}

	private void updateSlidePages() {
		if (pDialog != null)
			pDialog.dismiss();
		if (fragmentsList != null) {
			Alerts alertsFragment = (Alerts) fragmentsList[2];
			Status statusFragment = (Status) fragmentsList[3];
			MapControllerFragment mapFragment = (MapControllerFragment) fragmentsList[1];

			if (alertsFragment != null) {
				if (alertsFragment.listView != null)
					alertsFragment.updateListView();
			}
			if (statusFragment != null) {
				if (statusFragment.powerSystem != null
						&& statusFragment.battery != null)
					statusFragment.updateStateViews();
			}
			if (mapFragment != null) {
				if (mapFragment.map != null
						&& mShared
								.getBoolean(
										PreferenceRefrences.IS_LOOKING_FOR_CAR_LOCATION,
										false)) {
					mapFragment.showCarLocationOnMap();
				} else if (mapFragment.map == null)
					Toast.makeText(__instance, R.string.make_map_ready,
							Toast.LENGTH_SHORT).show();
			}
		}
	}

	private void checkImeiValidation() {
		if (alert == null) {
			alert = new AlertDialog.Builder(this);
			alert.setTitle(getResources().getString(
					R.string.imei_validation_check));
			alert.setCancelable(false);

			alert.setMessage(getResources().getString(
					R.string.imei_process_not_done));
			alert.setNegativeButton(
					this.getResources().getString(R.string.imei_check_start),
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int id) {
							pDialog = new ProgressDialog(__instance);
							pDialog.setMessage(__instance
									.getString(R.string.imeiCheking));
							pDialog.setCancelable(false);
							pDialog.show();
							new TextMessageSender(__instance).sendSms(
									CommandType.imei, null);
						}
					});
			alert.show();
		}
	}

	@Override
	protected void onStop() {
		mShared.edit().putBoolean(PreferenceRefrences.APP_VISIBILITY, false).commit();
		super.onStop();
	}

	public void alertButtonClicked(View arg) {
		if (mShared.getInt(PreferenceRefrences.DEVICE_TYPE_KEY, 3) == 4) {
			Toast.makeText(this, getResources().getString(R.string.notValidOperation), Toast.LENGTH_SHORT).show();
		} else {
			TextMessageSender sender = TextMessageSender.getInstance(this);
			sender.sendSms(CommandType.noQuickStop, null);
		}
	}

	public void disalertButtonClicked(View arg) {
		if (mShared.getInt(PreferenceRefrences.DEVICE_TYPE_KEY, 3) == 4) {
			Toast.makeText(this, getResources().getString(R.string.notValidOperation), Toast.LENGTH_SHORT).show();
		} else {
			TextMessageSender sender = TextMessageSender.getInstance(this);
			sender.sendSms(CommandType.resumePowerSystem, null);
		}
	}

	@SuppressLint("NewApi")
	public void importantBoxShowButtonCLicked(View arg) {
		ObjectAnimator angleAnim = new ObjectAnimator();
		FoldingPaneLayout main = (FoldingPaneLayout) this.findViewById(R.id.drawer_layout);
		
		RelativeLayout nav = (RelativeLayout) this.findViewById(R.id.bottomNav);
		LinearLayout coveredBox = (LinearLayout) this.findViewById(R.id.alertBox);
		final ImageButton upButton = (ImageButton) this.findViewById(R.id.upAnimationButton);

		class importantBoxAnimator implements AnimatorListener {
			@Override
			public void onAnimationEnd(Animator animation) {
			}

			@Override
			public void onAnimationCancel(Animator animation) {
			}

			@Override
			public void onAnimationRepeat(Animator animation) {
			}

			@Override
			public void onAnimationStart(Animator animation) {
			}
		}
		;

		if (isUp == false) {
			angleAnim = ObjectAnimator.ofFloat(main, "y",
					nav.getY() - main.getHeight() - coveredBox.getHeight())
					.setDuration(1000);
			angleAnim.addListener(new importantBoxAnimator() {
				@Override
				public void onAnimationStart(Animator animation) {
					isUp = true;
					upButton.setBackgroundResource(R.drawable.down_alert_arrow);
				}
			});

			angleAnim.start();

		} else {
			angleAnim = ObjectAnimator.ofFloat(main, "y",
					nav.getY() - main.getHeight()).setDuration(1000);
			angleAnim.addListener(new importantBoxAnimator() {
				@Override
				public void onAnimationStart(Animator animation) {
					isUp = false;
					upButton.setBackgroundResource(R.drawable.up_alert_arrow);
				}
			});

			angleAnim.start();
		}
	}

	public void StateCenterRefreshButtonClicked(View v) {
		TextMessageSender smsHandler = TextMessageSender.getInstance(this);
		smsHandler.sendSms(CommandType.checkState, null);
	}

	/**
	 * A simple pager adapter that represents ScreenSlidePageFragment objects,
	 * in sequence.
	 */
	private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter
			implements IconTabProvider {

		public ScreenSlidePagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			return fragmentsList[position];
		}

		@Override
		public int getCount() {
			return fragmentsList.length;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return (position + 1) + "";
		}

		@Override
		public int getPageIconResId(int position) {
			return slideIcons[position];
		}

		@Override
		public int getPageSelectedIconResId(int position) {
			return slideSelectedIcons[position];
		}
	}

	@Override
	public void onPageScrollStateChanged(int arg0) {
	}
}