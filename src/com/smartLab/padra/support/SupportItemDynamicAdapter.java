package com.smartLab.padra.support;

/**
 * Author: alex askerov
 * Date: 9/9/13
 * Time: 10:52 PM
 */

import java.util.List;

import org.askerov.dynamicgrid.BaseDynamicGridAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.padra.R;
import com.smartLab.padra.utils.ImageLoader;

/**
 * Author: Ramtin Rasooli Date: 9/7/14 Time: 10:56 PM
 */
public class SupportItemDynamicAdapter extends BaseDynamicGridAdapter {
	Context context;

	public SupportItemDynamicAdapter(Context context, List<?> items,
			int columnCount) {
		super(context, items, columnCount);
		this.context = context;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		itemViewHolder holder;
		if (convertView == null) {
			convertView = LayoutInflater.from(getContext()).inflate(
					R.layout.support_item_grid, null);
			holder = new itemViewHolder(convertView);
			convertView.setTag(holder);
		} else {
			holder = (itemViewHolder) convertView.getTag();
		}

		holder.build(context, ((SupportGridItem) getItem(position)).title,
				((SupportGridItem) getItem(position)).subTitle,
				((SupportGridItem) getItem(position)).imageViewId);
		return convertView;
	}

	private class itemViewHolder {
		private TextView titleText;
		private TextView subTitleText;
		private ImageView image;

		private itemViewHolder(View view) {
			titleText = (TextView) view.findViewById(R.id.item_title);
			subTitleText = (TextView) view.findViewById(R.id.item_sub_title);
			image = (ImageView) view.findViewById(R.id.item_img);
		}

		void build(Context context, String title, String subTitle, int image_id) {
			titleText.setText(title);
			subTitleText.setText(subTitle);
			new ImageLoader(context, image, image_id,1).execute();
		}
	}
}