
package com.smartLab.padra.support;

import java.util.Arrays;

import org.askerov.dynamicgrid.DynamicGridView;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;

import com.example.padra.R;
import com.smartLab.padra.PadraApp;

class SupportGridItem {
	public String title;
	public String subTitle;
	public int imageViewId;
	public int id;

	public SupportGridItem(String title, String subTitle, int imageViewId,
			int id) {
		this.title = title;
		this.imageViewId = imageViewId;
		this.id = id;
		this.subTitle = subTitle;
	}
}

public class SupportActivity extends ActionBarActivity {
	private static final String PADRA_WEBSITE_URL = "http://www.ipadra.ir";
	private static final String SUPPORT_PHONE_NUMBER = "02188318599";
	
	private static final int NUMBER_OF_ITEMS = 4;
	final SupportGridItem[] items = new SupportGridItem[NUMBER_OF_ITEMS];
	
	private DynamicGridView gridView;
	private final Activity __Instance = this;
	private Toolbar toolbar;

	/**
	 * Config for Mr. Amini as our retailer
	 * 
	 */
	//private static final String AMINI_NAME = ;
	private static final String AMINI_MOBILE = "09366357922";
	private static final String AMINI_PHONE = "08734521293";
	private static final String AMINI_EMAIL = "admin@alfamarket.ir";
	private static final String AMINI_WEBSITE = "www.alfamarket.ir";
	
	private boolean retailerSpec = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_support);

		toolbar = (Toolbar) findViewById(R.id.SettingsToolbar);
		((TextView) toolbar.findViewById(R.id.SettingsTitle))
				.setText(getTitle());
		setSupportActionBar(toolbar);

		toolbar.findViewById(R.id.information).setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						LayoutInflater inflater = LayoutInflater
								.from(__Instance);
						View view = inflater.inflate(
								R.layout.dialog_support_costs, null);
						AlertDialog.Builder builder = new AlertDialog.Builder(
								__Instance);
						builder.setTitle(R.string.costs)
								.setView(view)
								.setPositiveButton(R.string.confirm,
										new DialogInterface.OnClickListener() {
											public void onClick(
													DialogInterface dialog,
													int id) {
											}
										});
						builder.show();

					}
				});

		gridView = (DynamicGridView) findViewById(R.id.dynamic_grid_settings);

		// Determine whether this application is for specific retailers
		retailerSpec = true;
	}

	@Override
	public void onResume() {

		//String value = "" + ;
		
		if (retailerSpec) {
			retailerData();
		} else {
		
		items[0] = new SupportGridItem(getResources().getString(
				R.string.padra_version), PadraApp.getVersionName(getApplicationContext()), R.drawable.icon, 0);
		
		items[1] = new SupportGridItem(getResources().getString(
				R.string.contactUs), getResources().getString(
				R.string.appPhoneNumber), R.drawable.call_us, 1);
		
		items[2] = new SupportGridItem(getResources().getString(R.string.fax),
				getResources().getString(R.string.faxNumber), R.drawable.fax, 2);
		
		items[3] = new SupportGridItem(getResources().getString(
				R.string.ourSite), getResources().getString(
				R.string.siteAddress), R.drawable.site, 3);

		gridView.setAdapter(new SupportItemDynamicAdapter(__Instance, Arrays
				.asList(items), getResources().getInteger(
				R.integer.settings_column_count)));

		gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				if (items[position].id == 1) {
					Intent callIntent = new Intent(Intent.ACTION_CALL);
					callIntent.setData(Uri.parse("tel:" + SUPPORT_PHONE_NUMBER));
					startActivity(callIntent);

				} else if (items[position].id == 2) {

				} else if (items[position].id == 3) {
					Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri
							.parse(PADRA_WEBSITE_URL));
					startActivity(browserIntent);
				}
			}
		});
		
		}
		super.onResume();
	}
	
	private void retailerData() {
		
		items[0] = new SupportGridItem(getResources().getString(
				R.string.padra_version), PadraApp.getVersionName(getApplicationContext()), 
				R.drawable.icon, 0);
		
		StringBuilder sb = new StringBuilder();
		sb.append(getString(R.string.contact_mobile));
		sb.append(" ");
		sb.append(AMINI_MOBILE);
		sb.append("\n");
		sb.append(getString(R.string.contact_phone));
		sb.append(" ");
		sb.append(AMINI_PHONE);
		sb.append("\n");
		sb.append(getString(R.string.amini_name));
		
		items[1] = new SupportGridItem(getResources().getString(R.string.contactUs), sb.toString(), R.drawable.call_us, 1);
		
		items[2] = new SupportGridItem(getString(R.string.contact_send_email), AMINI_EMAIL, R.drawable.site, 2);

		items[3] = new SupportGridItem(getString(R.string.ourSite), AMINI_WEBSITE, R.drawable.website, 3);

		
		gridView.setAdapter(new SupportItemDynamicAdapter(__Instance, Arrays
				.asList(items), getResources().getInteger(
				R.integer.settings_column_count)));

		gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				if (items[position].id == 1) {
					
					Intent callIntent = new Intent(Intent.ACTION_CALL);
					callIntent.setData(Uri.parse("tel:" + AMINI_PHONE));
					startActivity(callIntent);

				} else if (items[position].id == 2) {
					
					Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
				            "mailto", AMINI_EMAIL, null));
					emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject");
					emailIntent.putExtra(Intent.EXTRA_TEXT, "Body");
					startActivity(Intent.createChooser(emailIntent, "Send email..."));
					
				} else if (items[position].id == 3) {
					
					Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://"+AMINI_WEBSITE));
					startActivity(browserIntent);
					
				}
				
			}
		});

	}
}
