package com.smartLab.padra.status;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.padra.R;
import com.smartLab.padra.BaseSliderFragment;
import com.smartLab.padra.PreferenceRefrences;
import com.smartLab.padra.SMS.CommandType;
import com.smartLab.padra.SMS.TextMessageSender;
import com.smartLab.padra.remote.AppListAdapter;
import com.smartLab.padra.remote.FoldingPaneLayout;
import com.smartLab.padra.utils.ListItem;
import com.smartLab.setting.SettingActivity;

public class Status extends BaseSliderFragment {

	public FoldingPaneLayout mPaneLayout;
	public ListView mPaneList;
	public TextView powerSystem = null;
	public TextView battery = null;
	public TextView GPRS = null;
	public TextView GPS = null;
	public TextView ACC = null;
	public TextView doorsLock = null;
	public TextView GSM = null;
	public TextView Oil = null;
	public TextView lastUpdate = null;
	
	List<ListItem> drawerChoices;

	public void updateStateViews() {
		SharedPreferences mShared = PreferenceManager
				.getDefaultSharedPreferences(getActivity()
						.getApplicationContext());

		String powerSystemState = mShared.getString(
				PreferenceRefrences.POWER_SYSTEM_STATUS, "?");
		String batteryState = mShared.getString(
				PreferenceRefrences.BATTERY_STATUS, "?");
		String GPRS_State = mShared.getString(PreferenceRefrences.GPRS_STATUS,
				"?");
		String GPS_State = mShared.getString(PreferenceRefrences.GPS_STATUS,
				"?");
		String ACC_State = mShared.getString(PreferenceRefrences.ACC_STATUS,
				"?");
		String doorsLockState = mShared.getString(
				PreferenceRefrences.DOORS_STATUS, "?");
		String GSM_State = mShared.getString(PreferenceRefrences.GSM_STATUS,
				"?");
		String OilState = mShared
				.getString(PreferenceRefrences.OIL_STATUS, "?");

		lastUpdate = (TextView) getActivity().findViewById(R.id.status_last_update);
		powerSystem = (TextView) getActivity().findViewById(R.id.powerSystem);
		battery = (TextView) getActivity().findViewById(R.id.battery);
		GPRS = (TextView) getActivity().findViewById(R.id.GPRS);
		GPS = (TextView) getActivity().findViewById(R.id.GPS);
		ACC = (TextView) getActivity().findViewById(R.id.ACC);
		doorsLock = (TextView) getActivity().findViewById(R.id.doorsLock);
		GSM = (TextView) getActivity().findViewById(R.id.GSM);
		Oil = (TextView) getActivity().findViewById(R.id.Oil);

		String ON = getActivity().getResources().getString(R.string.ON);
		String OFF = getActivity().getResources().getString(R.string.OFF);

		setBooleanTextViewState(powerSystem,
				getActivity().getString(R.string.active), getActivity()
						.getString(R.string.deactive), powerSystemState, true);

		setBooleanTextViewState(GPRS, ON, OFF, GPRS_State, true);
		setBooleanTextViewState(GPS, ON, OFF, GPS_State, true);
		setBooleanTextViewState(ACC, ON, OFF, ACC_State, false);
		setBooleanTextViewState(doorsLock,
				getActivity().getString(R.string.Open), getActivity()
						.getString(R.string.Close), doorsLockState, false);

		setPercentageTextViewState(battery, batteryState);
		setPercentageTextViewState(GSM, GSM_State);
		setPercentageTextViewState(Oil, OilState);

		// FD
		try {
			
		
		long update = mShared.getLong(PreferenceRefrences.CHECK_LAST_UPDATE, 0);
		//update = System.currentTimeMillis();
		if (update != 0) {
			final SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd / HH:mm:ss");//, Locale.getDefault());
			lastUpdate.setText("آخرین بروزرسانی: " + df.format(new Date(update)));
		}
		
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
	}

	private void setBooleanTextViewState(TextView v, String checkOn,
			String CheckOff, String state, boolean onIsGreen) {
		int onColor = (onIsGreen) ? android.R.color.holo_green_light
				: android.R.color.holo_red_light;
		int offColor = (onIsGreen) ? android.R.color.holo_red_light
				: android.R.color.holo_green_light;

		if (state.equals("on")) {
			v.setText(checkOn);
			v.setTextColor(getActivity().getResources().getColor(onColor));
		} else if (state.equals("off")) {
			v.setText(CheckOff);
			v.setTextColor(getActivity().getResources().getColor(offColor));
		} else
			v.setText(getActivity().getResources().getString(R.string.unclear));
	}

	private void setPercentageTextViewState(TextView view, String state) {
		if (view == null) {
			return;
		}
		if (!view.equals("?")) {
			view.setText(digitsDecimal2persian(state));
		} else
			view.setText(getActivity().getResources().getString(
					R.string.unclear));
	}

	public String digitsDecimal2persian(String s) {
		return s.replaceAll(
				"1",
				getActivity().getResources().getString(
						R.string.persianNumberOne))
				.replaceAll(
						"2",
						getActivity().getResources().getString(
								R.string.persianNumberTwo))
				.replaceAll(
						"3",
						getActivity().getResources().getString(
								R.string.persianNumberThree))
				.replaceAll(
						"4",
						getActivity().getResources().getString(
								R.string.persianNumberFour))
				.replaceAll(
						"5",
						getActivity().getResources().getString(
								R.string.persianNumberFive))
				.replaceAll(
						"6",
						getActivity().getResources().getString(
								R.string.persianNumberSix))
				.replaceAll(
						"7",
						getActivity().getResources().getString(
								R.string.persianNumberSeven))
				.replaceAll(
						"8",
						getActivity().getResources().getString(
								R.string.persianNumberEight))
				.replaceAll(
						"9",
						getActivity().getResources().getString(
								R.string.PersianNumberNine))
				.replaceAll(
						"0",
						getActivity().getResources().getString(
								R.string.persianNumberZero));
	}

	@Override
	public void onResume() {
		initDrawer();
		updateStateViews();
		setDrawableAnimation(R.id.animationSwipeLeftStatus,
				R.drawable.animation_left);
		setDrawableAnimation(R.id.animationSwipeRightStatus,
				R.drawable.animation_right);
		super.onResume();
	}

	public static BaseSliderFragment newInstance(int position) {
		BaseSliderFragment f = new Status();
		Bundle bundle = new Bundle();
		bundle.putInt(POSITION_KEY, position);
		f.setArguments(bundle);

		return f;
	}

	@Override
	protected View getLayout(LayoutInflater inflater, ViewGroup container,
			boolean b) {
		return inflater.inflate(R.layout.fragments_status_view, container,
				false);
	}

	private void initDrawer() {
		mPaneLayout = (FoldingPaneLayout) getActivity().findViewById(
				R.id.drawer_layout_status);
		mPaneList = (ListView) getActivity().findViewById(
				R.id.left_drawer_status);

		mPaneLayout.getFoldingLayout().setNumberOfFolds(4);

		mPaneLayout.foldingNavigationLayout.setBackgroundColor(Color.BLACK);

		// set up the drawer's list view with items and click listener

		drawerChoices = new LinkedList<ListItem>();

		drawerChoices.add(new ListItem(0, getActivity().getResources()
				.getString(R.string.settings), getActivity().getResources()
				.getDrawable(R.drawable.icon)));

		drawerChoices.add(new ListItem(1, getActivity().getResources()
				.getString(R.string.udpate), getActivity().getResources()
				.getDrawable(R.drawable.refresh)));

		AppListAdapter adapter = new AppListAdapter(getActivity()
				.getApplicationContext(), drawerChoices);
		mPaneList.setAdapter(adapter);

		mPaneList.setOnItemClickListener(new DrawerItemClickListener());
	}

	/* The click listner for ListView in the navigation drawer */
	private class DrawerItemClickListener implements
			ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			selectItem(position);
		}
	}

	private void selectItem(int position) {
		Toast.makeText(getActivity(), drawerChoices.get(position).text,
				Toast.LENGTH_SHORT);

		if (position == 0) {
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					Intent myIntent = new Intent(getActivity(),
							SettingActivity.class);
					getActivity().startActivity(myIntent);
				}
			}, 200);
		} else if (position == 1) {
			TextMessageSender smsHandler = TextMessageSender
					.getInstance(getActivity());
			smsHandler.sendSms(CommandType.checkState, null);
		}
		mPaneLayout.closePane();
	}

}