package com.smartLab.setting;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.padra.R;
import com.skd.swipetodelete.ItemBase;
import com.skd.swipetodelete.list.ItemBaseListAdapter;
import com.skd.swipetodelete.list.ItemBaseListHandler;
import com.skd.swipetodelete.menu.MenuItemDesc;
import com.smartLab.padra.PreferenceRefrences;
import com.smartLab.padra.SMS.CommandType;
import com.smartLab.padra.SMS.TextMessageSender;

public class UserEditableListView extends Dialog {
	private Activity __activity;
	private EditText editText;
	private Button addButton;
	private ListView list;
	private ItemListAdapter listAdapter;
	private ItemListHandler listHandler;
	private SharedPreferences mShared;
	private Dialog thisDialog;

	public UserEditableListView(Activity a) {
		super(a);
		this.thisDialog = this;
		this.__activity = a;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_users_editable_list);
		mShared = PreferenceManager.getDefaultSharedPreferences(__activity);
		editText = (EditText) findViewById(R.id.editText);
		addButton = (Button) findViewById(R.id.addButton);
		setCancelable(true);
		process();
		addButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String newUser = editText.getText().toString();
				if (newUser.length() >= 11) {
					if (mShared.getString(
							PreferenceRefrences.DEVICE_VALID_USERS, "")
							.length() < 63) {
						boolean newUserExists = false;
						if (newUser.length() == 11 && newUser.charAt(0) == '0')
							newUser = "+98" + newUser.substring(1);
						String[] allUsers = mShared.getString(
								PreferenceRefrences.DEVICE_VALID_USERS, "")
								.split(",");
						for (int i = 0; i < allUsers.length; i++)
							if (newUser.equals(allUsers[i]))
								newUserExists = true;
						if (!newUserExists) {
							TextMessageSender sms = TextMessageSender.getInstance(__activity);
							mShared.edit().putString(PreferenceRefrences.DEVICE_VALID_USERS_REQ, newUser).commit();
							sms.sendSms(CommandType.authorization, newUser);
							Toast.makeText(__activity,R.string.waitingForAdminConfirm,Toast.LENGTH_LONG).show();
							thisDialog.dismiss();
						} else {
							editText.setText("");
							Toast.makeText(__activity,
									R.string.newUserAlreadyExists,
									Toast.LENGTH_LONG).show();
						}
					} else
						Toast.makeText(__activity, R.string.limitedUsers,
								Toast.LENGTH_LONG).show();
				} else
					Toast.makeText(__activity, R.string.invalidInputNumber,
							Toast.LENGTH_LONG).show();
			}

		});
		process();
	}

	private void fillAdapter() {
		Log.i("Users", "users are : " + mShared.getString(PreferenceRefrences.DEVICE_VALID_USERS, ""));
		
		String[] users = mShared.getString(PreferenceRefrences.DEVICE_VALID_USERS, "").split(",");
		for (int i = 0; i < users.length; i++)
			if (users[i].length() == 13 || users[i].length() == 11)
				listAdapter.add(new Item((i + 1), users[i]));
	}

	private void process() {
		list = (ListView) findViewById(android.R.id.list);

		listHandler = new ItemListHandler(list);

		ArrayList<MenuItemDesc> menuItems = fillMenu();

		listAdapter = new ItemListAdapter(R.layout.item, menuItems, listHandler);
		fillAdapter();
		list.setAdapter(listAdapter);
	}

	private ArrayList<MenuItemDesc> fillMenu() {
		ArrayList<MenuItemDesc> items = new ArrayList<MenuItemDesc>();

		MenuItemDesc itemEdit = new MenuItemDesc();
		itemEdit.setAction(__activity.getString(R.string.editAction));
		itemEdit.setIcon(R.drawable.btn_edit);
		items.add(itemEdit);

		MenuItemDesc itemShare = new MenuItemDesc();
		itemShare.setAction(__activity.getString(R.string.shareAction));
		itemShare.setIcon(R.drawable.btn_share);
		items.add(itemShare);

		return items;
	}

	private void showEditItemConfirmDialog(final int position) {
		final EditText itemText = new EditText(__activity);

		AlertDialog.Builder builder = new AlertDialog.Builder(__activity);
		builder.setMessage(R.string.sureEdit)
				.setCancelable(false)
				.setView(itemText)
				.setPositiveButton(R.string.ok,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								listAdapter.edit(position, itemText.getText()
										.toString());
							}
						})
				.setNegativeButton(R.string.cancel,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});
		AlertDialog alert = builder.create();
		alert.show();
	}

	private void showShareItemDialog(int position) {
		Intent i = new Intent();
		i.setAction(Intent.ACTION_SEND);
		i.putExtra(Intent.EXTRA_TEXT,
				((Item) listAdapter.getItem(position)).getText());
		i.setType("text/plain");
		__activity.startActivity(i);
	}

	class ItemListHandler extends ItemBaseListHandler {

		public ItemListHandler(ListView list) {
			super(list);
		}

		@Override
		public void onItemMenuAction(int position, String action) {
			if (action.equals(__activity.getString(R.string.editAction))) {
				showEditItemConfirmDialog(position);
			} else if (action
					.equals(__activity.getString(R.string.shareAction))) {
				showShareItemDialog(position);
			}
		}
	}

	class Item extends ItemBase {
		private int id;
		private String text;

		public Item(int id, String text) {
			super();
			this.id = id;
			this.text = text;
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getText() {
			return text;
		}

		public void setText(String text) {
			this.text = text;
		}
	}

	class ItemListAdapter extends ItemBaseListAdapter {

		public ItemListAdapter(int itemLayoutID,
				ArrayList<MenuItemDesc> menuItems, ItemBaseListHandler handler) {
			super(itemLayoutID, menuItems, handler);
		}

		@Override
		public void remove(int position) {
			String adminToDelete = ((Item) data.get(position)).getText();
			TextMessageSender sms = TextMessageSender.getInstance(__activity);
			if (adminToDelete.length() == 11 || adminToDelete.length() == 13) {
				mShared.edit()
						.putString(PreferenceRefrences.ADMIN_DELETING_REQ,
								adminToDelete).commit();
				sms.sendSms(CommandType.deleteAuthorization, adminToDelete);
				super.remove(position);

				Toast.makeText(__activity, R.string.waitingForNoAdminConfirm,
						Toast.LENGTH_SHORT).show();
				thisDialog.dismiss();
			} else {
				Toast.makeText(__activity, R.string.noAdminToRemove,
						Toast.LENGTH_SHORT).show();
			}
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			View view = super.getView(position, convertView, parent);

			Item dt = (Item) data.get(position);

			TextView text = (TextView) view.findViewById(R.id.text);
			text.setText(dt.getText());

			return view;
		}

		// actions
		// ***********************************************************************************

		public void edit(int position, String text) {
			if (text.length() != 11) {
				Toast.makeText(__activity, R.string.invalidInputNumber,
						Toast.LENGTH_LONG).show();
			}
			if (!((Item) this.data.get(position)).getText().equals(text)) {
				if (text.length() != 11)
					Toast.makeText(__activity, R.string.invalidInputNumber,
							Toast.LENGTH_LONG).show();
				else {
					TextMessageSender sms = TextMessageSender
							.getInstance(__activity);
					sms.sendSms(CommandType.deleteAuthorization,
							((Item) this.data.get(position)).getText());
					sms.sendSms(CommandType.authorization, text);
					Toast.makeText(__activity,
							R.string.waitingForAdminEditConfirm,
							Toast.LENGTH_LONG).show();
					((Item) this.data.get(position)).setText(text);
					notifyDataSetChanged();
				}
			}
		}
	}
}