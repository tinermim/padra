package com.smartLab.setting;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

import org.askerov.dynamicgrid.DynamicGridView;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.example.padra.R;
import com.haibison.android.lockpattern.LockPatternActivity;
import com.smartLab.padra.PadraApp;
import com.smartLab.padra.PreferenceRefrences;
import com.smartLab.padra.SMS.CommandType;
import com.smartLab.padra.SMS.TextMessageSender;
import com.smartLab.padra.options.GridItem;
import com.smartLab.padra.remote.FoldingPaneLayout;
import com.smartLab.padra.utils.ListItem;

public class SettingActivity extends ActionBarActivity {

	private static final int NUMBER_OF_ITEMS = 10;
	private static final int REQ_CREATE_PATTERN = 1;
	// private static final int REQ_ENTER_PATTERN = 2;

	final GridItem[] items = new GridItem[NUMBER_OF_ITEMS];
	private SharedPreferences mShared;
	public FoldingPaneLayout mPaneLayout;
	public ListView mPaneList;
	FrameLayout frame;
	List<ListItem> appsData;
	private DynamicGridView gridView;
	private final Activity __Instance = this;

	private EditText text;
	private int choice = -1;
	Toolbar toolbar;

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		switch (requestCode) {
		case 1: {
			if (resultCode == RESULT_OK) {

				char[] pattern = data
						.getCharArrayExtra(LockPatternActivity.EXTRA_PATTERN);
				String patternToSave = new String(pattern);
				mShared.edit()
						.putString(PreferenceRefrences.APP_PASSWORD_KEY,
								patternToSave).commit();
				mShared.edit()
						.putBoolean(PreferenceRefrences.IS_APP_PASSWORD_NEEDED_KEY,
								true).commit();
				Toast.makeText(getApplicationContext(), R.string.passwordSet,
						Toast.LENGTH_SHORT).show();
			} else if (resultCode == RESULT_CANCELED) {
				Toast.makeText(getApplicationContext(), R.string.noPasswordSet,
						Toast.LENGTH_SHORT).show();
			}
			break;
		}// REQ_CREATE_PATTERN
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);
		mShared = PreferenceManager
				.getDefaultSharedPreferences(getApplicationContext());
		toolbar = (Toolbar) findViewById(R.id.SettingsToolbar);
		((TextView) toolbar.findViewById(R.id.SettingsTitle))
				.setText(getTitle()
						+ " "
						+ mShared
								.getString(PreferenceRefrences.DEVICE_NAME, ""));
		setSupportActionBar(toolbar);

		gridView = (DynamicGridView) findViewById(R.id.dynamic_grid_settings);
		
	}

	
	@Override
	public void onResume() {
		try {
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	
		
		items[mShared.getInt(0 + "", 0)] = new GridItem(
				getString(R.string.carName), R.drawable.car_name, 0);
		items[mShared.getInt(1 + "", 1)] = new GridItem(
				getString(R.string.devicePassword), R.drawable.device_password,
				1);
		items[mShared.getInt(2 + "", 2)] = new GridItem(
				getString(R.string.deviceNumber), R.drawable.device_number, 2);
		items[mShared.getInt(3 + "", 3)] = new GridItem(
				getString(R.string.authenticatedNumbers),
				R.drawable.allowed_numbers, 3);
		items[mShared.getInt(4 + "", 4)] = new GridItem(
				getString(R.string.factorySettings), R.drawable.factory_reset,
				4);
		items[mShared.getInt(5 + "", 5)] = new GridItem(
				getString(R.string.appPassword), R.drawable.app_password, 5);
		items[mShared.getInt(6 + "", 6)] = new GridItem(
				getString(R.string.deviceType), R.drawable.padra, 6);
		items[mShared.getInt(7 + "", 7)] = new GridItem(
				getString(R.string.speed_alarm), R.drawable.speedometer, 7);
		
		items[mShared.getInt(8 + "", 8)] = new GridItem(
				getString(R.string.alarm_set), R.drawable.alert, 8);
		
		items[mShared.getInt(9 + "", 9)] = new GridItem(
				getString(R.string.time_interval_set), R.drawable.schedule_time_watch_001, 9);
		
		
		gridView.setAdapter(new SettingsItemDynamicAdapter(__Instance, Arrays
				.asList(items), getResources().getInteger(
				R.integer.settings_column_count)));

		gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				if (items[position].id == 0) {
					dialogBuilder(1);

				} else if (items[position].id == 1) {
					dialogBuilder(2);

				} else if (items[position].id == 2) {
					dialogBuilder(3);

				} else if (items[position].id == 3) {

					dialogBuilder(4);

				} else if (items[position].id == 4) {
					dialogBuilder(5);

				} else if (items[position].id == 5) {

					final Boolean isPasswrodActive = mShared.getBoolean(
							PreferenceRefrences.IS_APP_PASSWORD_NEEDED_KEY, false);
					Builder changePassword = new AlertDialog.Builder(__Instance);
					int arrayToShow = (isPasswrodActive) ? R.array.passwordChange
							: R.array.passwordActivation;

					changePassword.setCancelable(false);
					changePassword.setSingleChoiceItems(arrayToShow,
							(isPasswrodActive) ? 1 : 0,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int which) {
									switch (which) {
									case 0:
										choice = 0;
										break;
									case 1:
										choice = 1;
										break;
									case 2:
										choice = 2;
										break;
									}
								}

							});

					// alertDialogBuilder.setIcon(R.drawable.question_mark);
					// // Set the action buttons
					changePassword.setPositiveButton(
							getResources().getString(R.string.accept),
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int id) {
									if (choice == 0) {
										mShared.edit()
												.putBoolean(
														PreferenceRefrences.IS_APP_PASSWORD_NEEDED_KEY,
														false).commit();
									} else if ((!isPasswrodActive && choice == 1)
											|| (isPasswrodActive && choice == 2)) {
										Intent intent = new Intent(
												LockPatternActivity.ACTION_CREATE_PATTERN,
												null, getApplicationContext(),
												LockPatternActivity.class);
										startActivityForResult(intent,
												REQ_CREATE_PATTERN);
									}

								}
							});
					changePassword.setCancelable(true);
					changePassword.show();

				}
				else if (items[position].id == 6) {
					
					handleDeviceType();
					
				} else if (items[position].id == 7) {
					
					speedLimit();
					
				} else if (items[position].id == 8) {
					
					handleAlarmStatus();
					
				} else if (items[position].id == 9) {
					
					handleTimeInterval();
					
				}
			}
		});

		super.onResume();
	}

	/**
	 * Handle device type
	 */
	private void handleDeviceType() {
		int key = mShared.getInt(PreferenceRefrences.DEVICE_TYPE_KEY, 3);
		Builder alertDialogBuilder = new AlertDialog.Builder(__Instance);
		// set title

		alertDialogBuilder.setTitle(getResources().getString(R.string.deviceType));
		alertDialogBuilder.setSingleChoiceItems(R.array.deviceType, key, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				switch (which) {
				case 0:
					mShared.edit().putInt(PreferenceRefrences.DEVICE_TYPE_KEY, 0).commit();
					break;
				case 1:
					mShared.edit().putInt(PreferenceRefrences.DEVICE_TYPE_KEY, 1).commit();
					break;
				case 2:
					mShared.edit().putInt(PreferenceRefrences.DEVICE_TYPE_KEY, 2).commit();
					break;
				case 3:
					mShared.edit().putInt(PreferenceRefrences.DEVICE_TYPE_KEY, 3).commit();
					break;
				case 4:
					mShared.edit().putInt(PreferenceRefrences.DEVICE_TYPE_KEY, 4).commit();
					break;
				}
			}
		});

		// alertDialogBuilder.setIcon(R.drawable.question_mark);
		// // Set the action buttons
		alertDialogBuilder.setPositiveButton(__Instance.getResources().getString(R.string.accept), null);
		alertDialogBuilder.setCancelable(true);
		alertDialogBuilder.show();

	}
	
	/**
	 * Handling speed limit number
	 */
	@SuppressLint("InflateParams")
	private void speedLimit() {
		
		LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		final NumberPicker np = (NumberPicker) inflater.inflate(R.layout.num_picker_dialog, null);
		
		//myNumberPick.setDisplayedValues(myValues);
		//int iStepsArray = (iMaxValue-iMinValue)/iStep+1; //get the lenght array that will return
		//String[] arrayValues= new String[iStepsArray]; //Create array with length of iStepsArray 
		//for(int i = 0; i < iStepsArray; i++){
		//	arrayValues[i] = String.valueOf(iMinValue + (i*iStep));
		//}
		// Defining speed limit from 20 to 220 KM/H 
		// Step size is 5
		np.setMinValue(4);
		np.setMaxValue(44);
		NumberPicker.Formatter formatter = new NumberPicker.Formatter() {
	        @Override
	        public String format(int value) {
	            int temp = value * 5;
	            return "" + temp;
	        }
	    };
	    np.setFormatter(formatter);
	    np.setValue(mShared.getInt(PreferenceRefrences.SPEED_LIMIT_KEY, 24));

	    //np.setMinValue(20);
		//np.setMaxValue(220);
		//np.setValue(mShared.getInt(PreferenceRefrences.SPEED_LIMIT_KEY, 120));
	    Log.i("SettingsActivity", "Assigning speed limit");
		
	    np.setWrapSelectorWheel(true);
		setNumberPickerTextColor(np, getResources().getColor(R.color.black));
		np.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
		Builder alertDialogBuilder = new AlertDialog.Builder(__Instance);
		alertDialogBuilder
			.setTitle(R.string.speed_limit_title)
			.setCancelable(true)
			.setView(np)
			.setPositiveButton(R.string.send_command,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int id) {
						TextMessageSender sender = TextMessageSender.getInstance(__Instance);
						int value = np.getValue() * 5;
						mShared.edit().putInt(PreferenceRefrences.SPEED_LIMIT_KEY, np.getValue()).commit();
						String addInfo = (value >= 100) ? value + "" : "0" + value;
						sender.sendSms(CommandType.speedLimit, addInfo);
						PadraApp.showToast("پیام حداکثر سرعت مجاز به دستگاه ارسال شد. منتظر پاسخ از دستگاه بمانید.", Toast.LENGTH_LONG);
						Log.i("SettingsActivity", "Picker value : " + value);
					}
				}).setNegativeButton(R.string.cancel, null)
			.show();
	}
	
	/**
	 * Handling alarm on or off status
	 */
	private void handleAlarmStatus() {
		
		int key = mShared.getInt(PreferenceRefrences.ALARM_TYPE_KEY, 0);
		Builder alertDialogBuilder = new AlertDialog.Builder(__Instance);
		
		// set title
		alertDialogBuilder.setTitle(getResources().getString( R.string.alarm_set));
		alertDialogBuilder.setSingleChoiceItems(R.array.alarmType, key, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				switch (which) {
				case 0:
					mShared.edit().putInt(PreferenceRefrences.ALARM_TYPE_KEY, 0).commit();
					break;
				case 1:
					mShared.edit().putInt(PreferenceRefrences.ALARM_TYPE_KEY, 1).commit();
					break;
				}
			}
		});

		// alertDialogBuilder.setIcon(R.drawable.question_mark);
		// _Instance.getResources().getString(R.string.accept)
		alertDialogBuilder.setPositiveButton(R.string.accept, null);
		alertDialogBuilder.setCancelable(true);
		alertDialogBuilder.show();
		
	}
	
	
	/**
	 * Handling tracker time interval
	 */
	private int interval;
	private void handleTimeInterval() {
		
		interval = mShared.getInt(PreferenceRefrences.TIME_INTERVAL_TO_SERVER, 2);
		Builder alertDialogBuilder = new AlertDialog.Builder(__Instance);
		alertDialogBuilder.setTitle(R.string.time_interval_set); //getResources().getString(R.string.time_interval_set)
		alertDialogBuilder.setSingleChoiceItems(R.array.timeIntervalValues, interval, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				interval = which;
			}
		});
		//__Instance.getResources().getString(R.string.accept)
		alertDialogBuilder.setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				mShared.edit().putInt(PreferenceRefrences.TIME_INTERVAL_TO_SERVER, interval).commit();
				Log.i("SettingsActivity", "Interval item : " + interval);
				
				String data;
				switch (interval) {
				case 0:
					data = "fix015s***n";
					break;
				case 1:
					data = "fix030s***n";
					break;
				case 2:
					data = "fix060s***n";
					break;
				case 3:
					data = "fix090s***n";
					break;
				case 4:
					data = "fix002m***n";
					break;
				case 5:
					data = "fix003m***n";
					break;
				case 6:
					data = "fix004m***n";
					break;
				case 7:
					data = "fix005m***n";
					break;
				case 8:
					data = "fix010m***n";
					break;
				case 9:
					data = "fix015m***n";
					break;
				case 10:
					data = "fix020m***n";
					break;
				case 11:
					data = "fix025m***n";
					break;
				case 12:
					data = "fix030m***n";
					break;
				case 13:
					data = "fix045m***n";
					break;
				case 14:
					data = "fix001h***n";
					break;
				case 15:
					data = "fix002h***n";
					break;
				case 16:
					data = "fix003h***n";
					break;
				case 17:
					data = "fix004h***n";
					break;
				case 18:
					data = "fix005h***n";
					break;
				case 19:
					data = "fix010h***n";
					break;
				case 20:
					data = "fix015h***n";
					break;
				case 21:
					data = "fix024h***n";
					break;
				default:
					data = "fix060s***n";
					break;
				}
				// set this value as default
				// Sending time interval to server
				PadraApp.showToast("پیام بازه ی زمانی سرور به دستگاه ارسال شد. ", Toast.LENGTH_LONG);
				Log.i("SettingsActivity", "Interval data : " + data);
				
				TextMessageSender sender = TextMessageSender.getInstance(__Instance);
				mShared.edit().putBoolean("went_for_time_interval", true).commit();
				sender.sendSmsCustom(CommandType.trackUnlimitedIntervalCustom, data, null, true);
			}
		});
		
		alertDialogBuilder.setNegativeButton(R.string.cancel, null);
		alertDialogBuilder.setCancelable(true);
		alertDialogBuilder.show();
	}
	
	public static boolean setNumberPickerTextColor(NumberPicker numberPicker, int color)
	{
	    final int count = numberPicker.getChildCount();
	    for(int i = 0; i < count; i++){
	        View child = numberPicker.getChildAt(i);
	        if(child instanceof EditText){
	            try{
	                Field selectorWheelPaintField = numberPicker.getClass()
	                    .getDeclaredField("mSelectorWheelPaint");
	                selectorWheelPaintField.setAccessible(true);
	                ((Paint)selectorWheelPaintField.get(numberPicker)).setColor(color);
	                ((EditText)child).setTextColor(color);
	                numberPicker.invalidate();
	                return true;
	            }
	            catch(NoSuchFieldException e){
	                Log.w("setNumberPickerTextColor", e);
	            }
	            catch(IllegalAccessException e){
	                Log.w("setNumberPickerTextColor", e);
	            }
	            catch(IllegalArgumentException e){
	                Log.w("setNumberPickerTextColor", e);
	            }
	        }
	    }
	    return false;
	}
	
	private void dialogBuilder(int id) {
		boolean newDialog = false;
		final Dialog dialog = new Dialog(this);
		Button ok, cancel;

		switch (id) {
		case 1:
			dialog.setContentView(R.layout.setting_car_name);

			dialog.setTitle(getResources().getString(R.string.changeCarName)
					+ " ("
					+ mShared.getString(PreferenceRefrences.DEVICE_NAME, "")
					+ ")");

			ok = (Button) dialog.findViewById(R.id.carName_ok);
			cancel = (Button) dialog.findViewById(R.id.carName_cancel);
			cancel.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
				}
			});
			ok.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					text = (EditText) dialog
							.findViewById(R.id.setting_car_name);
					if (text.getText().toString().length() != 0) {
						mShared.edit()
								.putString(PreferenceRefrences.DEVICE_NAME,
										text.getText().toString()).commit();
						dialog.dismiss();
					} else
						Toast.makeText(__Instance, R.string.invalidInput,
								Toast.LENGTH_LONG).show();
				}
			});
			break;
		case 2:
			dialog.setContentView(R.layout.setting_pass_dialog);
			dialog.setTitle(R.string.changePasswordTitle);
			ok = (Button) dialog.findViewById(R.id.carPass_ok);
			cancel = (Button) dialog.findViewById(R.id.carPass_cancel);
			cancel.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
				}
			});
			ok.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					String[] string = new String[3];
					text = (EditText) dialog
							.findViewById(R.id.setting_car_prev_pass);
					string[0] = text.getText().toString();
					text = (EditText) dialog
							.findViewById(R.id.setting_car_pass);
					string[1] = text.getText().toString();
					text = (EditText) dialog
							.findViewById(R.id.setting_car_pass_confirm);
					string[2] = text.getText().toString();
					if (string[1].length() != 6)
						Toast.makeText(__Instance,
								R.string.invalidPasswordLenght,
								Toast.LENGTH_LONG).show();
					else if ((!string[1].equals(string[2]))) {
						Toast.makeText(__Instance, R.string.invalidPassword,
								Toast.LENGTH_LONG).show();
						text = (EditText) dialog
								.findViewById(R.id.setting_car_pass);
						text.setText("");
						text = (EditText) dialog
								.findViewById(R.id.setting_car_pass_confirm);
						text.setText("");
					} else if (string[0].equals(mShared.getString(
							PreferenceRefrences.DEVICE_PASSWORD, ""))) {
						mShared.edit()
								.putString(
										PreferenceRefrences.DEVICE_PASSWORD_REQ,
										string[2]).commit();

						TextMessageSender sms = TextMessageSender
								.getInstance(__Instance);
						sms.sendSms(
								CommandType.changePassword,
								mShared.getString(
										PreferenceRefrences.DEVICE_PASSWORD_REQ,
										""));
						dialog.dismiss();
					} else {
						Toast.makeText(__Instance, R.string.wrongPassword,
								Toast.LENGTH_LONG).show();
						text = (EditText) dialog
								.findViewById(R.id.setting_car_prev_pass);
						text.setText("");
					}
				}
			});
			break;
		case 3:
			newDialog = true;
			final Dialog mDialog = new Dialog(__Instance);
			mDialog.setContentView(R.layout.device_sim_number_change_confirm);
			mDialog.setCancelable(true);
			mDialog.setTitle(__Instance.getResources().getString(R.string.device_simNum_change_title));
			mDialog.show();
			
			Button mConfirm = (Button) mDialog
					.findViewById(R.id.device_simNum_change_ok);
			Button mCancel = (Button) mDialog
					.findViewById(R.id.device_simNum_change_cancel);
			mConfirm.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					PreferenceRefrences.resetApplicationPreferencesInfo(
							__Instance, mShared);
					mDialog.dismiss();
					
					
					
				}
			});

			mCancel.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					mDialog.dismiss();
				}
			});

			break;
		case 4:
			Log.i("SettingsActivity", "Adding more admins");
			/* Adding more administrator users to device */
			UserEditableListView listView = new UserEditableListView(__Instance);
			listView.show();
			newDialog = true;
			break;
			
		case 5:
			dialog.setContentView(R.layout.setting_car_factory_reset);
			dialog.setTitle(R.string.factorySettingsTitle);
			ok = (Button) dialog.findViewById(R.id.factory_reset_ok);
			cancel = (Button) dialog.findViewById(R.id.factory_reset_cancel);
			cancel.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
				}
			});
			ok.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					TextMessageSender sms = TextMessageSender.getInstance(__Instance);
					sms.sendSms(CommandType.initialization, null);
					dialog.dismiss();
				}
			});
			break;

		}
		if (!newDialog)
			dialog.show();
		else
			dialog.cancel();
	}

}
